This a small playground to test Eclipse Scout (https://www.eclipse.org/scout/). A Framework to easily build Business Applications in Java with web surfaces.

This one simulates a simple doctors office. You can create patients. Plan appointments in a calendar and track home visits.
