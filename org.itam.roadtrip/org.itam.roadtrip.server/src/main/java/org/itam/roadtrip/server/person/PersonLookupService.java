package org.itam.roadtrip.server.person;

import org.eclipse.scout.rt.server.jdbc.lookup.AbstractSqlLookupService;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.person.IPersonLookupService;

public class PersonLookupService extends AbstractSqlLookupService<String> implements IPersonLookupService {

	@Override
	protected String getConfiguredSqlSelect() {
		return SQLs.PERSON_LOOKUP;
	}
}
