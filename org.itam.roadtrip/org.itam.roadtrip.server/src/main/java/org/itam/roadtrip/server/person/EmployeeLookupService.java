package org.itam.roadtrip.server.person;

import org.eclipse.scout.rt.server.jdbc.lookup.AbstractSqlLookupService;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.person.IEmployeeLookupService;

public class EmployeeLookupService extends AbstractSqlLookupService<String> implements IEmployeeLookupService {

	@Override
	protected String getConfiguredSqlSelect() {
		return SQLs.EMPLOYEE_LOOKUP;
	}
}
