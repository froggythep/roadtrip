package org.itam.roadtrip.server.sql;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.platform.exception.PlatformExceptionTranslator;
import org.eclipse.scout.rt.server.jdbc.derby.AbstractDerbySqlService;
import org.itam.roadtrip.server.sql.DatabaseProperties.DatabaseAutoCreateProperty;
import org.itam.roadtrip.server.sql.DatabaseProperties.JdbcMappingNameProperty;

@Order(1950)
// tag::service[]
public class DerbySqlService extends AbstractDerbySqlService {

	@Override
	protected String getConfiguredJdbcMappingName() {
		final String mappingName = CONFIG.getPropertyValue(JdbcMappingNameProperty.class);

		// add create attribute if we need to autocreate the db
		if (CONFIG.getPropertyValue(DatabaseAutoCreateProperty.class)) {
			return mappingName + ";create=true"; // <1>
		}

		return mappingName;
	}
	// end::service[]

	public void dropDB() {
		try {
			final String jdbcMappingName = CONFIG.getPropertyValue(DatabaseProperties.JdbcMappingNameProperty.class);
			DriverManager.getConnection(jdbcMappingName + ";drop=true");
		}
		catch (final SQLException e) {
			BEANS.get(PlatformExceptionTranslator.class).translate(e);
		}
	}
	// tag::service[]
}
// end::service[]
