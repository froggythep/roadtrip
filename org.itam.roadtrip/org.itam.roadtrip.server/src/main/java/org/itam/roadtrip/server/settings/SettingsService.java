package org.itam.roadtrip.server.settings;

import java.util.HashMap;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.platform.holders.StringHolder;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.settings.CreateSettingsPermission;
import org.itam.roadtrip.shared.settings.ISettingsService;
import org.itam.roadtrip.shared.settings.ReadSettingsPermission;
import org.itam.roadtrip.shared.settings.SettingsDefines;
import org.itam.roadtrip.shared.settings.SettingsDefines.SettingsKeys;
import org.itam.roadtrip.shared.settings.UpdateSettingsPermission;

public class SettingsService implements ISettingsService {

	@Override
	public void prepareCreate() {
		if (!ACCESS.check(new CreateSettingsPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

	}

	@Override
	public void create() {
		if (!ACCESS.check(new CreateSettingsPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

	}

	@Override
	public void load() {
		if (!ACCESS.check(new ReadSettingsPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

	}

	@Override
	public void store() {
		if (!ACCESS.check(new UpdateSettingsPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
	}

	@Override
	public void storeSettingsKeyValuePair(final SettingsDefines.SettingsKeys key, final String value) {
		SQL.insert(SQLs.CONFIGURATION_UPDATE, new NVPair("key", key.getKey()), new NVPair("value", value));
	}

	@Override
	public HashMap<SettingsDefines.SettingsKeys, String> loadSettingsKeyValuePairs() {
		final HashMap<SettingsDefines.SettingsKeys, String> keyValuePair = new HashMap<>(SettingsKeys.values().length);
		for (final SettingsKeys key : SettingsKeys.values()) {
			final StringHolder stringHolder = new StringHolder("value");
			SQL.selectInto(SQLs.CONFIGURATION_BY_KEY, new NVPair("key", key.getKey()), stringHolder);
			keyValuePair.put(key, stringHolder.getValue());
		}
		return keyValuePair;
	}
}
