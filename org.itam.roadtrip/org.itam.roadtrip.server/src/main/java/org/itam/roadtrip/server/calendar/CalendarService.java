package org.itam.roadtrip.server.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.itam.roadtrip.server.ServerDefines;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.GlobalDefines.UpdateType;
import org.itam.roadtrip.shared.RepeatTypes;
import org.itam.roadtrip.shared.calendar.CalendarAppointmentRoadtrip;
import org.itam.roadtrip.shared.calendar.CalendarUtil;
import org.itam.roadtrip.shared.calendar.ICalendarService;

public class CalendarService implements ICalendarService {

	@Override
	public void saveAppointment(final CalendarAppointmentRoadtrip appointment) {
		saveAppointment(appointment, false);

	}

	public void saveAppointment(final CalendarAppointmentRoadtrip appointment, final boolean saveByAppointmentRepeatId) {
		if (appointment.isRepeating() && appointment.isRepeatTillInfinity()) {
			insertRepeatTillInfinityAppointments(appointment, saveByAppointmentRepeatId);
		} else if (appointment.isRepeating() && !appointment.isRepeatTillInfinity() && appointment.getRepeatTill() != null) {
			insertRepeatTillDateAppointments(appointment, saveByAppointmentRepeatId);
		} else {
			insertUpdateSingleCalendarAppointment(appointment, saveByAppointmentRepeatId);
		}

	}

	private void insertRepeatTillDateAppointments(final CalendarAppointmentRoadtrip master, final boolean saveByAppointmentRepeatId) {
		if (StringUtility.isNullOrEmpty(master.getAppointmentRepeatId())) {
			master.setAppointmentRepeatId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_APPOINTMENT_REPEAT));
		}

		insertUpdateSingleCalendarAppointment(master, saveByAppointmentRepeatId);
	}

	private void insertRepeatTillInfinityAppointments(final CalendarAppointmentRoadtrip master, final boolean saveByAppointmentRepeatId) {
		if (StringUtility.isNullOrEmpty(master.getAppointmentRepeatId())) {
			master.setAppointmentRepeatId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_APPOINTMENT_REPEAT));
		}
		insertUpdateSingleCalendarAppointment(master, saveByAppointmentRepeatId);
	}

	private void insertUpdateSingleCalendarAppointment(final CalendarAppointmentRoadtrip appointment, final boolean saveByAppointmentRepeatId) {
		if (StringUtility.isNullOrEmpty(appointment.getAppointmentId()) && ! saveByAppointmentRepeatId) {
			appointment.setAppointmentId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_APPOINTMENT));
			SQL.insert(SQLs.APPOINTMENT_INSERT, new NVPair("appointmentId", appointment.getAppointmentId()));
		}

		final NVPair appointmentId = new NVPair("appointmentId", appointment.getAppointmentId());
		final NVPair employeeId = new NVPair("employeeId", appointment.getEmployeeId());
		final NVPair patientId = new NVPair("patientId", appointment.getPatientId());
		final NVPair roomId = new NVPair("roomId", appointment.getRoomId());
		final NVPair isFullday = new NVPair("isFullday", appointment.isFullDay());
		final NVPair dateFrom = new NVPair("dateFrom", appointment.getStart());
		final NVPair dateTo = new NVPair("dateTo", appointment.getEnd());
		final NVPair isRepeating = new NVPair("isRepeating", appointment.isRepeating());
		final NVPair appointmentRepeatId = new NVPair("appointmentRepeatId", appointment.getAppointmentRepeatId());
		final NVPair repeatType = new NVPair("repeatType", appointment.getRepeatType());
		final NVPair isRepeatTillInfinity = new NVPair("isRepeatTillInfinity", appointment.isRepeatTillInfinity());
		final NVPair repeatTill = new NVPair("repeatTill", appointment.getRepeatTill());
		final NVPair notice = new NVPair("notice", appointment.getNotice());
		final NVPair isCancelled = new NVPair("isCancelled", appointment.isCancelled());
		final NVPair recordstatusId = new NVPair("recordstatusId", appointment.getRecordstatusId());

		if ( ! saveByAppointmentRepeatId) {
			SQL.update(SQLs.APPOINTMENT_UPDATE, appointmentId, employeeId, patientId, roomId, isFullday, dateFrom, dateTo, isRepeating,
					appointmentRepeatId, repeatType, isRepeatTillInfinity, repeatTill, notice, isCancelled, recordstatusId);
		} else {
			SQL.update(SQLs.APPOINTMENT_UPDATE_BY_APPOINTMENT_REPEAT_ID, appointmentId, employeeId, patientId, roomId, isFullday, dateFrom, dateTo, isRepeating,
					appointmentRepeatId, repeatType, isRepeatTillInfinity, repeatTill, notice, isCancelled, recordstatusId);
		}
	}

	@Override
	public Collection<CalendarAppointmentRoadtrip> getAllAppointments(final Date dateFrom, final Date dateTo) {
		final Object[][] appointmentObjects = SQL.select(SQLs.APPOINTMENT_ALL_ACTIVE, new NVPair("dateFrom", dateFrom), new NVPair("dateTo", dateTo));

		return createCalendarAppointments(appointmentObjects, true, dateFrom, dateTo);
	}

	private Collection<CalendarAppointmentRoadtrip> createCalendarAppointments(final Object[][] appointmentObjects, final boolean withAddtionalData, final Date dateFrom, final Date dateTo) {
		final Collection<CalendarAppointmentRoadtrip> persistedAppointments = new HashSet<>();

		for (final Object[] appointmentObject : appointmentObjects) {
			final CalendarAppointmentRoadtrip appointment = new CalendarAppointmentRoadtrip();
			appointment.setAppointmentId(String.valueOf(appointmentObject[0]));
			appointment.setEmployeeId(String.valueOf(appointmentObject[1]));
			appointment.setPatientId(String.valueOf(appointmentObject[2]));
			appointment.setRoomId(String.valueOf(appointmentObject[3]));
			appointment.setFullDay(Integer.valueOf(String.valueOf(appointmentObject[4])) == 0 ? false : true);
			appointment.setStart(transformDateTime(String.valueOf(appointmentObject[5])));
			appointment.setEnd(transformDateTime(String.valueOf(appointmentObject[6])));
			appointment.setRepeating(Integer.valueOf(String.valueOf(appointmentObject[7])) == 0 ? false : true);
			appointment.setAppointmentRepeatId(String.valueOf(appointmentObject[8]));
			appointment.setRepeatType(String.valueOf(appointmentObject[9]));
			appointment.setRepeatTillInfinity(Integer.valueOf(String.valueOf(appointmentObject[10])) == 0 ? false : true);
			appointment.setRepeatTill(transformDateTime(String.valueOf(appointmentObject[11])));
			appointment.setNotice(String.valueOf(appointmentObject[12]));
			appointment.setCancelled(Integer.valueOf(String.valueOf(appointmentObject[13])) == 0 ? false : true);

			if (withAddtionalData) {
				appointment.setRecordstatusId(Integer.valueOf(String.valueOf(appointmentObject[14])));
				appointment.setEmployeeName(String.valueOf(appointmentObject[15]));
				appointment.setPatientName(String.valueOf(appointmentObject[16]));
				appointment.setPatientSirname(String.valueOf(appointmentObject[17]));
				appointment.setRoomName(String.valueOf(appointmentObject[18]));
			}

			if (appointment.isRepeating() && appointment.isRepeatTillInfinity()) {
				persistedAppointments.addAll(createAppointmentsTill(appointment, dateTo));
			} else if (appointment.isRepeating() && ! appointment.isRepeatTillInfinity()) {
				final Date dateToForRepeating = dateTo.before(appointment.getRepeatTill()) ? dateTo : appointment.getRepeatTill();
				persistedAppointments.addAll(createAppointmentsTill(appointment, dateToForRepeating));
			} else {
				persistedAppointments.add(appointment);
			}
		}

		return persistedAppointments;
	}

	private Collection<CalendarAppointmentRoadtrip> createAppointmentsTill(final CalendarAppointmentRoadtrip appointment, final Date till) {
		final Collection<CalendarAppointmentRoadtrip> appointments = new ArrayList<>();

		final Calendar calStart = Calendar.getInstance();
		calStart.setTime(appointment.getStart());
		final Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(appointment.getEnd());
		final Calendar calTill = Calendar.getInstance();
		calTill.setTime(till);
		calTill.set(Calendar.HOUR_OF_DAY, 23);
		calTill.set(Calendar.MINUTE, 59);
		CalendarAppointmentRoadtrip nextApp = appointment.copy();
		do {
			appointments.add(nextApp);
			RepeatTypes.addDaysToCalendar(calStart, nextApp.getRepeatType());
			RepeatTypes.addDaysToCalendar(calEnd, nextApp.getRepeatType());
			nextApp = nextApp.copy();
			nextApp.setStart(calStart.getTime());
			nextApp.setEnd(calEnd.getTime());
		} while (calStart.before(calTill));

		return appointments;
	}

	private Date transformDateTime(final String dateString) {
		if (dateString.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{1}")) {
			final String[] dateTimeParts = dateString.split("\\D");
			final Calendar cal = Calendar.getInstance();
			cal.set(Integer.valueOf(dateTimeParts[0]), Integer.valueOf(dateTimeParts[1]) - 1, Integer.valueOf(dateTimeParts[2]), Integer.valueOf(dateTimeParts[3]), Integer.valueOf(dateTimeParts[4]), 0);
			return cal.getTime();
		}
		return null;
	}

	@Override
	public void deleteAppointment(final String appointmentId) {
		SQL.delete(SQLs.APPOINTMENT_DELETE, new NVPair("appointmentId", appointmentId));
	}

	@Override
	public void deleteThisAndFollowingAppointmentsInRepetition(final String appointmentRepeatId, final Date dateFrom) {
		SQL.delete(SQLs.APPOINTMENT_DELETE_BY_REPEAT_ID_AND_DATE, new NVPair("appointmentRepeatId", appointmentRepeatId), new NVPair("dateFrom", dateFrom));
	}

	@Override
	public void deleteAllAppointmentsInRepetition(final String appointmentRepeatId) {
		SQL.delete(SQLs.APPOINTMENT_DELETE_BY_REPEAT_ID, new NVPair("appointmentRepeatId", appointmentRepeatId));
	}

	@Override
	public Collection<CalendarAppointmentRoadtrip> getAppointmentsBetween(final Date startDate, final Date endDate) {
		final Object[][] appointmentObjects = SQL.select(SQLs.APPOINTMENT_ALL_ACTIVE_BETWEEN,
				new NVPair("startDate", CalendarUtil.setDateToStartTime(startDate)),
				new NVPair("endDate", CalendarUtil.setDateToEndTime(endDate)));
		return createCalendarAppointments(appointmentObjects, false, startDate, endDate);
	}

	@Override
	public void updateAppointment(final UpdateType updateType, final CalendarAppointmentRoadtrip oldApp, final CalendarAppointmentRoadtrip updatedApp) {
		switch (updateType) {
		case SINGLE:
			updateSingleAppointment(oldApp, updatedApp);
			break;
		case THIS_AND_FOLLOWING:
			updateThisAndFollowing(oldApp, updatedApp);
			break;

		default:
			break;
		}
	}

	private void updateSingleAppointment(final CalendarAppointmentRoadtrip oldApp, final CalendarAppointmentRoadtrip updatedApp) {
		if ( ! updatedApp.isRepeating()) {
			// before: not repeating
			// after: not repeating
			saveAppointment(updatedApp);
		} else if (updatedApp.isRepeating() && ! updatedApp.isRepeatTillInfinity()) {
			// before: not repeating
			// after: repeating, not repeat till infinity
			//update old Appointments
			updateExistingAppointmentToRepititionTillLastTime(oldApp, false);

			//insert new Appointment
			updatedApp.setRepeating(false);
			updatedApp.setRepeatTill(null);
			updatedApp.setRepeatType(null);
			saveAppointment(updatedApp);

			//insert repeat till from now
			createNewRepititionFromNextTime(oldApp);
		} else if (updatedApp.isRepeating() && updatedApp.isRepeatTillInfinity()) {
			// before: not repeating
			// after: repeating, repeat till infinity
			//update old Appointments
			updateExistingAppointmentToRepititionTillLastTime(oldApp, true);

			//insert updatedSingleAppointment
			updatedApp.setRepeatTill(null);
			updatedApp.setRepeating(false);
			updatedApp.setRepeatTillInfinity(false);
			updatedApp.setRepeatType(null);
			saveAppointment(updatedApp);

			//insert repeat till infinity from now
			createNewRepititionFromNextTime(oldApp);
		}
	}

	private void updateThisAndFollowing(final CalendarAppointmentRoadtrip oldApp, final CalendarAppointmentRoadtrip updatedApp) {
		if ( ! updatedApp.isRepeating()) {
			// before: not repeating
			// after: not repeating
			saveAppointment(updatedApp);
		} else if (updatedApp.isRepeating() && ! updatedApp.isRepeatTillInfinity()) {
			// before: not repeating
			// after: repeating, not repeat till infinity
			//update old Appointments
			updateExistingAppointmentToRepititionTillLastTime(oldApp, false);

			//insert repeat till from now
			createNewRepititionSinceGivenAppointment(updatedApp);
		} else if (updatedApp.isRepeating() && updatedApp.isRepeatTillInfinity()) {
			// before: not repeating
			// after: repeating, repeat till infinity
			//update old Appointments
			updateExistingAppointmentToRepititionTillLastTime(oldApp, true);

			//insert repeat till infinity from now
			createNewRepititionSinceGivenAppointment(updatedApp);
		}
	}

	private void updateExistingAppointmentToRepititionTillLastTime(final CalendarAppointmentRoadtrip oldApp, final boolean wasRepeatTillInfinity) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(CalendarUtil.setDateToEndTime(oldApp.getEnd()));
		RepeatTypes.substractDaysFromCalendar(cal, oldApp.getRepeatType());
		if (wasRepeatTillInfinity){
			updateInfinityAppointmentToRepeatTillLastTime(oldApp.getAppointmentRepeatId(), cal.getTime());
		} else {
			updateRepeatTillAppointmentRepeatTillDate(oldApp.getAppointmentRepeatId(), cal.getTime());
		}
	}

	private void createNewRepititionSinceGivenAppointment(final CalendarAppointmentRoadtrip updatedApp) {
		final CalendarAppointmentRoadtrip newRepition = updatedApp.copy();
		newRepition.setAppointmentRepeatId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_APPOINTMENT_REPEAT));

		saveAppointment(newRepition);
	}

	private void createNewRepititionFromNextTime(final CalendarAppointmentRoadtrip oldApp) {
		final CalendarAppointmentRoadtrip newRepition = oldApp.copy();
		final Calendar newCal = Calendar.getInstance();
		newCal.setTimeInMillis(newRepition.getStart().getTime());
		RepeatTypes.addDaysToCalendar(newCal, newRepition.getRepeatType());
		newRepition.setStart(newCal.getTime());

		newCal.setTimeInMillis(newRepition.getEnd().getTime());
		RepeatTypes.addDaysToCalendar(newCal, newRepition.getRepeatType());
		newRepition.setEnd(newCal.getTime());

		RepeatTypes.addDaysToCalendar(newCal, newRepition.getRepeatType());
		newRepition.setAppointmentRepeatId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_APPOINTMENT_REPEAT));
		saveAppointment(newRepition);
	}

	private void updateInfinityAppointmentToRepeatTillLastTime(final String appointmentRepeatId, final Date repeatTill) {
		SQL.update(SQLs.APPOINTMENT_UPDATE_INFINITY_TO_REPEAT_TILL_BY_APPOINTMENT_REPEAT_ID,
				new NVPair("appointmentRepeatId", appointmentRepeatId),
				new NVPair("isRepeatTillInfinity", false),
				new NVPair("repeatTill", repeatTill));
	}

	private void updateRepeatTillAppointmentRepeatTillDate(final String appointmentRepeatId, final Date repeatTill) {
		SQL.update(SQLs.APPOINTMENT_UPDATE_REPEAT_TILL_APPOINTMENT_REPEAT_TILL_DATE_BY_APPOINTMENT_REPEAT_ID,
				new NVPair("appointmentRepeatId", appointmentRepeatId),
				new NVPair("repeatTill", repeatTill));
	}
}
