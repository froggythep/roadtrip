package org.itam.roadtrip.server.route;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.itam.roadtrip.shared.route.AddTripFormData;
import org.itam.roadtrip.shared.route.CreateAddTripPermission;
import org.itam.roadtrip.shared.route.IAddTripService;
import org.itam.roadtrip.shared.route.ReadAddTripPermission;
import org.itam.roadtrip.shared.route.UpdateAddTripPermission;

public class AddTripService implements IAddTripService {

	@Override
	public AddTripFormData prepareCreate(AddTripFormData formData) {
		if (!ACCESS.check(new CreateAddTripPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}

	@Override
	public AddTripFormData create(AddTripFormData formData) {
		if (!ACCESS.check(new CreateAddTripPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}

	@Override
	public AddTripFormData load(AddTripFormData formData) {
		if (!ACCESS.check(new ReadAddTripPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}

	@Override
	public AddTripFormData store(AddTripFormData formData) {
		if (!ACCESS.check(new UpdateAddTripPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}
}
