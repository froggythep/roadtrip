package org.itam.roadtrip.server.sql;

import java.util.Set;

import javax.annotation.PostConstruct;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.CreateImmediately;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.platform.context.RunContext;
import org.eclipse.scout.rt.platform.exception.ExceptionHandler;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.platform.holders.StringArrayHolder;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.platform.util.concurrent.IRunnable;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// tag::service[]
@ApplicationScoped
@CreateImmediately
public class DatabaseSetupService implements IDataStoreService {
	private static final Logger LOG = LoggerFactory.getLogger(DatabaseSetupService.class);

	@PostConstruct
	public void autoCreateDatabase() {
		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoCreateProperty.class)) {
			try {
				final RunContext context = BEANS.get(SuperUserRunContextProducer.class).produce();
				final IRunnable runnable = new IRunnable() {

					@Override
					public void run() throws Exception {
						//						createIdsTable();
						//						createPersonTable();
						//						createRouteTable();
						//						createTripTable();
						//						createPersonRouteTable();
						//						createRouteTripTable();
						//						createRoomTable();
						//						createConfigurationTable();

						//						insertMandatoryData();
					}
				};

				context.run(runnable);
			}
			catch (final RuntimeException e) {
				BEANS.get(ExceptionHandler.class).handle(e);
			}
		}
	}

	public void createIdsTable() {
		SQL.insert(SQLs.CREATE_IDS_TABLE);
		SQL.insert(SQLs.IDS_TABLE_INSERT);
	}

	public void createPersonTable() {
		//		if (!getExistingTables().contains("PERSON")) {
		SQL.insert(SQLs.CREATE_PERSON_TABLE);
		LOG.info("Database table 'PERSON' created");

		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoPopulateProperty.class)) {
			SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_PRAXIS);
			SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_02);
			SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_03);
			SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_04);
			SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_05);
			SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_06);
			// tag::service[]
			LOG.info("Database table 'PERSON' populated with sample data");
		}
		//		}
	}

	public void createRouteTable() {
		//		if (!getExistingTables().contains("ROUTE")) {
		SQL.insert(SQLs.CREATE_ROUTE_TABLE);
		LOG.info("Database table 'ROUTE' created");

		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoPopulateProperty.class)) {
			SQL.insert(SQLs.ROUTE_INSERT_SAMPLE + SQLs.ROUTE_VALUES_01);
			SQL.insert(SQLs.ROUTE_INSERT_SAMPLE + SQLs.ROUTE_VALUES_02);
			SQL.insert(SQLs.ROUTE_INSERT_SAMPLE + SQLs.ROUTE_VALUES_03);
		}
		//		}
	}

	public void createTripTable() {
		//		if (!getExistingTables().contains("TRIP")) {
		SQL.insert(SQLs.CREATE_TRIP_TABLE);
		LOG.info("Database table 'TRIP' created");

		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoPopulateProperty.class)) {
			SQL.insert(SQLs.TRIP_INSERT_SAMPLE + SQLs.TRIP_INSERT_VALUES_01);
			SQL.insert(SQLs.TRIP_INSERT_SAMPLE + SQLs.TRIP_INSERT_VALUES_02);
			SQL.insert(SQLs.TRIP_INSERT_SAMPLE + SQLs.TRIP_INSERT_VALUES_03);
			SQL.insert(SQLs.TRIP_INSERT_SAMPLE + SQLs.TRIP_INSERT_VALUES_04);
			SQL.insert(SQLs.TRIP_INSERT_SAMPLE + SQLs.TRIP_INSERT_VALUES_05);
			SQL.insert(SQLs.TRIP_INSERT_SAMPLE + SQLs.TRIP_INSERT_VALUES_06);
		}
		//		}
	}

	public void createPersonRouteTable() {
		//		if (!getExistingTables().contains("PERSON_ROUTE")) {
		SQL.insert(SQLs.CREATE_PERSON_ROUTE_TABLE);
		LOG.info("Database table 'PERSON_ROUTE' created");

		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoPopulateProperty.class)) {
			SQL.insert(SQLs.PERSON_ROUTE_INSERT_SAMPLE + SQLs.PERSON_ROUTE_VALUES_01);
			SQL.insert(SQLs.PERSON_ROUTE_INSERT_SAMPLE + SQLs.PERSON_ROUTE_VALUES_02);
			SQL.insert(SQLs.PERSON_ROUTE_INSERT_SAMPLE + SQLs.PERSON_ROUTE_VALUES_03);
		}
		//		}
	}

	public void createRouteTripTable() {
		//		if (!getExistingTables().contains("ROUTE_TRIP")) {
		SQL.insert(SQLs.CREATE_ROUTE_TRIP_TABLE);
		LOG.info("Database table 'ROUTE_TRIP' created");

		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoPopulateProperty.class)) {
			SQL.insert(SQLs.ROUTE_TRIP_INSERT_SAMPLE + SQLs.ROUTE_TRIP_VALUES_01);
			SQL.insert(SQLs.ROUTE_TRIP_INSERT_SAMPLE + SQLs.ROUTE_TRIP_VALUES_02);
			SQL.insert(SQLs.ROUTE_TRIP_INSERT_SAMPLE + SQLs.ROUTE_TRIP_VALUES_03);
			SQL.insert(SQLs.ROUTE_TRIP_INSERT_SAMPLE + SQLs.ROUTE_TRIP_VALUES_04);
			SQL.insert(SQLs.ROUTE_TRIP_INSERT_SAMPLE + SQLs.ROUTE_TRIP_VALUES_05);
			SQL.insert(SQLs.ROUTE_TRIP_INSERT_SAMPLE + SQLs.ROUTE_TRIP_VALUES_06);
		}
		//		}
	}

	public void createConfigurationTable() {
		SQL.insert(SQLs.CREATE_CONFIGURATION_TABLE);
		LOG.info("Database table 'CONFIGURATION' created");

		SQL.insert(SQLs.CONFIGURATION_TABLE_INSERT);
	}

	public void createRoomTable() {
		//		if (!getExistingTables().contains("ROOM")) {
		SQL.insert(SQLs.CREATE_ROOM_TABLE);
		LOG.info("Database table 'ROOM' created");

		if (CONFIG.getPropertyValue(DatabaseProperties.DatabaseAutoPopulateProperty.class)) {
			SQL.insert(SQLs.ROOM_INSERT_SAMPLE + SQLs.ROOM_INSERT_VALUES_01);
			SQL.insert(SQLs.ROOM_INSERT_SAMPLE + SQLs.ROOM_INSERT_VALUES_02);
			SQL.insert(SQLs.ROOM_INSERT_SAMPLE + SQLs.ROOM_INSERT_VALUES_03);
			SQL.insert(SQLs.ROOM_INSERT_SAMPLE + SQLs.ROOM_INSERT_VALUES_04);
		}
		//		}
	}

	public void createAppointmentTable() {
		SQL.insert(SQLs.CREATE_APPOINTMENT_TABLE);
		LOG.info("Database table 'ROOM' created");
	}

	private Set<String> getExistingTables() {
		final StringArrayHolder tables = new StringArrayHolder();
		SQL.selectInto(SQLs.SELECT_TABLE_NAMES, new NVPair("result", tables)); // <1>
		return CollectionUtility.hashSet(tables.getValue());
	}

	public void insertMandatoryData() {
		SQL.insert(SQLs.PERSON_INSERT_SAMPLE + SQLs.PERSON_VALUES_PRAXIS);
	}
	// end::service[]

	@Override
	public void dropDataStore() {
		SQL.update(SQLs.DROP_PERSON_TABLE);
		SQL.update(SQLs.DROP_ROUTE_TABLE);
		SQL.update(SQLs.DROP_TRIP_TABLE);
		SQL.update(SQLs.DROP_PERSON_ROUTE_TABLE);
		SQL.update(SQLs.DROP_ROUTE_TRIP_TABLE);
		SQL.update(SQLs.DROP_ROOM_TABLE);
	}

	@Override
	public void createDataStore() {
		//		createPersonTable();
		//		createRouteTable();
		//		createTripTable();
		//		createPersonRouteTable();
		//		createRouteTripTable();
		//		createRoomTable();
		//		createConfigurationTable();
	}

	// tag::service[]
}
// end::service[]
