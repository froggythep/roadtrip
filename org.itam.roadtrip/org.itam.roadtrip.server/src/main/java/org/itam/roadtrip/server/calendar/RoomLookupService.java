package org.itam.roadtrip.server.calendar;

import org.eclipse.scout.rt.server.jdbc.lookup.AbstractSqlLookupService;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.calendar.IRoomLookupService;

public class RoomLookupService extends AbstractSqlLookupService<String> implements IRoomLookupService {

	@Override
	protected String getConfiguredSqlSelect() {
		return SQLs.ROOM_LOOKUP;
	}
}
