package org.itam.roadtrip.server.route;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.itam.roadtrip.server.ServerDefines;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.GlobalDefines;
import org.itam.roadtrip.shared.calendar.CalendarAppointmentRoadtrip;
import org.itam.roadtrip.shared.calendar.CalendarAppointmentSorter;
import org.itam.roadtrip.shared.calendar.CalendarUtil;
import org.itam.roadtrip.shared.calendar.ICalendarService;
import org.itam.roadtrip.shared.calendar.IRoomService;
import org.itam.roadtrip.shared.maps.MapsUtil;
import org.itam.roadtrip.shared.person.IPersonService;
import org.itam.roadtrip.shared.route.CreateRoutePermission;
import org.itam.roadtrip.shared.route.IRouteService;
import org.itam.roadtrip.shared.route.ReadRoutePermission;
import org.itam.roadtrip.shared.route.RouteFormData;
import org.itam.roadtrip.shared.route.RouteFormData.Trip;
import org.itam.roadtrip.shared.route.RouteFormData.Trip.TripRowData;
import org.itam.roadtrip.shared.route.RouteTablePageData;
import org.itam.roadtrip.shared.route.RouteTablePageData.RouteTableRowData;
import org.itam.roadtrip.shared.route.RouteTripFormData;
import org.itam.roadtrip.shared.route.UpdateRoutePermission;

public class RouteService implements IRouteService {

	@Override
	public RouteTablePageData getRouteTableData(final SearchFilter filter) {
		final RouteTablePageData pageData = new RouteTablePageData();

		final String sql = SQLs.ROUTE_GENERAL_ALL_ACTIVE + SQLs.ROUTE_GENERAL_ALL_ACTIVE_PAGE_DATA_SELECT_INTO;
		SQL.selectInto(sql, new NVPair("page", pageData));

		for (final RouteTableRowData rowData : pageData.getRows()) {
			final Object[][] additionalInfo = SQL.select(SQLs.ROUTE_ADDITIONAL_INFO_BY_ROUTE_ID, new NVPair("routeId", rowData.getRouteId()));
			if (additionalInfo.length > 0 && additionalInfo[0].length > 0) {
				rowData.setTotalDistance(String.valueOf(additionalInfo[0][0]).replace(".", ",") + " km");
				rowData.setTripCount(Long.valueOf(String.valueOf(additionalInfo[0][1])));
			} else {
				rowData.setTotalDistance("0");
				rowData.setTripCount(0L);
			}
		}

		return pageData;
	}

	@Override
	public RouteFormData prepareCreate(final RouteFormData formData) {
		if (!ACCESS.check(new CreateRoutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		return formData;
	}

	@Override
	public RouteFormData create(final RouteFormData formData) {
		if (!ACCESS.check(new CreateRoutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		if (StringUtility.isNullOrEmpty(formData.getRouteId())) {
			formData.setRouteId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_ROUTE));
		}

		SQL.insert(SQLs.ROUTE_INSERT, formData);

		return store(formData);
	}

	@Override
	public RouteFormData load(final RouteFormData formData) {
		if (!ACCESS.check(new ReadRoutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		SQL.selectInto(SQLs.ROUTE_SELECT, formData);

		SQL.selectInto(SQLs.TRIP_SELECT, formData);
		for (final TripRowData tripRow : formData.getTrip().getRows()) {
			final Object[][] addressInfo = SQL.select(SQLs.TRIP_SELECT_ADDRESS_PARTS, new NVPair("destinationId", tripRow.getDestinationId()));
			if (addressInfo.length > 0 && addressInfo[0].length == 3) {
				tripRow.setDestinationAddress(addressInfo[0][0] + ", " + addressInfo[0][1] + " " + addressInfo[0][2]);
			}
			System.out.println();
		}

		return formData;
	}


	@Override
	public RouteFormData store(final RouteFormData formData) {
		if (!ACCESS.check(new UpdateRoutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		SQL.delete(SQLs.PERSON_ROUTE_COMPLETE_DELETE_FOR_UPDATE_BY_ROUTE_ID, formData);
		SQL.insert(SQLs.PERSON_ROUTE_INSERT, formData);

		for (final TripRowData tripRow : formData.getTrip().getRows()) {
			if (StringUtility.isNullOrEmpty(tripRow.getTripId())) {
				tripRow.setTripId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_TRIP));

				SQL.delete(SQLs.TRIP_COMPLETE_DELETE_FOR_UPDATE_BY_TRIP_ID, tripRow);
				SQL.insert(SQLs.TRIP_INSERT, tripRow);
				SQL.update(SQLs.TRIP_UPDATE, tripRow);

				SQL.delete(SQLs.ROUTE_TRIP_COMPLETE_DELETE_FOR_UPDATE_BY_TRIP_ID, tripRow);
				SQL.insert(SQLs.ROUTE_TRIP_INSERT, new RouteTripFormData(formData.getRouteId(), tripRow.getTripId()));
			}
		}
		SQL.insert(SQLs.ROUTE_UPDATE, formData);

		return formData;
	}

	@Override
	public void deleteRoute(final String routeId) {
		SQL.delete(SQLs.ROUTE_DELETE, new NVPair("routeId", routeId));
		SQL.delete(SQLs.TRIP_DELETE_BY_ROUTE_ID, new NVPair("routeId", routeId));
	}

	@Override
	public LinkedList<String> getAddressesInOrderForRoute(final String routeId) {
		final Object[][] routeDatas = SQL.select(SQLs.ROUTE_DATA_FOR_MAPS, new NVPair("routeId", routeId));
		final LinkedList<String> addresses = new LinkedList<>();
		boolean first = true;
		for (final Object[] routeData : routeDatas) {
			if (first) {
				addresses.add(String.valueOf(routeData[0]) + ", " + String.valueOf(routeData[1]) + " " + String.valueOf(routeData[2]));
				addresses.add(String.valueOf(routeData[3]) + ", " + String.valueOf(routeData[4]) + " " + String.valueOf(routeData[5]));
				first = false;
			} else {
				addresses.add(String.valueOf(routeData[3]) + ", " + String.valueOf(routeData[4]) + " " + String.valueOf(routeData[5]));
			}
		}

		return addresses;
	}

	@Override
	public void generateRoutesFromAppointments(final Date startDate, final Date endDate) {
		final ICalendarService service = BEANS.get(ICalendarService.class);
		final Collection<CalendarAppointmentRoadtrip> appointments = service.getAppointmentsBetween(startDate, endDate);
		final CalendarAppointmentSorter calendarAppointmentSorter = new CalendarAppointmentSorter();

		final Map<String, Boolean> roomCache = getRoomCache();
		final Map<String, String> personCache = getPersonCache(appointments);

		final Map<String, Collection<CalendarAppointmentRoadtrip>> employeeSortedAppointments = getAppointmentMapSortedByEmployee(appointments);
		for (final String employeeId : employeeSortedAppointments.keySet()) {
			final Map<Date, Collection<CalendarAppointmentRoadtrip>> dateSortedAppointments = getAppointmentMapSortedByDate(employeeSortedAppointments.get(employeeId));
			for (final Date date : dateSortedAppointments.keySet()) {
				final List<CalendarAppointmentRoadtrip> appointmentsForEmployeeAndDate = new ArrayList<>();
				for (final CalendarAppointmentRoadtrip app : dateSortedAppointments.get(date)) {
					if ( ! app.isCancelled() && ! app.isFullDay()) {
						appointmentsForEmployeeAndDate.add(app);
					}
				}
				Collections.sort(appointmentsForEmployeeAndDate, calendarAppointmentSorter);
				final RouteFormData routeFormData = createRouteFormData(employeeId, date, appointmentsForEmployeeAndDate, roomCache, personCache);
				create(routeFormData);
			}
		}
	}

	private RouteFormData createRouteFormData(final String employeeId, final Date date, final List<CalendarAppointmentRoadtrip> appointments,
			final Map<String, Boolean> roomCache, final Map<String, String> personCache) {
		final RouteFormData formData = new RouteFormData();
		formData.getEmployee().setValue(employeeId);
		formData.getOrigin().setValue(GlobalDefines.PRAXIS_PERSON_ID);
		formData.getRouteDate().setValue(CalendarUtil.setDateToMidday(date));
		formData.setRecordstatusId(GlobalDefines.RECORDSTATUS_ACTIVE);

		final TripRowData[] rows = new TripRowData[appointments.size()+1];
		int numberInRoute = 1;
		String lastDestination = "";
		for (final CalendarAppointmentRoadtrip calendarAppointmentRoadtrip : appointments) {
			final TripRowData rowData = new TripRowData();
			if (numberInRoute == 1) {
				rowData.setOriginId(GlobalDefines.PRAXIS_PERSON_ID);
			} else {
				rowData.setOriginId(lastDestination);
			}

			if (roomCache.get(calendarAppointmentRoadtrip.getRoomId())) {
				lastDestination = GlobalDefines.PRAXIS_PERSON_ID;
			} else {
				lastDestination = calendarAppointmentRoadtrip.getPatientId();
			}
			rowData.setDestinationId(lastDestination);

			rowData.setNumberInRoute(numberInRoute);

			rowData.setDistance(String.valueOf(MapsUtil.getDistanceForTrip(personCache.get(rowData.getOriginId()), personCache.get(rowData.getDestinationId()))));

			rowData.setRecordstatusId(GlobalDefines.RECORDSTATUS_ACTIVE);

			rows[numberInRoute-1] = rowData;
			numberInRoute++;
		}
		final TripRowData tripToPraxis = new TripRowData();
		tripToPraxis.setOriginId(lastDestination);
		tripToPraxis.setDestinationId(GlobalDefines.PRAXIS_PERSON_ID);
		tripToPraxis.setNumberInRoute(numberInRoute);
		tripToPraxis.setDistance(String.valueOf(MapsUtil.getDistanceForTrip(personCache.get(tripToPraxis.getOriginId()), personCache.get(tripToPraxis.getDestinationId()))));
		tripToPraxis.setRecordstatusId(GlobalDefines.RECORDSTATUS_ACTIVE);
		rows[numberInRoute-1] = tripToPraxis;

		final Trip trip = formData.getTrip();
		trip.setRows(rows);

		return formData;
	}

	private Map<String, Boolean> getRoomCache() {
		final IRoomService roomService = BEANS.get(IRoomService.class);
		final Map<String, Boolean> roomCache = roomService.getRoomInhouseMap();
		return roomCache;
	}

	private Map<String, String> getPersonCache(final Collection<CalendarAppointmentRoadtrip> employeeSortedAppointments) {
		final Set<String> personIds = new HashSet<>();
		personIds.add(GlobalDefines.PRAXIS_PERSON_ID);
		for (final CalendarAppointmentRoadtrip car : employeeSortedAppointments) {
			personIds.add(car.getEmployeeId());
			personIds.add(car.getPatientId());
		}

		final IPersonService personService = BEANS.get(IPersonService.class);
		final Map<String, String> personCache = personService.getAddressStringsForPersons(personIds);
		return personCache;
	}

	private Map<String, Collection<CalendarAppointmentRoadtrip>> getAppointmentMapSortedByEmployee(final Collection<CalendarAppointmentRoadtrip> appointments) {
		final Map<String, Collection<CalendarAppointmentRoadtrip>> employeeSortedAppointments = new HashMap<>();
		for (final CalendarAppointmentRoadtrip calendarAppointmentRoadtrip : appointments) {
			final String employeeId = calendarAppointmentRoadtrip.getEmployeeId();
			if (employeeSortedAppointments.containsKey(employeeId)) {
				employeeSortedAppointments.get(employeeId).add(calendarAppointmentRoadtrip);
			} else {
				final Collection<CalendarAppointmentRoadtrip> col = new HashSet<>();
				col.add(calendarAppointmentRoadtrip);
				employeeSortedAppointments.put(employeeId, col);
			}
		}
		return employeeSortedAppointments;
	}

	private Map<Date, Collection<CalendarAppointmentRoadtrip>> getAppointmentMapSortedByDate(final Collection<CalendarAppointmentRoadtrip> appointments) {
		final Map<Date, Collection<CalendarAppointmentRoadtrip>> dateSortedAppointments = new HashMap<>();
		for (final CalendarAppointmentRoadtrip calendarAppointmentRoadtrip : appointments) {
			final Date date = CalendarUtil.setDateToStartTime(calendarAppointmentRoadtrip.getStart());
			if (dateSortedAppointments.containsKey(date)) {
				dateSortedAppointments.get(date).add(calendarAppointmentRoadtrip);
			} else {
				final Collection<CalendarAppointmentRoadtrip> col = new HashSet<>();
				col.add(calendarAppointmentRoadtrip);
				dateSortedAppointments.put(date, col);
			}
		}
		return dateSortedAppointments;
	}
}
