package org.itam.roadtrip.server;

import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.itam.roadtrip.server.sql.SQLs;

public class ServerDefines {

	public static final String TABLE_NAME_PERSON = "PERSON";
	public static final String TABLE_NAME_ROOM = "ROOM";
	public static final String TABLE_NAME_ROUTE = "ROUTE";
	public static final String TABLE_NAME_TRIP = "TRIP";
	public static final String TABLE_NAME_APPOINTMENT = "APPOINTMENT";
	public static final String TABLE_NAME_APPOINTMENT_REPEAT = "APPOINTMENT_REPEAT";


	public static String getNextId(final String tableName) {
		final String nextId = String.valueOf(SQL.select(SQLs.IDS_GET_NEXT_ID_BY_NAME, new NVPair("name", tableName))[0][0]);
		SQL.update(SQLs.IDS_UPDATE_NEXT_ID_BY_NAME, new NVPair("name", tableName), new NVPair("updatedId", getUpdatedId(nextId)));
		return nextId;
	}

	private static String getUpdatedId(final String id) {
		int intId = Integer.valueOf(id);
		intId++;
		return String.valueOf(intId);
	}

}
