package org.itam.roadtrip.server.sql;

public interface IDataStoreService {

	void dropDataStore();

	void createDataStore();
}
