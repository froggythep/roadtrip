package org.itam.roadtrip.server.sql;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.server.jdbc.mysql.AbstractMySqlSqlService;

@Order(1940)
public class MySqlSqlService extends AbstractMySqlSqlService {

	@Override
	protected String getConfiguredJdbcMappingName() {
		return CONFIG.getPropertyValue(DatabaseProperties.JdbcMappingNameProperty.class);
	}

	@Override
	protected String getConfiguredUsername() {
		return CONFIG.getPropertyValue(DatabaseProperties.JdbcMappingUsernameProperty.class);
	}

	@Override
	protected String getConfiguredPassword() {
		return CONFIG.getPropertyValue(DatabaseProperties.JdbcMappingPasswordProperty.class);
	}
}
