package org.itam.roadtrip.server.sql;

public abstract class SqlWriter {

	public static void main(final String[] args) {
		System.out.println(SQLs.CREATE_IDS_TABLE);
		System.out.println(SQLs.IDS_TABLE_INSERT);
		System.out.println(SQLs.CREATE_PERSON_TABLE);
		System.out.println(SQLs.CREATE_ROUTE_TABLE);
		System.out.println(SQLs.CREATE_TRIP_TABLE);
		System.out.println(SQLs.CREATE_ROOM_TABLE);
		System.out.println(SQLs.CREATE_PERSON_ROUTE_TABLE);
		System.out.println(SQLs.CREATE_ROUTE_TRIP_TABLE);
		System.out.println(SQLs.CREATE_APPOINTMENT_TABLE);
		System.out.println(SQLs.CREATE_CONFIGURATION_TABLE);
		System.out.println(SQLs.CONFIGURATION_TABLE_INSERT);
	}

}
