package org.itam.roadtrip.server.calendar;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.itam.roadtrip.shared.calendar.AppointmentFormData;
import org.itam.roadtrip.shared.calendar.CreateAppointmentPermission;
import org.itam.roadtrip.shared.calendar.IAppointmentService;
import org.itam.roadtrip.shared.calendar.ReadAppointmentPermission;
import org.itam.roadtrip.shared.calendar.UpdateAppointmentPermission;

public class AppointmentService implements IAppointmentService {

	@Override
	public AppointmentFormData prepareCreate(AppointmentFormData formData) {
		if (!ACCESS.check(new CreateAppointmentPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}

	@Override
	public AppointmentFormData create(AppointmentFormData formData) {
		if (!ACCESS.check(new CreateAppointmentPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}

	@Override
	public AppointmentFormData load(AppointmentFormData formData) {
		if (!ACCESS.check(new ReadAppointmentPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}

	@Override
	public AppointmentFormData store(AppointmentFormData formData) {
		if (!ACCESS.check(new UpdateAppointmentPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [Andi] add business logic here.
		return formData;
	}
}
