package org.itam.roadtrip.server.calendar;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.itam.roadtrip.server.ServerDefines;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.calendar.CreateRoomPermission;
import org.itam.roadtrip.shared.calendar.IRoomService;
import org.itam.roadtrip.shared.calendar.ReadRoomPermission;
import org.itam.roadtrip.shared.calendar.RoomFormData;
import org.itam.roadtrip.shared.calendar.RoomTablePageData;
import org.itam.roadtrip.shared.calendar.UpdateRoomPermission;

public class RoomService implements IRoomService {

	@Override
	public RoomTablePageData getRoomTableData(final SearchFilter filter) {
		final RoomTablePageData pageData = new RoomTablePageData();
		final String sql = SQLs.ROOM_ALL_ACTIVE + SQLs.ROOM_ALL_ACTIVE_PAGE_DATA_SELECT_INTO;
		SQL.selectInto(sql, new NVPair("page", pageData));
		return pageData;
	}

	@Override
	public RoomFormData prepareCreate(final RoomFormData formData) {
		if (!ACCESS.check(new CreateRoomPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		return formData;
	}

	@Override
	public RoomFormData create(final RoomFormData formData) {
		if (!ACCESS.check(new CreateRoomPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		if (StringUtility.isNullOrEmpty(formData.getRoomId())) {
			formData.setRoomId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_ROOM));
		}

		SQL.insert(SQLs.ROOM_INSERT, formData);
		return store(formData);
	}

	@Override
	public RoomFormData load(final RoomFormData formData) {
		if (!ACCESS.check(new ReadRoomPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		SQL.selectInto(SQLs.ROOM_SELECT, formData);
		return formData;
	}

	@Override
	public RoomFormData store(final RoomFormData formData) {
		if (!ACCESS.check(new UpdateRoomPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		SQL.update(SQLs.ROOM_UPDATE, formData);
		return formData;
	}

	@Override
	public void deleteRoom(final String roomId) {
		SQL.delete(SQLs.ROOM_DELETE, new NVPair("roomId", roomId));
	}

	@Override
	public String getRoomColorString(final String roomId) {
		final Object[][] colorString = SQL.select(SQLs.ROOM_COLOR_BY_ID, new NVPair("roomId", roomId));
		if ( ! StringUtility.isNullOrEmpty(String.valueOf(colorString[0][0]))) {
			return String.valueOf(colorString[0][0]);
		}
		return "";
	}

	@Override
	public Map<String, Boolean> getRoomInhouseMap() {
		final Map<String, Boolean> roomInhouseMap = new HashMap<>();

		final Object[][] rooms = SQL.select(SQLs.ROOM_ID_AND_INHOUSE);
		for (final Object[] room : rooms) {
			roomInhouseMap.put(String.valueOf(room[0]), Integer.valueOf(String.valueOf(room[1])) == 0 ? Boolean.FALSE : Boolean.TRUE);
		}
		return roomInhouseMap;
	}
}
