package org.itam.roadtrip.server.sql;

public interface SQLs {

	String SELECT_TABLE_NAMES = ""
			+ "SELECT   UPPER(tablename) "
			+ "FROM     sys.systables "
			+ "INTO     :result";

	String CREATE_IDS_TABLE = ""
			+ "CREATE TABLE IDS "
			+ "	(NAME VARCHAR(50) NOT NULL, "
			+ "	 NEXT_ID VARCHAR(10) NOT NULL, "
			+ "	 PRIMARY KEY (NAME)"
			+ "	)";

	String IDS_TABLE_INSERT = ""
			+ "INSERT INTO IDS (NAME, NEXT_ID) "
			+ "VALUES ('PERSON', '1'), "
			+ "		  ('ROOM', '1'), "
			+ "		  ('ROUTE', '1'), "
			+ "		  ('TRIP', '1'), "
			+ "		  ('APPOINTMENT', '1'), "
			+ "		  ('APPOINTMENT_REPEAT', '1')";

	String IDS_GET_NEXT_ID_BY_NAME = ""
			+ "SELECT NEXT_ID FROM IDS WHERE NAME = :name";

	String IDS_UPDATE_NEXT_ID_BY_NAME = ""
			+ "UPDATE IDS SET NEXT_ID = :updatedId WHERE NAME = :name";

	String CREATE_PERSON_TABLE = ""
			+ "CREATE TABLE PERSON "
			+ " (PERSON_ID VARCHAR(50) NOT NULL, "
			+ "  NAME VARCHAR(100), "
			+ "  SIRNAME VARCHAR(100), "
			+ "  BIRTHDAY DATE, "
			+ "  STREET VARCHAR(100), "
			+ "  PLZ VARCHAR(5), "
			+ "  CITY VARCHAR(50), "
			+ "  POSITION VARCHAR(20), "
			+ "  HEALTHINSURANCE VARCHAR(100), "
			+ "  PHONENUMBER VARCHAR(20), "
			+ "  RECORDSTATUS_ID INTEGER, "
			+ "  PRIMARY KEY (PERSON_ID)"
			+ " )";

	String CREATE_ROUTE_TABLE = ""
			+ "CREATE TABLE ROUTE "
			+ " (ROUTE_ID VARCHAR(50) NOT NULL, "
			+ "  ROUTE_DATE DATE, "
			+ "  ORIGIN_ID VARCHAR(50), "
			+ "  RECORDSTATUS_ID INTEGER, "
			+ "  PRIMARY KEY (ROUTE_ID)"
			+ " )";

	String CREATE_TRIP_TABLE = ""
			+ "CREATE TABLE TRIP "
			+ " (TRIP_ID VARCHAR(50) NOT NULL, "
			+ "  ORIGIN VARCHAR(50), "
			+ "  DESTINATION VARCHAR(50), "
			+ "  NUMBER_IN_ROUTE INTEGER, "
			+ "  DISTANCE DOUBLE, "
			+ "  RECORDSTATUS_ID INTEGER, "
			+ "  PRIMARY KEY (TRIP_ID)"
			+ " )";

	String CREATE_ROOM_TABLE = ""
			+ "CREATE TABLE ROOM "
			+ " (ROOM_ID VARCHAR(50) NOT NULL, "
			+ "  NAME VARCHAR(100), "
			+ "	 IS_INHOUSE INTEGER DEFAULT 0, "
			+ "	 COLOR VARCHAR(20), "
			+ "  RECORDSTATUS_ID INTEGER, "
			+ "  PRIMARY KEY(ROOM_ID)"
			+ " )";

	String CREATE_PERSON_ROUTE_TABLE = ""
			+ "CREATE TABLE PERSON_ROUTE "
			+ " (PERSON_ID VARCHAR(50) NOT NULL, "
			+ "  ROUTE_ID VARCHAR(50) NOT NULL, "
			+ "  PRIMARY KEY (PERSON_ID, ROUTE_ID)"
			+ " )";

	String CREATE_ROUTE_TRIP_TABLE = ""
			+ "CREATE TABLE ROUTE_TRIP "
			+ " (ROUTE_ID VARCHAR(50) NOT NULL, "
			+ "  TRIP_ID VARCHAR(50) NOT NULL, "
			+ "  PRIMARY KEY (ROUTE_ID, TRIP_ID) "
			+ " )";

	String CREATE_APPOINTMENT_TABLE = ""
			+ "CREATE TABLE APPOINTMENT ( "
			+ "		APPOINTMENT_ID VARCHAR(50) NOT NULL, "
			+ "		EMPLOYEE_ID VARCHAR(50) , "
			+ "		PATIENT_ID VARCHAR(50) , "
			+ "		ROOM_ID VARCHAR(50) , "
			+ "		IS_FULLDAY INTEGER  DEFAULT 0, "
			+ "		DATE_FROM DATETIME , "
			+ "		DATE_TO DATETIME , "
			+ "		IS_REPEATING INTEGER , "
			+ "		APPOINTMENT_REPEAT_ID VARCHAR(50) , "
			+ "		REPEATTYPE VARCHAR(20), "
			+ "		IS_REPEATTILLINFINITY INTEGER, "
			+ "		REPEATTILL DATETIME, "
			+ "		NOTICE VARCHAR(4000), "
			+ "		IS_CANCELLED INTEGER  DEFAULT 0, "
			+ "		RECORDSTATUS_ID INTEGER, "
			+ "		PRIMARY KEY (APPOINTMENT_ID)"
			+ "	)";

	String CREATE_CONFIGURATION_TABLE = ""
			+ "CREATE TABLE CONFIGURATION "
			+ "		 (CONFIGURATION_KEY VARCHAR(50) NOT NULL, "
			+ "		  CONFIGURATION_VALUE VARCHAR(255), "
			+ "		 PRIMARY KEY (CONFIGURATION_KEY)"
			+ "	)";

	String CONFIGURATION_TABLE_INSERT = ""
			+ "INSERT INTO CONFIGURATION (CONFIGURATION_KEY, CONFIGURATION_VALUE) "
			+ "VALUES ('EXPORT_PATH', 'C:\\roadtrip\\exports'), "
			+ "		  ('GOOGLE_MAPS_API_KEY', 'AIzaSyC57DmNzlUP5KAfulHZOGqFQrJkjaQqJWM')";

	String DROP_PERSON_TABLE = "DROP TABLE PERSON";
	String DROP_ROUTE_TABLE = "DROP TABLE ROUTE";
	String DROP_TRIP_TABLE = "DROP TABLE TRIP";
	String DROP_PERSON_ROUTE_TABLE = "DROP TABLE PERSON_ROUTE";
	String DROP_ROUTE_TRIP_TABLE = "DROP TABLE ROUTE_TRIP";
	String DROP_ROOM_TABLE = "DROP TABLE ROOM";
	String DROP_CONFIGURATION_TABLE = "DROP TABLE CONFIGURATION";

	//**************************************************************************
	// *** 						PERSON DATA									 ***
	// *************************************************************************

	String PERSON_INSERT = ""
			+ "INSERT   INTO "
			+ "PERSON  (person_id) "
			+ "VALUES   (:personId)";

	String PERSON_DELETE= ""
			+ "UPDATE PERSON "
			+ "SET RECORDSTATUS_ID = 0 "
			+ "WHERE PERSON_ID LIKE :personId ";

	String PERSON_SELECT = ""
			+ "SELECT   PERSON_ID, "
			+ "         NAME, "
			+ "         SIRNAME, "
			+ "         BIRTHDAY, "
			+ "         STREET, "
			+ "         PLZ, "
			+ "         CITY, "
			+ "         POSITION, "
			+ "			HEALTHINSURANCE, "
			+ "			PHONENUMBER "
			+ "FROM     PERSON "
			+ "WHERE    PERSON_ID = :personId "
			+ "INTO     :personId, "
			+ "         :name, "
			+ "         :sirname, "
			+ "         :birthday, "
			+ "         :street, "
			+ "         :plz, "
			+ "         :city, "
			+ "         :positionGroup, "
			+ "			:healthInsurance, "
			+ " 		:phoneNumber ";

	String PERSON_UPDATE = ""
			+ "UPDATE   PERSON "
			+ "SET      PERSON_ID = :personId, "
			+ "         NAME = :name, "
			+ "         SIRNAME = :sirname, "
			+ "         BIRTHDAY = :birthday, "
			+ "         STREET  = :street, "
			+ "         PLZ = :plz, "
			+ "         CITY = :city, "
			+ "         POSITION = :positionGroup, "
			+ "			HEALTHINSURANCE = :healthInsurance, "
			+ "			PHONENUMBER = :phoneNumber, "
			+ "			RECORDSTATUS_ID = :recordstatusId "
			+ "WHERE    person_id = :personId";

	String PERSON_ALL_ACTIVE = ""
			+ "SELECT   PERSON_ID, "
			+ "         NAME, "
			+ "         SIRNAME, "
			+ "         BIRTHDAY, "
			+ "         STREET, "
			+ "         PLZ, "
			+ "         CITY, "
			+ "         POSITION, "
			+ "			HEALTHINSURANCE, "
			+ "			PHONENUMBER, "
			+ "         RECORDSTATUS_ID "
			+ "FROM     PERSON "
			+ "WHERE	RECORDSTATUS_ID = 1";

	String PERSON_ALL_ACTIVE_PAGE_DATA_SELECT_INTO = ""
			+ "INTO     :{page.personId}, "
			+ "         :{page.name}, "
			+ "         :{page.sirname}, "
			+ "         :{page.birthday}, "
			+ "         :{page.street}, "
			+ "         :{page.plz}, "
			+ "         :{page.city}, "
			+ "         :{page.position}, "
			+ "			:{page.healthInsurance}, "
			+ "			:{page.phoneNumber}, "
			+ "			:{page.recordstatusId}";

	String PERSON_ADDRESS_PARTS = ""
			+ "SELECT 	PERSON_ID, STREET, PLZ, CITY "
			+ "FROM 	PERSON "
			+ "WHERE 	RECORDSTATUS_ID = 1 "
			+ "AND		PERSON_ID = :personIds";

	String EMPLOYEE_LOOKUP = ""
			+ "SELECT PERSON_ID AS PERSONID, NAME, SIRNAME, STREET, PLZ, CITY "
			+ "FROM PERSON "
			+ "WHERE POSITION LIKE 'Employee' "
			+ "AND RECORDSTATUS_ID = 1 "
			+ "<text> AND (UPPER(NAME) LIKE UPPER(:text||'%') OR UPPER(SIRNAME) LIKE UPPER(:text||'%') OR UPPER(STREET) LIKE UPPER(:text||'%') OR UPPER(PLZ) LIKE UPPER(:text||'%') OR UPPER(CITY) LIKE UPPER(:text||'%')) </text> "
			+ "<key> AND (PERSON_ID = :key) </key> "
			+ "<all></all> ";

	String PERSON_LOOKUP = ""
			+ "SELECT PERSON_ID AS PERSONID, NAME, SIRNAME, STREET, PLZ, CITY "
			+ "FROM PERSON "
			+ "WHERE 1 = 1 "
			+ "AND RECORDSTATUS_ID = 1"
			+ "<text> AND (UPPER(NAME) LIKE UPPER(:text||'%') OR UPPER(SIRNAME) LIKE UPPER(:text||'%') OR UPPER(STREET) LIKE UPPER(:text||'%') OR UPPER(PLZ) LIKE UPPER(:text||'%') OR UPPER(CITY) LIKE UPPER(:text||'%')) </text> "
			+ "<key> AND (PERSON_ID = :key) </key> "
			+ "<all></all>";

	String PERSON_INSERT_SAMPLE = ""
			+ "INSERT   INTO PERSON "
			+ "         (PERSON_ID, "
			+ "          NAME, "
			+ "          SIRNAME, "
			+ "          BIRTHDAY, "
			+ "          STREET, "
			+ "          PLZ, "
			+ "          CITY, "
			+ "          POSITION, "
			+ "			 HEALTHINSURANCE, "
			+ "			 RECORDSTATUS_ID) ";


	String PERSON_VALUES_PRAXIS = ""
			+ "VALUES   ('1', "
			+ "          'Praxis', "
			+ "          '', "
			+ "          '1970-01-01', "
			+ "          'Martin-Huber-Straße 1', "
			+ "          '85221', "
			+ "          'Dachau', "
			+ "          'Praxis', "
			+ "			 'KK', "
			+ "			 1) ";

	String PERSON_VALUES_02 = ""
			+ "VALUES   ('2', "
			+ "          'Margarete', "
			+ "          'Nitschel', "
			+ "          '1954-03-12', "
			+ "          'Hochstraße 3', "
			+ "          '85221', "
			+ "          'Dachau', "
			+ "          'Patient', "
			+ "			 1) ";

	String PERSON_VALUES_03 = ""
			+ "VALUES   ('3', "
			+ "          'Hildegard', "
			+ "          'Drumstick', "
			+ "          '1944-05-01', "
			+ "          'Am Stögnfeld 11a', "
			+ "          '85224', "
			+ "          'Röhrmoos', "
			+ "          'Patient', "
			+ "			 'KK2', "
			+ "			 1) ";

	String PERSON_VALUES_04 = ""
			+ "VALUES   ('4', "
			+ "          'Rüdiger', "
			+ "          'Nibbl', "
			+ "          '1932-03-07', "
			+ "          'Weiherweg 18', "
			+ "          '85229', "
			+ "          'Markt Indersdorf', "
			+ "          'Patient', "
			+ "			 'KK3', "
			+ "			 1) ";

	String PERSON_VALUES_05 = ""
			+ "VALUES   ('5', "
			+ "          'Julia', "
			+ "          'Mitarbeiter', "
			+ "          '1985-05-03', "
			+ "          'Schleißheimer Straße 10', "
			+ "          '85221', "
			+ "          'Dachau', "
			+ "          'Employee', "
			+ "			 'KK4', "
			+ "			 1) ";

	String PERSON_VALUES_06 = ""
			+ "VALUES   ('6', "
			+ "          'Michael', "
			+ "          'Hofmann', "
			+ "          '1987-12-22', "
			+ "          'Pasenbacher Straße 1', "
			+ "          '85224', "
			+ "          'Röhrmoos', "
			+ "          'Employee', "
			+ "			 '', "
			+ "			 1) ";


	//**************************************************************************
	// *** 						ROUTE DATA									 ***
	// *************************************************************************

	String ROUTE_INSERT = ""
			+ "INSERT   INTO "
			+ "ROUTE  (route_id) "
			+ "VALUES   (:routeId)";

	String ROUTE_DELETE= ""
			+ "UPDATE ROUTE "
			+ "SET RECORDSTATUS_ID = 0 "
			+ "WHERE ROUTE_ID LIKE :routeId ";

	String ROUTE_SELECT = ""
			+ "SELECT   ROUTE.ROUTE_ID, "
			+ "         ROUTE.ROUTE_DATE, "
			+ "			ROUTE.ORIGIN_ID, "
			+ "         ROUTE.RECORDSTATUS_ID, "
			+ "			PERSON.PERSON_ID "
			+ "FROM     ROUTE "
			+ "INNER JOIN PERSON_ROUTE ON ROUTE.ROUTE_ID = PERSON_ROUTE.ROUTE_ID "
			+ "INNER JOIN PERSON ON PERSON_ROUTE.PERSON_ID = PERSON.PERSON_ID "
			+ "WHERE    ROUTE.ROUTE_ID = :routeId "
			+ "INTO     :routeId, "
			+ "         :routeDate, "
			+ "			:origin, "
			+ "         :recordstatusId, "
			+ "			:employee ";

	String ROUTE_UPDATE = ""
			+ "UPDATE   ROUTE "
			+ "SET      ROUTE_DATE = :routeDate, "
			+ "			ORIGIN_ID = :origin, "
			+ "			RECORDSTATUS_ID = :recordstatusId "
			+ "WHERE    ROUTE_ID = :routeId";

	String ROUTE_GENERAL_ALL_ACTIVE = ""
			+ "SELECT   ROUTE.ROUTE_ID, "
			+ "         ROUTE.ROUTE_DATE, "
			+ "         PERSON.NAME, "
			+ "         PERSON.SIRNAME, "
			+ "			ROUTE.RECORDSTATUS_ID "
			+ "FROM     ROUTE "
			+ "INNER JOIN PERSON_ROUTE ON ROUTE.ROUTE_ID = PERSON_ROUTE.ROUTE_ID "
			+ "INNER JOIN PERSON ON PERSON_ROUTE.PERSON_ID = PERSON.PERSON_ID "
			+ "WHERE ROUTE.RECORDSTATUS_ID = 1 ";

	String ROUTE_GENERAL_ALL_ACTIVE_PAGE_DATA_SELECT_INTO = ""
			+ "INTO     :{page.routeId}, "
			+ "         :{page.date}, "
			+ "         :{page.name}, "
			+ "         :{page.sirname}, "
			+ "			:{page.recordstatusId} ";

	String ROUTE_ADDITIONAL_INFO_BY_ROUTE_ID = ""
			+ "SELECT SUM(TRIP.DISTANCE), COUNT(ROUTE.ROUTE_ID) FROM ROUTE "
			+ "INNER JOIN ROUTE_TRIP ON ROUTE.ROUTE_ID = ROUTE_TRIP.ROUTE_ID "
			+ "INNER JOIN TRIP ON ROUTE_TRIP.TRIP_ID = TRIP.TRIP_ID AND TRIP.RECORDSTATUS_ID = 1 "
			+ "WHERE ROUTE.ROUTE_ID = :routeId "
			+ "GROUP BY ROUTE.ROUTE_ID";

	String ROUTE_DATA_FOR_MAPS = ""
			+ " SELECT ORIGIN.STREET, ORIGIN.PLZ, ORIGIN.CITY, DESTINATION.STREET, DESTINATION.PLZ, DESTINATION.CITY FROM ROUTE "
			+ " INNER JOIN ROUTE_TRIP ON ROUTE.ROUTE_ID = ROUTE_TRIP.ROUTE_ID "
			+ " INNER JOIN TRIP ON ROUTE_TRIP.TRIP_ID = TRIP.TRIP_ID AND TRIP.RECORDSTATUS_ID = 1 "
			+ " INNER JOIN PERSON AS ORIGIN ON TRIP.ORIGIN = ORIGIN.PERSON_ID "
			+ " INNER JOIN PERSON AS DESTINATION ON TRIP.DESTINATION = DESTINATION.PERSON_ID "
			+ " WHERE ROUTE.ROUTE_ID = :routeId "
			+ " AND ROUTE.RECORDSTATUS_ID = 1 "
			+ " ORDER BY NUMBER_IN_ROUTE ASC ";

	String ROUTE_INSERT_SAMPLE = ""
			+ "INSERT   INTO ROUTE "
			+ "         (ROUTE_ID, "
			+ "          ROUTE_DATE, "
			+ "			 RECORDSTATUS_ID) ";

	String ROUTE_VALUES_01 = ""
			+ "VALUES   ('1', "
			+ "          '2017-07-18', "
			+ "			 1) ";

	String ROUTE_VALUES_02 = ""
			+ "VALUES   ('2', "
			+ "          '2017-07-16', "
			+ "			 1) ";

	String ROUTE_VALUES_03 = ""
			+ "VALUES   ('3', "
			+ "          '2017-06-02', "
			+ "			 1) ";

	//**************************************************************************
	// *** 						PERSON ROUTE DATA							 ***
	// *************************************************************************
	String PERSON_ROUTE_COMPLETE_DELETE_FOR_UPDATE_BY_ROUTE_ID = ""
			+ "DELETE FROM PERSON_ROUTE WHERE ROUTE_ID = :routeId";

	String PERSON_ROUTE_INSERT = ""
			+ "INSERT   INTO "
			+ "PERSON_ROUTE  (person_id, route_id) "
			+ "VALUES   (:employee, :routeId)";

	String PERSON_ROUTE_INSERT_SAMPLE = ""
			+ "INSERT   INTO PERSON_ROUTE "
			+ "         (PERSON_ID, "
			+ "          ROUTE_ID) ";

	String PERSON_ROUTE_VALUES_01 = ""
			+ "VALUES   ('1', "
			+ "          '1') ";

	String PERSON_ROUTE_VALUES_02 = ""
			+ "VALUES   ('5', "
			+ "          '2') ";

	String PERSON_ROUTE_VALUES_03 = ""
			+ "VALUES   ('5', "
			+ "          '3') ";

	//**************************************************************************
	// *** 						TRIP DATA									 ***
	// *************************************************************************
	String TRIP_COMPLETE_DELETE_FOR_UPDATE_BY_TRIP_ID = ""
			+ "DELETE FROM TRIP WHERE TRIP_ID = :tripId";

	String TRIP_INSERT = ""
			+ "INSERT   INTO "
			+ "TRIP  (trip_id) "
			+ "VALUES   (:tripId)";

	String TRIP_DELETE = ""
			+ "UPDATE TRIP "
			+ "SET RECORDSTATUS_ID = 0 "
			+ "WHERE TRIP_ID LIKE :tripId ";

	String TRIP_DELETE_BY_ROUTE_ID = ""
			+ "UPDATE TRIP, ROUTE_TRIP "
			+ "SET RECORDSTATUS_ID = 0 "
			+ "WHERE TRIP.TRIP_ID = ROUTE_TRIP.TRIP_ID "
			+ "AND ROUTE_TRIP.ROUTE_ID = :routeId ";

	String TRIP_SELECT = ""
			+ "SELECT   TRIP.NUMBER_IN_ROUTE, "
			+ "			TRIP.DISTANCE, "
			+ "			TRIP.TRIP_ID, "
			+ "			TRIP.ORIGIN, "
			+ "			TRIP.DESTINATION, "
			+ "			TRIP.RECORDSTATUS_ID, "
			+ "			PERSON.NAME, "
			+ "			PERSON.SIRNAME "
			+ "FROM ROUTE "
			+ "INNER JOIN ROUTE_TRIP ON ROUTE.ROUTE_ID = ROUTE_TRIP.ROUTE_ID "
			+ "INNER JOIN TRIP ON ROUTE_TRIP.TRIP_ID = TRIP.TRIP_ID AND TRIP.RECORDSTATUS_ID = 1 "
			+ "INNER JOIN PERSON ON TRIP.DESTINATION = PERSON.PERSON_ID "
			+ "WHERE ROUTE.ROUTE_ID = :routeId "
			+ "INTO     :trip.numberInRoute, "
			+ "         :trip.distance, "
			+ "			:trip.tripId, "
			+ "         :trip.originId, "
			+ "			:trip.destinationId, "
			+ "			:trip.recordstatusId, "
			+ "			:trip.destinationName, "
			+ "			:trip.destinationSirname ";

	String TRIP_SELECT_ADDRESS_PARTS = ""
			+ "SELECT STREET, PLZ, CITY "
			+ "FROM PERSON "
			+ "WHERE PERSON_ID = :destinationId ";

	String TRIP_UPDATE = ""
			+ "UPDATE   TRIP "
			+ "SET      ORIGIN = :originId, "
			+ "         DESTINATION = :destinationId, "
			+ "         NUMBER_IN_ROUTE = :numberInRoute, "
			+ "         DISTANCE  = :distance, "
			+ "			RECORDSTATUS_ID = :recordstatusId "
			+ "WHERE    trip_id = :tripId";

	String TRIP_INSERT_SAMPLE = ""
			+ "INSERT   INTO TRIP "
			+ "         (TRIP_ID, "
			+ "          ORIGIN, "
			+ "			 DESTINATION, "
			+ "			 NUMBER_IN_ROUTE, "
			+ "			 DISTANCE, "
			+ "			 RECORDSTATUS_ID "
			+ ") ";

	String TRIP_INSERT_VALUES_01 = ""
			+ "VALUES   ('1', "
			+ "          '1', "
			+ "			 '2', "
			+ "			 1, "
			+ "			 10.2, "
			+ "			 1"
			+ ") ";

	String TRIP_INSERT_VALUES_02 = ""
			+ "VALUES   ('2', "
			+ "          '2', "
			+ "			 '3', "
			+ "			 2, "
			+ "			 2.4, "
			+ "			 1 "
			+ ") ";

	String TRIP_INSERT_VALUES_03 = ""
			+ "VALUES   ('3', "
			+ "          '3', "
			+ "			 '1', "
			+ "			 3, "
			+ "			 9.1, "
			+ "			 1"
			+ ") ";

	String TRIP_INSERT_VALUES_04 = ""
			+ "VALUES   ('4', "
			+ "          '5', "
			+ "			 '3', "
			+ "			 1, "
			+ "			 9.1, "
			+ "			 1"
			+ ") ";

	String TRIP_INSERT_VALUES_05 = ""
			+ "VALUES   ('5', "
			+ "          '5', "
			+ "			 '1', "
			+ "			 1, "
			+ "			 3.7, "
			+ "			 1"
			+ ") ";

	String TRIP_INSERT_VALUES_06 = ""
			+ "VALUES   ('6', "
			+ "          '1', "
			+ "			 '4', "
			+ "			 2, "
			+ "			 9.2, "
			+ "			 1"
			+ ") ";

	//**************************************************************************
	// *** 						ROUTE TRIP DATA								 ***
	// *************************************************************************
	String ROUTE_TRIP_COMPLETE_DELETE_FOR_UPDATE_BY_TRIP_ID = ""
			+ "DELETE FROM ROUTE_TRIP WHERE TRIP_ID = :tripId";

	String ROUTE_TRIP_INSERT = ""
			+ "INSERT   INTO "
			+ "ROUTE_TRIP  (route_id, trip_id) "
			+ "VALUES   (:routeId, :tripId)";

	String ROUTE_TRIP_INSERT_SAMPLE = ""
			+ "INSERT   INTO ROUTE_TRIP "
			+ "         (ROUTE_ID, "
			+ "          TRIP_ID) ";

	String ROUTE_TRIP_VALUES_01 = ""
			+ "VALUES   ('1', "
			+ "          '1') ";

	String ROUTE_TRIP_VALUES_02 = ""
			+ "VALUES   ('1', "
			+ "          '2') ";

	String ROUTE_TRIP_VALUES_03 = ""
			+ "VALUES   ('1', "
			+ "          '3') ";

	String ROUTE_TRIP_VALUES_04 = ""
			+ "VALUES   ('2', "
			+ "          '4') ";

	String ROUTE_TRIP_VALUES_05 = ""
			+ "VALUES   ('3', "
			+ "          '5') ";

	String ROUTE_TRIP_VALUES_06 = ""
			+ "VALUES   ('3', "
			+ "          '6') ";


	//**************************************************************************
	// *** 						ROOM DATA									 ***
	// *************************************************************************
	String ROOM_ALL_ACTIVE = ""
			+ "SELECT   ROOM_ID, "
			+ "         NAME, "
			+ "         IS_INHOUSE, "
			+ "			COLOR, "
			+ "			RECORDSTATUS_ID "
			+ "FROM     ROOM "
			+ "WHERE 	RECORDSTATUS_ID = 1";

	String ROOM_ALL_ACTIVE_PAGE_DATA_SELECT_INTO = ""
			+ "INTO     :{page.roomId}, "
			+ "         :{page.name}, "
			+ "         :{page.isInhouse}, "
			+ "         :{page.color}, "
			+ "			:{page.recordstatusId} ";

	String ROOM_LOOKUP = ""
			+ "SELECT ROOM_ID AS ROOMID, NAME "
			+ "FROM ROOM "
			+ "WHERE 1 = 1 "
			+ "AND RECORDSTATUS_ID = 1"
			+ "<text> AND (UPPER(NAME) LIKE UPPER(:text||'%')) </text> "
			+ "<all></all>";

	String ROOM_INSERT = ""
			+ "INSERT   INTO "
			+ "ROOM  (room_id)"
			+ "VALUES   (:roomId)";

	String ROOM_DELETE= ""
			+ "UPDATE ROOM "
			+ "SET RECORDSTATUS_ID = 0 "
			+ "WHERE ROOM_ID LIKE :roomId ";

	String ROOM_SELECT = ""
			+ "SELECT   ROOM_ID, "
			+ "         NAME, "
			+ "         IS_INHOUSE, "
			+ "         COLOR, "
			+ "			RECORDSTATUS_ID "
			+ "FROM     ROOM "
			+ "WHERE    ROOM_ID = :roomId "
			+ "INTO     :roomId, "
			+ "         :name, "
			+ "         :isInhouse, "
			+ "         :color, "
			+ "			:recordstatusId ";

	String ROOM_UPDATE = ""
			+ "UPDATE   ROOM "
			+ "SET      ROOM_ID = :roomId, "
			+ "         NAME = :name, "
			+ "         IS_INHOUSE = :isInhouse, "
			+ "         COLOR = :color, "
			+ "			RECORDSTATUS_ID = :recordstatusId "
			+ "WHERE    ROOM_ID = :roomId";

	String ROOM_COLOR_BY_ID = ""
			+ "SELECT COLOR "
			+ "FROM ROOM "
			+ "WHERE ROOM_ID = :roomId "
			+ "AND RECORDSTATUS_ID = 1";

	String ROOM_ID_AND_INHOUSE = ""
			+ "SELECT ROOM_ID, IS_INHOUSE "
			+ "FROM ROOM "
			+ "WHERE RECORDSTATUS_ID = 1";

	String ROOM_INSERT_SAMPLE = ""
			+ "INSERT   INTO ROOM "
			+ "         (ROOM_ID, "
			+ "          NAME, "
			+ "			 IS_INHOUSE, "
			+ "			 RECORDSTATUS_ID) ";

	String ROOM_INSERT_VALUES_01 = ""
			+ "VALUES   ('1', "
			+ "          'Raum 1', "
			+ "          1, "
			+ "			 1) ";

	String ROOM_INSERT_VALUES_02 = ""
			+ "VALUES   ('2', "
			+ "          'Raum 2', "
			+ "          1, "
			+ "			 1) ";

	String ROOM_INSERT_VALUES_03 = ""
			+ "VALUES   ('3', "
			+ "          'Raum 3', "
			+ "          1, "
			+ "			 1) ";

	String ROOM_INSERT_VALUES_04 = ""
			+ "VALUES   ('4', "
			+ "          'Patientenbesuch', "
			+ "          0, "
			+ "			 1) ";

	//**************************************************************************
	// *** 					APPOINTMENT DATA								 ***
	// *************************************************************************
	String APPOINTMENT_INSERT = ""
			+ "INSERT   INTO "
			+ "APPOINTMENT  (APPOINTMENT_ID) "
			+ "VALUES   (:appointmentId)";

	String APPOINTMENT_DELETE = ""
			+ "UPDATE 	APPOINTMENT "
			+ "SET 		RECORDSTATUS_ID = 0 "
			+ "WHERE 	APPOINTMENT_ID LIKE :appointmentId ";

	String APPOINTMENT_DELETE_BY_REPEAT_ID_AND_DATE = ""
			+ "UPDATE 	APPOINTMENT "
			+ "SET		RECORDSTATUS_ID = 0 "
			+ "WHERE 	APPOINTMENT_REPEAT_ID = :appointmentRepeatId "
			+ "AND 		DATE_FROM > :dateFrom ";

	String APPOINTMENT_DELETE_BY_REPEAT_ID = ""
			+ "UPDATE 	APPOINTMENT "
			+ "SET		RECORDSTATUS_ID = 0 "
			+ "WHERE 	APPOINTMENT_REPEAT_ID = :appointmentRepeatId ";

	String APPOINTMENT_UPDATE = ""
			+ "UPDATE   APPOINTMENT "
			+ "SET      APPOINTMENT_ID = :appointmentId, "
			+ "         EMPLOYEE_ID = :employeeId, "
			+ "         PATIENT_ID = :patientId, "
			+ "         ROOM_ID = :roomId, "
			+ "         IS_FULLDAY = :isFullday, "
			+ "         DATE_FROM = :dateFrom, "
			+ "         DATE_TO = :dateTo, "
			+ "         IS_REPEATING = :isRepeating, "
			+ "         APPOINTMENT_REPEAT_ID = :appointmentRepeatId, "
			+ "         REPEATTYPE = :repeatType, "
			+ "         IS_REPEATTILLINFINITY = :isRepeatTillInfinity, "
			+ "         REPEATTILL = :repeatTill, "
			+ "         NOTICE = :notice, "
			+ "         IS_CANCELLED = :isCancelled, "
			+ "			RECORDSTATUS_ID = :recordstatusId "
			+ "WHERE    APPOINTMENT_ID = :appointmentId";

	String APPOINTMENT_UPDATE_BY_APPOINTMENT_REPEAT_ID = ""
			+ "UPDATE   APPOINTMENT "
			+ "SET      EMPLOYEE_ID = :employeeId, "
			+ "         PATIENT_ID = :patientId, "
			+ "         ROOM_ID = :roomId, "
			+ "         IS_FULLDAY = :isFullday, "
			+ "         DATE_FROM = :dateFrom, "
			+ "         DATE_TO = :dateTo, "
			+ "         IS_REPEATING = :isRepeating, "
			+ "         APPOINTMENT_REPEAT_ID = :appointmentRepeatId, "
			+ "         REPEATTYPE = :repeatType, "
			+ "         IS_REPEATTILLINFINITY = :isRepeatTillInfinity, "
			+ "         REPEATTILL = :repeatTill, "
			+ "         NOTICE = :notice, "
			+ "         IS_CANCELLED = :isCancelled, "
			+ "			RECORDSTATUS_ID = :recordstatusId "
			+ "WHERE    APPOINTMENT_REPEAT_ID = :appointmentRepeatId";

	String APPOINTMENT_UPDATE_REPEAT_TILL_APPOINTMENT_REPEAT_TILL_DATE_BY_APPOINTMENT_REPEAT_ID = ""
			+ "UPDATE   APPOINTMENT "
			+ "SET      REPEATTILL = :repeatTill "
			+ "WHERE    APPOINTMENT_REPEAT_ID = :appointmentRepeatId";

	String APPOINTMENT_UPDATE_INFINITY_TO_REPEAT_TILL_BY_APPOINTMENT_REPEAT_ID = ""
			+ "UPDATE   APPOINTMENT "
			+ "SET      REPEATTILL = :repeatTill, "
			+ "         IS_REPEATTILLINFINITY = :isRepeatTillInfinity "
			+ "WHERE    APPOINTMENT_REPEAT_ID = :appointmentRepeatId";

	String APPOINTMENT_ALL_ACTIVE = ""
			+ "SELECT 	APPOINTMENT_ID, "
			+ "			EMPLOYEE_ID, "
			+ "			PATIENT_ID, "
			+ "			APPOINTMENT.ROOM_ID, "
			+ "			IS_FULLDAY, "
			+ "			DATE_FROM, "
			+ "			DATE_TO, "
			+ "			IS_REPEATING, "
			+ "			APPOINTMENT_REPEAT_ID, "
			+  "		REPEATTYPE, "
			+ "			IS_REPEATTILLINFINITY, "
			+ "			REPEATTILL, "
			+ "			NOTICE, "
			+ "			IS_CANCELLED, "
			+ "			APPOINTMENT.RECORDSTATUS_ID, "
			+ "			EMPLOYEE.NAME, "
			+ "			PATIENT.NAME, "
			+ "			PATIENT.SIRNAME, "
			+ "			ROOM.NAME "
			+ "FROM		APPOINTMENT "
			+ "INNER JOIN PERSON AS EMPLOYEE ON EMPLOYEE_ID = EMPLOYEE.PERSON_ID "
			+ "INNER JOIN PERSON AS PATIENT ON PATIENT_ID = PATIENT.PERSON_ID "
			+ "INNER JOIN ROOM ON APPOINTMENT.ROOM_ID = ROOM.ROOM_ID "
			+ "WHERE 	APPOINTMENT.RECORDSTATUS_ID = 1 "
			+ "AND 		DATE_FROM > :dateFrom "
			+ "AND		DATE_TO < :dateTo ";


	String APPOINTMENT_ALL_ACTIVE_BETWEEN = ""
			+ "SELECT 	APPOINTMENT_ID, "
			+ "			EMPLOYEE_ID, "
			+ "			PATIENT_ID, "
			+ "			APPOINTMENT.ROOM_ID, "
			+ "			IS_FULLDAY, "
			+ "			DATE_FROM, "
			+ "			DATE_TO, "
			+ "			IS_REPEATING, "
			+ "			APPOINTMENT_REPEAT_ID, "
			+  "		REPEATTYPE, "
			+ "			IS_REPEATTILLINFINITY, "
			+ "			REPEATTILL, "
			+ "			NOTICE, "
			+ "			IS_CANCELLED, "
			+ "			APPOINTMENT.RECORDSTATUS_ID "
			+ "FROM		APPOINTMENT "
			+ "WHERE 	RECORDSTATUS_ID = 1 "
			+ "AND 		DATE_FROM > :startDate "
			+ "AND 		DATE_TO < :endDate";

	//**************************************************************************
	// *** 					CONFIGURATION DATA								 ***
	// *************************************************************************
	String CONFIGURATION_UPDATE= ""
			+ "UPDATE   "
			+ "CONFIGURATION  SET CONFIGURATION_VALUE = :value "
			+ "WHERE CONFIGURATION_KEY = :key ";

	String CONFIGURATION_BY_KEY = ""
			+ "SELECT  	CONFIGURATION_VALUE "
			+ "FROM 	CONFIGURATION "
			+ "WHERE 	CONFIGURATION_KEY = :key "
			+ "INTO 	:value ";
}
