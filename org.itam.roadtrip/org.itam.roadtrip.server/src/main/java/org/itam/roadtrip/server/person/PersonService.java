package org.itam.roadtrip.server.person;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.itam.roadtrip.server.ServerDefines;
import org.itam.roadtrip.server.sql.SQLs;
import org.itam.roadtrip.shared.person.CreatePersonPermission;
import org.itam.roadtrip.shared.person.IPersonService;
import org.itam.roadtrip.shared.person.PersonFormData;
import org.itam.roadtrip.shared.person.PersonTablePageData;
import org.itam.roadtrip.shared.person.ReadPersonPermission;
import org.itam.roadtrip.shared.person.UpdatePersonPermission;

public class PersonService implements IPersonService {

	@Override
	public PersonTablePageData getPersonTableData(final SearchFilter filter) {
		final PersonTablePageData pageData = new PersonTablePageData();
		final String sql = SQLs.PERSON_ALL_ACTIVE + SQLs.PERSON_ALL_ACTIVE_PAGE_DATA_SELECT_INTO;
		SQL.selectInto(sql, new NVPair("page", pageData));

		return pageData;
	}

	@Override
	public PersonFormData prepareCreate(final PersonFormData formData) {
		if (!ACCESS.check(new CreatePersonPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		return formData;
	}

	@Override
	public PersonFormData create(final PersonFormData formData) {
		if (!ACCESS.check(new CreatePersonPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		if (StringUtility.isNullOrEmpty(formData.getPersonId())) {
			formData.setPersonId(ServerDefines.getNextId(ServerDefines.TABLE_NAME_PERSON));
		}

		SQL.insert(SQLs.PERSON_INSERT, formData);

		return store(formData);
	}

	@Override
	public PersonFormData load(final PersonFormData formData) {
		if (!ACCESS.check(new ReadPersonPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		SQL.selectInto(SQLs.PERSON_SELECT, formData);

		return formData;
	}

	@Override
	public PersonFormData store(final PersonFormData formData) {
		if (!ACCESS.check(new UpdatePersonPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		SQL.update(SQLs.PERSON_UPDATE, formData);

		return formData;
	}

	@Override
	public void deletePerson(final String personId) {
		SQL.delete(SQLs.PERSON_DELETE, new NVPair("personId", personId));
	}

	@Override
	public Map<String, String> getAddressStringsForPersons(final Collection<String> personIds) {
		final Map<String, String> personAddresses = new HashMap<>(personIds.size());
		final Object[][] addressParts = SQL.select(SQLs.PERSON_ADDRESS_PARTS, new NVPair("personIds", personIds));
		StringBuilder builder;
		for (final Object[] addressPart : addressParts) {
			if ( ! personAddresses.containsKey(String.valueOf(addressPart[0]))) {
				builder = new StringBuilder();
				builder.append(String.valueOf(addressPart[1]));
				builder.append(", ");
				builder.append(String.valueOf(addressPart[2]));
				builder.append(" ");
				builder.append(String.valueOf(addressPart[3]));

				personAddresses.put(String.valueOf(addressPart[0]), builder.toString());
			}
		}
		return personAddresses;
	}
}
