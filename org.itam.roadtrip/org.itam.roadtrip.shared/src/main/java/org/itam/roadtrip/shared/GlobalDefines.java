package org.itam.roadtrip.shared;

public class GlobalDefines {

	public static final int RECORDSTATUS_DELETED = 0;
	public static final int RECORDSTATUS_ACTIVE = 1;

	public static final String PRAXIS_PERSON_ID = "1";

	public static final String FEATURE_ROUTE_NAME = "route";
	public static final String FEATURE_PERSON_NAME = "person";
	public static final String FEATURE_ROOM_NAME = "room";

	public enum UpdateType {
		SINGLE,
		THIS_AND_FOLLOWING;
	}
}
