package org.itam.roadtrip.shared;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.eclipse.scout.rt.platform.BEANS;
import org.itam.roadtrip.shared.settings.ISettingsService;
import org.itam.roadtrip.shared.settings.SettingsDefines;

public class ExcelUtil {

	static CellStyle headerCellStyle;

	public static void exportToExcel(final Collection<String> headerNames, final Map<Integer, Collection<String>> rows, final String feature) throws IOException {
		final SXSSFWorkbook wb = new SXSSFWorkbook(100);
		final Sheet sheet = wb.createSheet();

		initHeaderCellStyle(wb);

		writeHeader(sheet, headerNames);
		writeData(sheet, rows);

		final ISettingsService service = BEANS.get(ISettingsService.class);
		String exportPath = service.loadSettingsKeyValuePairs().get(SettingsDefines.SettingsKeys.EXPORT_PATH);
		exportPath = exportPath.replace("\\", "\\\\");

		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		final String dateString = cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
		final String path =  exportPath + "\\" + feature;
		final String filename = "\\export-" + dateString + ".xlsx";
		try {
			File file = new File(path);
			file.mkdirs();
			file = new File(path+filename);
			file.createNewFile();
			final FileOutputStream out = new FileOutputStream(file);
			wb.write(out);
			out.close();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			wb.dispose();
			wb.close();
		}
	}

	private static void initHeaderCellStyle(final SXSSFWorkbook wb) {
		headerCellStyle = wb.createCellStyle();

		final Font headerFont = wb.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints(Short.valueOf("14"));
		headerCellStyle.setFont(headerFont);

	}

	private static void writeHeader(final Sheet sheet, final Collection<String> headerNames) {
		final Row row = sheet.createRow(0);
		int i = 0;
		for (final String headerName : headerNames) {
			final Cell cell = row.createCell(i);
			cell.setCellValue(headerName);
			cell.setCellStyle(headerCellStyle);
			sheet.setColumnWidth(i, 20*256);
			i++;
		}
	}

	private static void writeData(final Sheet sheet, final Map<Integer, Collection<String>> rows) {
		int rowCount = 1;
		for (final Integer key : rows.keySet()) {
			final Collection<String> data = rows.get(key);
			int i = 0;
			final Row row = sheet.createRow(rowCount);
			for (final String string : data) {
				final Cell cell = row.createCell(i);
				cell.setCellValue(string);
				i++;
			}
			rowCount++;
		}
	}
}
