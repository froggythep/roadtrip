package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class ReadGenerateRoutesPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadGenerateRoutesPermission() {
		super(ReadGenerateRoutesPermission.class.getSimpleName());
	}
}
