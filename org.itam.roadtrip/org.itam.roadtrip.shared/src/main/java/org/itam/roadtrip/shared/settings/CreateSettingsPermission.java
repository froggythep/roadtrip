package org.itam.roadtrip.shared.settings;

import java.security.BasicPermission;

public class CreateSettingsPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateSettingsPermission() {
		super(CreateSettingsPermission.class.getSimpleName());
	}
}
