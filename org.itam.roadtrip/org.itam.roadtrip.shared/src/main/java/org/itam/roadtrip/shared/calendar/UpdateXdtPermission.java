package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class UpdateXdtPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateXdtPermission() {
		super(UpdateXdtPermission.class.getSimpleName());
	}
}
