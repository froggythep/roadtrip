package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadRoomPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadRoomPermission() {
		super(ReadRoomPermission.class.getSimpleName());
	}
}
