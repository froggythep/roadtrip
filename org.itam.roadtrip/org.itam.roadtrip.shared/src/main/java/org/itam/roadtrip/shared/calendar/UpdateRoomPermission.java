package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class UpdateRoomPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateRoomPermission() {
		super(UpdateRoomPermission.class.getSimpleName());
	}
}
