package org.itam.roadtrip.shared;

import java.util.Calendar;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class RepeatTypes extends AbstractCodeType<Long, String> {

	private static final long serialVersionUID = 1L;
	public static final Long ID = 20000L;

	public RepeatTypes() {
		super();
	}

	@Override
	protected String getConfiguredText() {
		return TEXTS.get("Repetitions");
	}

	@Override
	public Long getId() {
		return ID;
	}

	@Order(30)
	public static class Daily extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "daily";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("daily");
		}

		@Override
		public String getId() {
			return ID;
		}
	}

	@Order(50)
	public static class Weekly extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "weekly";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("weekly");
		}

		@Override
		public String getId() {
			return ID;
		}
	}

	@Order(70)
	public static class Monthly extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "monthly";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("monthly");
		}

		@Override
		public String getId() {
			return ID;
		}
	}

	@Order(110)
	public static class Yearly extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "yearly";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("yearly");
		}

		@Override
		public String getId() {
			return ID;
		}
	}

	public static void addDaysToCalendar(final Calendar cal, final String repeatTypeId) {
		if (repeatTypeId.equalsIgnoreCase(Daily.ID)) {
			cal.add(Calendar.DAY_OF_YEAR, 1);
		} else if (repeatTypeId.equalsIgnoreCase(Weekly.ID)) {
			cal.add(Calendar.WEEK_OF_YEAR, 1);
		} else if (repeatTypeId.equalsIgnoreCase(Monthly.ID)) {
			cal.add(Calendar.MONTH, 1);
		} else if (repeatTypeId.equalsIgnoreCase(Yearly.ID)) {
			cal.add(Calendar.YEAR, 1);
		}
	}

	public static void substractDaysFromCalendar(final Calendar cal, final String repeatTypeId) {
		if (repeatTypeId.equalsIgnoreCase(Daily.ID)) {
			cal.add(Calendar.DAY_OF_YEAR, -1);
		} else if (repeatTypeId.equalsIgnoreCase(Weekly.ID)) {
			cal.add(Calendar.WEEK_OF_YEAR, -1);
		} else if (repeatTypeId.equalsIgnoreCase(Monthly.ID)) {
			cal.add(Calendar.MONTH, -1);
		} else if (repeatTypeId.equalsIgnoreCase(Yearly.ID)) {
			cal.add(Calendar.YEAR, -1);
		}
	}
}
