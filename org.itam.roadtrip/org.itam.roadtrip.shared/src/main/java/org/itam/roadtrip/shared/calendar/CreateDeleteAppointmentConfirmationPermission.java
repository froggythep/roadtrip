package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateDeleteAppointmentConfirmationPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateDeleteAppointmentConfirmationPermission() {
		super(CreateDeleteAppointmentConfirmationPermission.class.getSimpleName());
	}
}
