package org.itam.roadtrip.shared.person;

import org.eclipse.scout.rt.shared.data.basic.table.AbstractTableRowData;

public class PersonTableRowData extends AbstractTableRowData {

	private static final long serialVersionUID = 1L;
	public static final String personId = "personId";
	public static final String name = "name";
	public static final String sirname = "sirname";
	public static final String street = "street";
	public static final String plz = "plz";
	public static final String city = "city";
	private String m_personId;
	private String m_name;
	private String m_sirname;
	private String m_street;
	private String m_plz;
	private String m_city;

	public PersonTableRowData() {
	}

	public String getPersonId() {
		return  m_personId;
	}

	public void setPersonId(final String personId) {
		m_personId = personId;
	}

	public String getName() {
		return m_name;
	}

	public void setName(final String name) {
		m_name = name;
	}

	public String getSirname() {
		return m_sirname;
	}

	public void setSirname(final String sirname) {
		m_sirname = sirname;
	}

	public String getStreet() {
		return m_street;
	}

	public void setStreet(final String street) {
		m_street = street;
	}

	public String getPlz() {
		return m_plz;
	}

	public void setPlz(final String plz) {
		m_plz = plz;
	}

	public String getCity() {
		return m_city;
	}

	public void setCity(final String city) {
		m_city = city;
	}
}


