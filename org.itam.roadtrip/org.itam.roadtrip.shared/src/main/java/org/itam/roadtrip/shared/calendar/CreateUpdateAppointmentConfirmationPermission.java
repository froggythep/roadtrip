package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateUpdateAppointmentConfirmationPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateUpdateAppointmentConfirmationPermission() {
		super(CreateUpdateAppointmentConfirmationPermission.class.getSimpleName());
	}
}
