package org.itam.roadtrip.shared.settings;

public class SettingsDefines {

	public enum SettingsKeys {
		GOOGLE_MAPS_API_KEY("GOOGLE_MAPS_API_KEY"),
		EXPORT_PATH("EXPORT_PATH");

		private final String key;

		private SettingsKeys(final String key) {
			this.key = key;
		}

		public String getKey()  {
			return key;
		}
	}
}
