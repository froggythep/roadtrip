package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadAppointmentPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadAppointmentPermission() {
		super(ReadAppointmentPermission.class.getSimpleName());
	}
}
