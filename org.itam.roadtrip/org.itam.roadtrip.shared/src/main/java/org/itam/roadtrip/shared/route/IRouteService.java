package org.itam.roadtrip.shared.route;

import java.util.Collection;
import java.util.Date;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

@TunnelToServer
public interface IRouteService extends IService {

	RouteTablePageData getRouteTableData(SearchFilter filter);

	RouteFormData prepareCreate(RouteFormData formData);

	RouteFormData create(RouteFormData formData);

	RouteFormData load(RouteFormData formData);

	RouteFormData store(RouteFormData formData);

	void deleteRoute(final String routeId);

	Collection<String> getAddressesInOrderForRoute(final String routeId);

	void generateRoutesFromAppointments(final Date startDate, final Date endDate);
}
