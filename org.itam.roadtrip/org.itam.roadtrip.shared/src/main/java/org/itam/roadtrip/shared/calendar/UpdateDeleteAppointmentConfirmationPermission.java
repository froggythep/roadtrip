package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class UpdateDeleteAppointmentConfirmationPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateDeleteAppointmentConfirmationPermission() {
		super(UpdateDeleteAppointmentConfirmationPermission.class.getSimpleName());
	}
}
