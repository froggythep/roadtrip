package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateAppointmentPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateAppointmentPermission() {
		super(CreateAppointmentPermission.class.getSimpleName());
	}
}
