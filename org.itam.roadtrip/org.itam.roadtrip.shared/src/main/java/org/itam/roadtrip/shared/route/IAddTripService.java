package org.itam.roadtrip.shared.route;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface IAddTripService extends IService {

	AddTripFormData prepareCreate(AddTripFormData formData);

	AddTripFormData create(AddTripFormData formData);

	AddTripFormData load(AddTripFormData formData);

	AddTripFormData store(AddTripFormData formData);
}
