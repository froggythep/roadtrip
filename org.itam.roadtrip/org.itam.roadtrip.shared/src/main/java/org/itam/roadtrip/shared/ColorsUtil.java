package org.itam.roadtrip.shared;

import java.awt.Color;

public class ColorsUtil {

	public static Color getColorById(final String colorId) {
		if (colorId.equalsIgnoreCase("green")) {
			return Color.GREEN;
		} else if (colorId.equalsIgnoreCase("orange")) {
			return Color.ORANGE;
		} else if (colorId.equalsIgnoreCase("blue")) {
			return Color.BLUE;
		} else if (colorId.equalsIgnoreCase("cyan")) {
			return Color.CYAN;
		}
		return null;
	}

	public static String getColorHexStringById(final String colorId) {
		final Color color = getColorById(colorId);
		if (color != null) {
			return Integer.toHexString(color.getRGB()).substring(2);
		}
		return "";
	}
}
