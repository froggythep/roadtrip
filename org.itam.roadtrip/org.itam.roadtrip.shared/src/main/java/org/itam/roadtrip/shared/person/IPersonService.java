package org.itam.roadtrip.shared.person;

import java.util.Collection;
import java.util.Map;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

@TunnelToServer
public interface IPersonService extends IService {

	PersonTablePageData getPersonTableData(SearchFilter filter);

	PersonFormData prepareCreate(PersonFormData formData);

	PersonFormData create(PersonFormData formData);

	PersonFormData load(PersonFormData formData);

	PersonFormData store(PersonFormData formData);

	void deletePerson(final String personId);

	/**
	 * Key = personId
	 * Value = Address string
	 * @param personIds
	 * @return
	 */
	Map<String, String> getAddressStringsForPersons(final Collection<String> personIds);
}
