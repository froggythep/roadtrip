package org.itam.roadtrip.shared.calendar;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.scout.rt.shared.services.lookup.ILookupRow;
import org.eclipse.scout.rt.shared.services.lookup.ILookupService;
import org.eclipse.scout.rt.shared.services.lookup.LookupCall;
import org.eclipse.scout.rt.shared.services.lookup.LookupRow;
import org.itam.roadtrip.shared.calendar.RoomTablePageData.RoomTableRowData;

public class RoomLookupCall extends LookupCall<String> {

	private static final long serialVersionUID = 1L;

	@Override
	protected Class<? extends ILookupService<String>> getConfiguredService() {
		return IRoomLookupService.class;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByAll() {
		final List<? extends ILookupRow<String>> oldRows = super.getDataByAll();
		final List<LookupRow<String>> rows = new ArrayList<>();
		for (final ILookupRow<String> iLookupRow : oldRows) {
			rows.add(createLookupRow(iLookupRow));
		}
		return rows;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByText() {
		final List<? extends ILookupRow<String>> oldRows = super.getDataByText();
		final List<LookupRow<String>> rows = new ArrayList<>();
		for (final ILookupRow<String> iLookupRow : oldRows) {
			rows.add(createLookupRow(iLookupRow));
		}
		return rows;
	}

	private LookupRow<String> createLookupRow(final ILookupRow<String> row) {
		final String text = row.getText();
		final LookupRow<String> lookupRow = new LookupRow<>(row.getKey(), text);
		final RoomTableRowData data = new RoomTableRowData();
		data.setRoomId(row.getKey());
		data.setName(row.getText());
		return lookupRow.withAdditionalTableRowData(data);
	}
}
