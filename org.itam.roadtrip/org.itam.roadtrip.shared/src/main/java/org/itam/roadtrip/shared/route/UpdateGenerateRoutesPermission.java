package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class UpdateGenerateRoutesPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateGenerateRoutesPermission() {
		super(UpdateGenerateRoutesPermission.class.getSimpleName());
	}
}
