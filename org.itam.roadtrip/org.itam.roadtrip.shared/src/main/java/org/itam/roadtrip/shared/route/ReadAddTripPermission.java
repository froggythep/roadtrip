package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class ReadAddTripPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadAddTripPermission() {
		super(ReadAddTripPermission.class.getSimpleName());
	}
}
