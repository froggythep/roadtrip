package org.itam.roadtrip.shared.calendar;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.shared.services.common.calendar.CalendarAppointment;
import org.itam.roadtrip.shared.GlobalDefines;

public class CalendarAppointmentRoadtrip extends CalendarAppointment{

	private static final long serialVersionUID = -1029922963198406068L;
	private String appointmentId;
	private String roomId;
	private String roomName;
	private String employeeId;
	private String employeeName;
	private String patientId;
	private String patientName;
	private String patientSirname;
	private boolean isRepeating;
	private String appointmentRepeatId;
	private String repeatType;
	private boolean isRepeatTillInfinity;
	private Date repeatTill;
	private String notice;
	private boolean isCancelled;
	private int recordstatusId;

	public CalendarAppointmentRoadtrip() {
		//default constructor;
	}

	public CalendarAppointmentRoadtrip(final Object itemId, final Object person, final Date startDate, final Date endDate, final boolean fullDay,
			final String roomId, final String employeeId, final String patientId, final boolean isRepeating, final String repeatType, final boolean isRepeatTillInfinity,
			final Date repeatTill, final String notice, final boolean isCancelled,
			final String roomName, final String employeeName, final String patientName, final String patientSirname) {
		super(itemId, person, startDate, endDate, fullDay, "", "", "");
		this.roomId = roomId;
		this.employeeId = employeeId;
		this.patientId = patientId;
		this.isRepeating = isRepeating;
		this.repeatType = repeatType;
		this.isRepeatTillInfinity = isRepeatTillInfinity;
		this.repeatTill = repeatTill;
		this.notice = notice;
		this.isCancelled = isCancelled;
		this.roomName = roomName;
		this.employeeName = employeeName;
		this.patientName = patientName;
		this.patientSirname = patientSirname;
		recordstatusId = GlobalDefines.RECORDSTATUS_ACTIVE;
	}

	@Override
	public CalendarAppointmentRoadtrip copy() {
		final CalendarAppointmentRoadtrip newApp = new CalendarAppointmentRoadtrip();
		newApp.setRoomId(getRoomId());
		newApp.setRoomName(getRoomName());
		newApp.setEmployeeId(getEmployeeId());
		newApp.setEmployeeName(getEmployeeName());
		newApp.setPatientId(getPatientId());
		newApp.setPatientName(getPatientName());
		newApp.setPatientSirname(getPatientSirname());
		newApp.setRepeating(isRepeating());
		newApp.setAppointmentRepeatId(getAppointmentRepeatId());
		newApp.setRepeatType(getRepeatType());
		newApp.setRepeatTillInfinity(isRepeatTillInfinity());
		newApp.setRepeatTill(getRepeatTill());
		newApp.setNotice(getNotice());
		newApp.setCancelled(isCancelled());
		newApp.setRecordstatusId(getRecordstatusId());
		newApp.setFullDay(isFullDay());
		newApp.setStart(getStart());
		newApp.setEnd(getEnd());
		return newApp;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(final String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(final String employeeId) {
		this.employeeId = employeeId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(final String patientId) {
		this.patientId = patientId;
	}

	public boolean isRepeating() {
		return isRepeating;
	}

	public void setRepeating(final boolean isRepeating) {
		this.isRepeating = isRepeating;
	}

	public String getAppointmentRepeatId() {
		return appointmentRepeatId;
	}

	public void setAppointmentRepeatId(final String appointmentRepeatId) {
		this.appointmentRepeatId = appointmentRepeatId;
	}

	public String getRepeatType() {
		return repeatType;
	}

	public void setRepeatType(final String repeatType) {
		this.repeatType = repeatType;
	}

	public Date getRepeatTill() {
		return repeatTill;
	}

	public void setRepeatTill(final Date repeatTill) {
		this.repeatTill = repeatTill;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(final String notice) {
		this.notice = notice;
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(final boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	public int getRecordstatusId() {
		return recordstatusId;
	}

	public void setRecordstatusId(final int recordstatusId) {
		this.recordstatusId = recordstatusId;
	}

	public boolean isRepeatTillInfinity() {
		return isRepeatTillInfinity;
	}

	public void setRepeatTillInfinity(final boolean isRepeatTillInfinity) {
		this.isRepeatTillInfinity = isRepeatTillInfinity;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(final String roomName) {
		this.roomName = roomName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(final String employeeName) {
		this.employeeName = employeeName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(final String patientName) {
		this.patientName = patientName;
	}

	public String getPatientSirname() {
		return patientSirname;
	}

	public void setPatientSirname(final String patientSirname) {
		this.patientSirname = patientSirname;
	}

	@Override
	public String getSubject() {
		final String titleSubject = getTimeFrame() + "  " + getRoomName() + ", "
				+ "\n Mitarbeiter: " + getEmployeeName() + ", "
				+ " \n Patient: " + getPatientName() + " " + getPatientSirname();
		return titleSubject;
	}


	private String getTimeFrame() {
		if ( ! isFullDay()) {
			final Calendar calTimeFrom = Calendar.getInstance();
			calTimeFrom.setTime(getStart());
			final Calendar calTimeTo = Calendar.getInstance();
			calTimeTo.setTime(getEnd());

			return String.format("%02d", Integer.valueOf(calTimeFrom.get(Calendar.HOUR_OF_DAY))) + ":"
			+ String.format("%02d", Integer.valueOf(calTimeFrom.get(Calendar.MINUTE))) + " - "
			+ String.format("%02d", Integer.valueOf(calTimeTo.get(Calendar.HOUR_OF_DAY))) + ":"
			+ String.format("%02d", Integer.valueOf(calTimeTo.get(Calendar.MINUTE)));
		}
		return "";
	}

	@Override
	public String getBody() {
		if (getNotice() == null || getNotice().equalsIgnoreCase("null")) {
			return "";
		}
		return getNotice();
	}

	@Override
	public String getCssClass() {
		String cssClass = "";
		if (isCancelled()) {
			cssClass = "red line-through";
		} else {
			cssClass = getRoomColorCssClass(getRoomId());
		}
		return cssClass;
	}

	private String getRoomColorCssClass(final String roomId) {
		final IRoomService service = BEANS.get(IRoomService.class);
		final String color = service.getRoomColorString(roomId);
		if (StringUtility.isNullOrEmpty(color)) {
			return "";
		} else {
			return color;
		}
	}

}
