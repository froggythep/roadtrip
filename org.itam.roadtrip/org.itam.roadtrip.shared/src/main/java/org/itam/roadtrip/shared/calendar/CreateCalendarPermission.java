package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateCalendarPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateCalendarPermission() {
		super(CreateCalendarPermission.class.getSimpleName());
	}
}
