package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateXdtPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateXdtPermission() {
		super(CreateXdtPermission.class.getSimpleName());
	}
}
