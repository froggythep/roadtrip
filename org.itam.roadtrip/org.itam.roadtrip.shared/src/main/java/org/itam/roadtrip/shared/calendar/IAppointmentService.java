package org.itam.roadtrip.shared.calendar;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface IAppointmentService extends IService {

	AppointmentFormData prepareCreate(AppointmentFormData formData);

	AppointmentFormData create(AppointmentFormData formData);

	AppointmentFormData load(AppointmentFormData formData);

	AppointmentFormData store(AppointmentFormData formData);
}
