package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateRoomPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateRoomPermission() {
		super(CreateRoomPermission.class.getSimpleName());
	}
}
