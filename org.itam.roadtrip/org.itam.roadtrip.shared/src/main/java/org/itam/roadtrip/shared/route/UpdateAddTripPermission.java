package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class UpdateAddTripPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateAddTripPermission() {
		super(UpdateAddTripPermission.class.getSimpleName());
	}
}
