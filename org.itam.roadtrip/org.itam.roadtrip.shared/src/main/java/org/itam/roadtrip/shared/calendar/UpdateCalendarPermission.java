package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class UpdateCalendarPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateCalendarPermission() {
		super(UpdateCalendarPermission.class.getSimpleName());
	}
}
