package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class CreateXdsPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateXdsPermission() {
		super(CreateXdsPermission.class.getSimpleName());
	}
}
