package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadUpdateAppointmentConfirmationPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadUpdateAppointmentConfirmationPermission() {
		super(ReadUpdateAppointmentConfirmationPermission.class.getSimpleName());
	}
}
