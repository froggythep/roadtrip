package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class CreateAddTripPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateAddTripPermission() {
		super(CreateAddTripPermission.class.getSimpleName());
	}
}
