package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadCalendarPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadCalendarPermission() {
		super(ReadCalendarPermission.class.getSimpleName());
	}
}
