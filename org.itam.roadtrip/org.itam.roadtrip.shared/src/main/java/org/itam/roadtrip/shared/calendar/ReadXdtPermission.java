package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadXdtPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadXdtPermission() {
		super(ReadXdtPermission.class.getSimpleName());
	}
}
