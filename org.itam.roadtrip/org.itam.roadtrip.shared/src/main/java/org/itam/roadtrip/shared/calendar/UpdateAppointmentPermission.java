package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class UpdateAppointmentPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateAppointmentPermission() {
		super(UpdateAppointmentPermission.class.getSimpleName());
	}
}
