package org.itam.roadtrip.shared.calendar;

import java.util.Collection;
import java.util.Date;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.itam.roadtrip.shared.GlobalDefines.UpdateType;

@TunnelToServer
public interface ICalendarService extends IService {

	void saveAppointment(final CalendarAppointmentRoadtrip appointment);

	Collection<CalendarAppointmentRoadtrip> getAllAppointments(final Date dateFrom, final Date dateTo);

	void deleteAppointment(final String appointmentId);

	void deleteThisAndFollowingAppointmentsInRepetition(final String appointmentRepeatId, final Date dateFrom);

	void deleteAllAppointmentsInRepetition(final String appointmentRepeatId);

	Collection<CalendarAppointmentRoadtrip> getAppointmentsBetween(final Date startDate, final Date endDate);

	void updateAppointment(UpdateType updateType, CalendarAppointmentRoadtrip oldApp, CalendarAppointmentRoadtrip updatedApp);
}
