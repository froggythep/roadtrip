package org.itam.roadtrip.shared.settings;

import java.util.HashMap;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface ISettingsService extends IService {

	void prepareCreate();

	void create();

	void load();

	void store();

	void storeSettingsKeyValuePair(final SettingsDefines.SettingsKeys key, final String value);

	HashMap<SettingsDefines.SettingsKeys, String> loadSettingsKeyValuePairs();
}
