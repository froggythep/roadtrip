package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadDeleteAppointmentConfirmationPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadDeleteAppointmentConfirmationPermission() {
		super(ReadDeleteAppointmentConfirmationPermission.class.getSimpleName());
	}
}
