package org.itam.roadtrip.shared.calendar;

import java.util.Comparator;

public class CalendarAppointmentSorter implements Comparator<CalendarAppointmentRoadtrip> {

	@Override
	public int compare(final CalendarAppointmentRoadtrip car1, final CalendarAppointmentRoadtrip car2) {
		return car1.getStart().before(car2.getStart()) ? -1 : 1;
	}

}
