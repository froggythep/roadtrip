package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class UpdateUpdateAppointmentConfirmationPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateUpdateAppointmentConfirmationPermission() {
		super(UpdateUpdateAppointmentConfirmationPermission.class.getSimpleName());
	}
}
