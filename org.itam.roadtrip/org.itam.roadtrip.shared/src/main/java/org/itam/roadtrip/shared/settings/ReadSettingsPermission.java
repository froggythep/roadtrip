package org.itam.roadtrip.shared.settings;

import java.security.BasicPermission;

public class ReadSettingsPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadSettingsPermission() {
		super(ReadSettingsPermission.class.getSimpleName());
	}
}
