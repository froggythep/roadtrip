package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class CreateGenerateRoutesPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateGenerateRoutesPermission() {
		super(CreateGenerateRoutesPermission.class.getSimpleName());
	}
}
