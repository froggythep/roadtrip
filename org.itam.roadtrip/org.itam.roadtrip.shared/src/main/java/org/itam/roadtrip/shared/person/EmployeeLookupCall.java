package org.itam.roadtrip.shared.person;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.scout.rt.shared.services.lookup.ILookupRow;
import org.eclipse.scout.rt.shared.services.lookup.ILookupService;
import org.eclipse.scout.rt.shared.services.lookup.LookupCall;
import org.eclipse.scout.rt.shared.services.lookup.LookupRow;

public class EmployeeLookupCall extends LookupCall<String> {

	private static final long serialVersionUID = 1L;

	@Override
	protected Class<? extends ILookupService<String>> getConfiguredService() {
		return IEmployeeLookupService.class;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByAll() {
		final List<? extends ILookupRow<String>> oldRows = super.getDataByAll();
		final List<LookupRow<String>> rows = new ArrayList<>();
		for (final ILookupRow<String> iLookupRow : oldRows) {
			rows.add(createLookupRow(iLookupRow));
		}
		return rows;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByText() {
		final List<? extends ILookupRow<String>> oldRows = super.getDataByText();
		final List<LookupRow<String>> rows = new ArrayList<>();
		for (final ILookupRow<String> iLookupRow : oldRows) {
			rows.add(createLookupRow(iLookupRow));
		}
		return rows;
	}

	@Override
	public List<? extends ILookupRow<String>> getDataByKey() {
		final List<? extends ILookupRow<String>> oldRows = super.getDataByKey();
		final List<LookupRow<String>> rows = new ArrayList<>();
		for (final ILookupRow<String> iLookupRow : oldRows) {
			rows.add(createLookupRow(iLookupRow));
		}
		return rows;
	}

	private LookupRow<String> createLookupRow(final ILookupRow<String> row) {
		final String text = row.getText() + " " + row.getIconId();
		final LookupRow<String> lookupRow = new LookupRow<>(row.getKey(), text);
		final PersonTableRowData data = new PersonTableRowData();
		data.setPersonId(row.getKey());
		data.setName(row.getText());
		data.setSirname(row.getIconId());
		data.setStreet(row.getTooltipText());
		data.setPlz(row.getBackgroundColor());
		data.setCity(row.getForegroundColor());
		return lookupRow.withAdditionalTableRowData(data);
	}
}
