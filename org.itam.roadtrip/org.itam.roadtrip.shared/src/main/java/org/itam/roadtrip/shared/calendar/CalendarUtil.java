package org.itam.roadtrip.shared.calendar;

import java.util.Calendar;
import java.util.Date;

public class CalendarUtil {

	public static Date getTodayStartTime() {
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date setDateToStartTime(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date setDateToMidday(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 12);
		return cal.getTime();
	}

	public static Date setDateToEndTime(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getMondayOfCurrentWeek() {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
			cal.add(Calendar.DATE, -1);
		}
		return setDateToStartTime(cal.getTime());
	}

	public static Date getSundayInSixMonths() {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			cal.add(Calendar.DATE, 1);
		}
		cal.add(Calendar.MONTH, 6);
		return setDateToEndTime(cal.getTime());
	}
}
