package org.itam.roadtrip.shared.maps;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.scout.rt.platform.BEANS;
import org.itam.roadtrip.shared.route.IRouteService;
import org.itam.roadtrip.shared.settings.ISettingsService;
import org.itam.roadtrip.shared.settings.SettingsDefines.SettingsKeys;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;

public class MapsUtil {

	private static GeoApiContext context;

	private static void init() {
		final ISettingsService service = BEANS.get(ISettingsService.class);
		final HashMap<SettingsKeys, String> keyValuePairs = service.loadSettingsKeyValuePairs();
		context = new GeoApiContext().setApiKey(keyValuePairs.get(SettingsKeys.GOOGLE_MAPS_API_KEY));
	}

	public static Double getDistanceForTrip(final String originAddress, final String destinationAddress) {
		if (context == null) {
			init();
		}
		try {
			Double totalDistance = 0.0;
			final DirectionsResult result = DirectionsApi.getDirections(context, originAddress, destinationAddress).await();
			final DirectionsRoute[] route = result.routes;
			if (route.length != 0) {
				final DirectionsLeg[] legs = route[0].legs;
				for (final DirectionsLeg directionsLeg : legs) {
					totalDistance += new Long(directionsLeg.distance.inMeters).doubleValue() / 1000;
				}
			}
			return totalDistance;
		} catch (ApiException | InterruptedException | IOException e) {
			e.printStackTrace();
		}

		return 0.0;
	}

	public static String getUriForRoute(final String startAddress, final LinkedList<String> addresses) {
		final StringBuilder builder = new StringBuilder();
		builder.append("https://www.google.de/maps/dir/"+ startAddress + "/");
		for (final String string : addresses) {
			builder.append(string + "/");
		}

		String uri = builder.toString();
		uri = uri.toLowerCase();
		uri = uri.replaceAll(" ", "+");
		return uri;
	}

	public static String getUriForRoute(final String routeId) {
		final IRouteService service = BEANS.get(IRouteService.class);
		final Collection<String> addressesInOrder = service.getAddressesInOrderForRoute(routeId);
		String startAddress = "";
		final LinkedList<String> addresses = new LinkedList<>();
		boolean first = true;
		for (final String string : addressesInOrder) {
			if (first) {
				startAddress = string;
				first = false;
			} else {
				addresses.add(string);
			}
		}
		return getUriForRoute(startAddress, addresses);
	}


}
