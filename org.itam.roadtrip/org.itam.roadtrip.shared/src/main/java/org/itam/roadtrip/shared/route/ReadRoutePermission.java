package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class ReadRoutePermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadRoutePermission() {
		super(ReadRoutePermission.class.getSimpleName());
	}
}
