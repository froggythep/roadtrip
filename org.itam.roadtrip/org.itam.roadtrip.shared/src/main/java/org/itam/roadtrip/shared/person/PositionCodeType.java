package org.itam.roadtrip.shared.person;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class PositionCodeType extends AbstractCodeType<String, String> {

	private static final long serialVersionUID = 1L;
	public static final String ID = "Position";

	@Override
	public String getId() {
		return ID;
	}

	@Order(1000)
	public static class EmployeeCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "Employee";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Employee");
		}

		@Override
		public String getId() {
			return ID;
		}
	}

	@Order(2000)
	public static class PatientCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "Patient";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Patient");
		}

		@Override
		public String getId() {
			return ID;
		}
	}
}
