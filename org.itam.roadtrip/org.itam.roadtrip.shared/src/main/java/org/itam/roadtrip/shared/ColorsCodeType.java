package org.itam.roadtrip.shared;

import java.awt.Color;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class ColorsCodeType extends AbstractCodeType<Long, String> {

	private static final long serialVersionUID = 1L;
	public static final Long ID = 20000L;

	public ColorsCodeType() {
		super();
	}

	@Override
	protected String getConfiguredText() {
		return TEXTS.get("Colors");
	}

	@Override
	public Long getId() {
		return ID;
	}

	//	@Order(10)
	//	public static class BlackCode extends AbstractCode<Color> {
	//		private static final long serialVersionUID = 1L;
	//		public static final Color ID = Color.BLACK;
	//
	//		@Override
	//		protected String getConfiguredText() {
	//			return TEXTS.get("Black");
	//		}
	//
	//		@Override
	//		public Color getId() {
	//			return ID;
	//		}
	//	}

	@Order(80)
	public static class BlueCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "blue";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Blue");
		}

		@Override
		public String getId() {
			return ID;
		}

		public Color getColor() {
			return Color.BLUE;
		}
	}

	@Order(90)
	public static class CyanCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "cyan";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Cyan");
		}

		@Override
		public String getId() {
			return ID;
		}

		public Color getColor() {
			return Color.CYAN;
		}
	}

	//	@Order(20)
	//	public static class DarkGrayCode extends AbstractCode<Color> {
	//		private static final long serialVersionUID = 1L;
	//		public static final Color ID = Color.DARK_GRAY;
	//
	//		@Override
	//		protected String getConfiguredText() {
	//			return TEXTS.get("DarkGray");
	//		}
	//
	//		@Override
	//		public Color getId() {
	//			return ID;
	//		}
	//	}

	//	@Order(30)
	//	public static class GrayCode extends AbstractCode<Color> {
	//		private static final long serialVersionUID = 1L;
	//		public static final Color ID = Color.GRAY;
	//
	//		@Override
	//		protected String getConfiguredText() {
	//			return TEXTS.get("Gray");
	//		}
	//
	//		@Override
	//		public Color getId() {
	//			return ID;
	//		}
	//	}

	@Order(70)
	public static class GreenCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "green";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Green");
		}

		@Override
		public String getId() {
			return ID;
		}

		public Color getColor() {
			return Color.GREEN;
		}
	}

	//	@Order(40)
	//	public static class LightGrayCode extends AbstractCode<Color> {
	//		private static final long serialVersionUID = 1L;
	//		public static final Color ID = Color.LIGHT_GRAY;
	//
	//		@Override
	//		protected String getConfiguredText() {
	//			return TEXTS.get("LightGray");
	//		}
	//
	//		@Override
	//		public Color getId() {
	//			return ID;
	//		}
	//	}

	//	@Order(100)
	//	public static class MagentaCode extends AbstractCode<Color> {
	//		private static final long serialVersionUID = 1L;
	//		public static final Color ID = Color.MAGENTA;
	//
	//		@Override
	//		protected String getConfiguredText() {
	//			return TEXTS.get("Magenta");
	//		}
	//
	//		@Override
	//		public Color getId() {
	//			return ID;
	//		}
	//	}

	@Order(110)
	public static class OrangeCode extends AbstractCode<String> {
		private static final long serialVersionUID = 1L;
		public static final String ID = "orange";

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Orange");
		}

		@Override
		public String getId() {
			return ID;
		}

		public Color getColor() {
			return Color.ORANGE;
		}
	}

	//	@Order(120)
	//	public static class YellowCode extends AbstractCode<Color> {
	//		private static final long serialVersionUID = 1L;
	//		public static final Color ID = Color.YELLOW;
	//
	//		@Override
	//		protected String getConfiguredText() {
	//			return TEXTS.get("Yellow");
	//		}
	//
	//		@Override
	//		public Color getId() {
	//			return ID;
	//		}
	//	}
}
