package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class CreateRoutePermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateRoutePermission() {
		super(CreateRoutePermission.class.getSimpleName());
	}
}
