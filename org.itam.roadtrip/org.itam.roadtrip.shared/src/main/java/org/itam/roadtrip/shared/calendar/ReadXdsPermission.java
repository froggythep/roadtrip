package org.itam.roadtrip.shared.calendar;

import java.security.BasicPermission;

public class ReadXdsPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadXdsPermission() {
		super(ReadXdsPermission.class.getSimpleName());
	}
}
