package org.itam.roadtrip.shared.route;

import java.security.BasicPermission;

public class UpdateRoutePermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateRoutePermission() {
		super(UpdateRoutePermission.class.getSimpleName());
	}
}
