package org.itam.roadtrip.shared.settings;

import java.security.BasicPermission;

public class UpdateSettingsPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateSettingsPermission() {
		super(UpdateSettingsPermission.class.getSimpleName());
	}
}
