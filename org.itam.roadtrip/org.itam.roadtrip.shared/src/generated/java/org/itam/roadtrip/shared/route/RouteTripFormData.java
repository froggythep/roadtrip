package org.itam.roadtrip.shared.route;

import org.eclipse.scout.rt.shared.data.form.AbstractFormData;

public class RouteTripFormData extends AbstractFormData {

	private static final long serialVersionUID = 1L;

	private final String routeId;

	private final String tripId;

	public RouteTripFormData(final String routeId, final String tripId) {
		this.routeId = routeId;
		this.tripId = tripId;
	}

	public String getRouteId() {
		return routeId;
	}

	public String getTripId() {
		return tripId;
	}
}
