package org.itam.roadtrip.shared.calendar;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

@TunnelToServer
public interface IXdsService extends IService {

	XdsTablePageData getXdsTableData(SearchFilter filter);
}
