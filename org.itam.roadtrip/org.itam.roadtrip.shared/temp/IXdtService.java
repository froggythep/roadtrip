package org.itam.roadtrip.shared.calendar;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface IXdtService extends IService {

	XdtFormData prepareCreate(XdtFormData formData);

	XdtFormData create(XdtFormData formData);

	XdtFormData load(XdtFormData formData);

	XdtFormData store(XdtFormData formData);
}
