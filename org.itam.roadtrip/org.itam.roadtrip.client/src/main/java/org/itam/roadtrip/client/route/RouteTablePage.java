package org.itam.roadtrip.client.route;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractDateColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractIntegerColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractLongColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.OpenUriAction;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.client.ui.form.FormEvent;
import org.eclipse.scout.rt.client.ui.form.FormListener;
import org.eclipse.scout.rt.client.ui.form.fields.button.IButton;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBoxes;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.itam.roadtrip.client.ClientSession;
import org.itam.roadtrip.client.route.RouteTablePage.Table;
import org.itam.roadtrip.shared.ExcelUtil;
import org.itam.roadtrip.shared.GlobalDefines;
import org.itam.roadtrip.shared.maps.MapsUtil;
import org.itam.roadtrip.shared.route.IRouteService;
import org.itam.roadtrip.shared.route.RouteTablePageData;

@Data(RouteTablePageData.class)
public class RouteTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("RouteTablePage");
	}

	@Override
	protected boolean getConfiguredLeaf() {
		return true;
	}

	@Override
	protected void execLoadData(final SearchFilter filter) {
		importPageData(BEANS.get(IRouteService.class).getRouteTableData(filter));
	}

	public class Table extends AbstractTable {

		@Override
		protected Class<? extends IMenu> getConfiguredDefaultMenu() {
			return EditRouteMenu.class;
		}

		@Order(-1000)
		public class ExportToExcelMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("ExportToExcel");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.Header);
			}

			@Override
			protected void execAction() {
				try {
					ExcelUtil.exportToExcel(getTable().getColumnNames(), createContentMap(), GlobalDefines.FEATURE_ROUTE_NAME);
					MessageBoxes.createOk().withHeader(TEXTS.get("ExportSuccessful")).show();
				} catch (final IOException e) {
					e.printStackTrace();
					MessageBoxes.createOk().withHeader(TEXTS.get("ExportFailed")).show();
				}
			}

			private Map<Integer, Collection<String>> createContentMap() {
				final Map<Integer, Collection<String>> contentMap = new HashMap<>(getTable().getRowCount());
				int rowCount = 0;
				for (final ITableRow row : getTable().getRows()) {
					final Collection<String> data = new ArrayList<>(row.getCellCount());
					for (int j = 0; j < row.getCellCount(); j++) {
						data.add(row.getCell(j).getText());
					}
					contentMap.put(Integer.valueOf(rowCount), data);
					rowCount++;
				}
				return contentMap;
			}
		}

		@Order(0)
		public class OpenRouteMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("OpenRouteInGoogle");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				ClientSession.get().getDesktop().openUri(MapsUtil.getUriForRoute(String.valueOf(getSelectedRow().getCell(getRouteIdColumn()).getValue())), OpenUriAction.NEW_WINDOW);
				getTable().getSelectedRow();
			}
		}

		@Order(1000)
		public class EditRouteMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("EditRoute");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				final RouteForm form = new RouteForm();
				form.setRouteId(getRouteIdColumn().getSelectedValue());
				form.addFormListener(new RouteFormListener());
				form.startModify();
			}
		}

		@Order(1500)
		public class DeleteRouteMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("DeleteRoute");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				final IRouteService service = BEANS.get(IRouteService.class);
				service.deleteRoute(getRouteIdColumn().getSelectedValue());
				execLoadData(getSearchFilter());
			}
		}

		@Order(1750)
		public class GenerateRoutesMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("GenerateRoutes");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection, TableMenuType.MultiSelection, TableMenuType.EmptySpace);
			}

			@Override
			protected void execAction() {
				final GenerateRoutesForm form = new GenerateRoutesForm();
				form.addFormListener(new GenerateRoutesFormListener(form));
				form.start();
			}

			private class GenerateRoutesFormListener implements FormListener {

				final GenerateRoutesForm form;

				public GenerateRoutesFormListener(final GenerateRoutesForm form) {
					this.form = form;
				}

				@Override
				public void formChanged(final FormEvent e) {
					if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().getCloseSystemType() == IButton.SYSTEM_TYPE_OK) {
						final IRouteService service = BEANS.get(IRouteService.class);
						service.generateRoutesFromAppointments(form.getDateFromField().getValue(), form.getDateToField().getValue());

						reloadPage();
					}
				}
			}
		}



		@Order(2000)
		public class NewRouteMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("NewRoute");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.EmptySpace, TableMenuType.SingleSelection, TableMenuType.MultiSelection);
			}

			@Override
			protected void execAction() {
				final RouteForm form = new RouteForm();
				form.setRecordstatusId(GlobalDefines.RECORDSTATUS_ACTIVE);
				form.addFormListener(new RouteFormListener());
				form.startNew();
			}
		}

		private class RouteFormListener implements FormListener {

			@Override
			public void formChanged(final FormEvent e) {
				if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().isFormStored()) {
					reloadPage();
				}
			}
		}

		public SirnameColumn getSirnameColumn() {
			return getColumnSet().getColumnByClass(SirnameColumn.class);
		}

		public TotalDistanceColumn getTotalDistanceColumn() {
			return getColumnSet().getColumnByClass(TotalDistanceColumn.class);
		}

		public TripCountColumn getTripCountColumn() {
			return getColumnSet().getColumnByClass(TripCountColumn.class);
		}

		public RouteIdColumn getRouteIdColumn() {
			return getColumnSet().getColumnByClass(RouteIdColumn.class);
		}

		public RecordstatusIdColumn getRecordstatusIdColumn() {
			return getColumnSet().getColumnByClass(RecordstatusIdColumn.class);
		}

		public NameColumn getNameColumn() {
			return getColumnSet().getColumnByClass(NameColumn.class);
		}

		public DateColumn getDateColumn() {
			return getColumnSet().getColumnByClass(DateColumn.class);
		}



		@Order(0)
		public class RouteIdColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("ID");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}

			@Override
			protected boolean getConfiguredDisplayable() {
				return false;
			}

			@Override
			protected boolean getConfiguredPrimaryKey() {
				return true;
			}
		}



		@Order(1000)
		public class DateColumn extends AbstractDateColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Date");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(2000)
		public class NameColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Name");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(3000)
		public class SirnameColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Sirname");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(4000)
		public class TotalDistanceColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("TotalDistance");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(5000)
		public class TripCountColumn extends AbstractLongColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Count");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(6000)
		public class RecordstatusIdColumn extends AbstractIntegerColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("RecordstatusId");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}

			@Override
			protected boolean getConfiguredDisplayable() {
				return false;
			}
		}


	}
}
