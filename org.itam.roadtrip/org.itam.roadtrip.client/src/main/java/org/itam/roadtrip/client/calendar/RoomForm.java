package org.itam.roadtrip.client.calendar;

import java.awt.Color;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.itam.roadtrip.client.calendar.RoomForm.MainBox.CancelButton;
import org.itam.roadtrip.client.calendar.RoomForm.MainBox.ColorField;
import org.itam.roadtrip.client.calendar.RoomForm.MainBox.IsInhouseField;
import org.itam.roadtrip.client.calendar.RoomForm.MainBox.NameField;
import org.itam.roadtrip.client.calendar.RoomForm.MainBox.OkButton;
import org.itam.roadtrip.shared.ColorsCodeType;
import org.itam.roadtrip.shared.ColorsUtil;
import org.itam.roadtrip.shared.calendar.CreateRoomPermission;
import org.itam.roadtrip.shared.calendar.IRoomService;
import org.itam.roadtrip.shared.calendar.RoomFormData;
import org.itam.roadtrip.shared.calendar.UpdateRoomPermission;

@FormData(value = RoomFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class RoomForm extends AbstractForm {

	private String roomId;

	private Integer recordstatusId;

	@FormData
	public String getRoomId() {
		return roomId;
	}

	@FormData
	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}

	@FormData
	public Integer getRecordstatusId() {
		return recordstatusId;
	}

	@FormData
	public void setRecordstatusId(final Integer recordstatusId) {
		this.recordstatusId = recordstatusId;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Room");
	}

	@Override
	public Object computeExclusiveKey() {
		return getRoomId();
	}

	@Override
	protected int getConfiguredDisplayHint() {
		return IForm.DISPLAY_HINT_VIEW;
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public IsInhouseField getIsInhouseField() {
		return getFieldByClass(IsInhouseField.class);
	}

	public ColorField getColorField() {
		return getFieldByClass(ColorField.class);
	}

	public NameField getNameField() {
		return getFieldByClass(NameField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class NameField extends AbstractStringField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Name");
			}

			@Override
			protected int getConfiguredMaxLength() {
				return 100;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			//			@Override
			//			protected String validateValueInternal(final String rawValue) {
			//				if (rawValue == null || rawValue.trim().length() == 0 ) {
			//					throw new VetoException(TEXTS.get("Name") + TEXTS.get("notEmpty"));
			//				}
			//				return super.execValidateValue(rawValue);
			//			}
		}

		@Order(2000)
		public class IsInhouseField extends AbstractBooleanField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("IsInhouse");
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}
		}

		@Order(3000)
		public class ColorField extends AbstractSmartField<String> {

			@Override
			protected Class<? extends ICodeType<?, String>> getConfiguredCodeType() {
				return ColorsCodeType.class;
			}

			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Color");
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected void execChangedValue() {
				final Color color = ColorsUtil.getColorById(getValue());
				if (color == null) {
					setBackgroundColor(null);
				}
				else {
					final String hex = Integer.toHexString(color.getRGB()).substring(2);
					setBackgroundColor(hex);
				}
			}

		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IRoomService service = BEANS.get(IRoomService.class);
			RoomFormData formData = new RoomFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			setEnabledPermission(new UpdateRoomPermission());

			getForm().setSubTitle(calculateSubTitle());

			getColorField().execChangedValue();
		}

		@Override
		protected void execStore() {
			final IRoomService service = BEANS.get(IRoomService.class);
			final RoomFormData formData = new RoomFormData();
			exportFormData(formData);
			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IRoomService service = BEANS.get(IRoomService.class);
			RoomFormData formData = new RoomFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			setEnabledPermission(new CreateRoomPermission());
		}

		@Override
		protected void execStore() {
			final IRoomService service = BEANS.get(IRoomService.class);
			final RoomFormData formData = new RoomFormData();
			exportFormData(formData);
			service.create(formData);
		}
	}

	private String calculateSubTitle() {
		return getNameField().getValue();
	}
}
