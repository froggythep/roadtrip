package org.itam.roadtrip.client.route;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractIntegerColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.OpenUriAction;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.FormEvent;
import org.eclipse.scout.rt.client.ui.form.FormListener;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.ContentAssistFieldTable;
import org.eclipse.scout.rt.client.ui.form.fields.tablefield.AbstractTableField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;
import org.eclipse.scout.rt.shared.services.lookup.ILookupRow;
import org.itam.roadtrip.client.ClientSession;
import org.itam.roadtrip.client.route.RouteForm.MainBox.CancelButton;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.EmployeeField;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.OpenRouteButton;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.OriginAddressField;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.OriginField;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.RouteDateField;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.TotalDistanceField;
import org.itam.roadtrip.client.route.RouteForm.MainBox.GeneralBox.TripField;
import org.itam.roadtrip.client.route.RouteForm.MainBox.OkButton;
import org.itam.roadtrip.shared.GlobalDefines;
import org.itam.roadtrip.shared.maps.MapsUtil;
import org.itam.roadtrip.shared.person.EmployeeLookupCall;
import org.itam.roadtrip.shared.person.PersonLookupCall;
import org.itam.roadtrip.shared.person.PersonTableRowData;
import org.itam.roadtrip.shared.route.CreateRoutePermission;
import org.itam.roadtrip.shared.route.IRouteService;
import org.itam.roadtrip.shared.route.RouteFormData;
import org.itam.roadtrip.shared.route.RouteFormData.Trip.TripRowData;
import org.itam.roadtrip.shared.route.UpdateRoutePermission;

@FormData(value = RouteFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class RouteForm extends AbstractForm {

	private String routeId;

	private Integer recordstatusId;

	@FormData
	public String getRouteId() {
		return routeId;
	}

	@FormData
	public void setRouteId(final String routeId) {
		this.routeId = routeId;
	}

	@FormData
	public Integer getRecordstatusId() {
		return recordstatusId;
	}

	@FormData
	public void setRecordstatusId(final Integer recordstatusId) {
		this.recordstatusId = recordstatusId;
	}

	@Override
	public Object computeExclusiveKey() {
		return getRouteId();
	}

	@Override
	protected int getConfiguredDisplayHint() {
		return IForm.DISPLAY_HINT_VIEW;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("RouteEditNew");
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public GeneralBox getGeneralBox() {
		return getFieldByClass(GeneralBox.class);
	}

	public EmployeeField getEmployeeField() {
		return getFieldByClass(EmployeeField.class);
	}

	public OriginField getOriginField() {
		return getFieldByClass(OriginField.class);
	}

	public OriginAddressField getOriginAddressField() {
		return getFieldByClass(OriginAddressField.class);
	}

	public RouteDateField getDateField() {
		return getFieldByClass(RouteDateField.class);
	}

	public TripField getTripField() {
		return getFieldByClass(TripField.class);
	}

	public TotalDistanceField getTotalDistanceField() {
		return getFieldByClass(TotalDistanceField.class);
	}

	public OpenRouteButton getOpenRouteButton() {
		return getFieldByClass(OpenRouteButton.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class GeneralBox extends AbstractGroupBox {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("RouteEditNew");
			}

			@Order(1000)
			public class EmployeeField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Employee") + ":";
				}

				@Override
				protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
					return EmployeeLookupCall.class;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}

				@Override
				protected String validateValueInternal(final String rawValue) {
					if (rawValue != null && rawValue.trim().length() == 0 ) {
						throw new VetoException(TEXTS.get("Name") + TEXTS.get("notEmpty"));
					}
					return super.execValidateValue(rawValue);
				}

				public void setDefinedField(final String personId) {
					try {
						final EmployeeLookupCall plc = (EmployeeLookupCall) getConfiguredLookupCall().newInstance();
						plc.setKey(personId);
						final List<? extends ILookupRow<String>> rows = plc.getDataByKey();
						setCurrentLookupRow(rows.get(0));
						setValue(((PersonTableRowData) rows.get(0).getAdditionalTableRowData()).getName());
					} catch (InstantiationException | IllegalAccessException e) {
						e.printStackTrace();
					}
					execChangedValue();
				}

				@Order(10.0)
				public class Table extends ContentAssistFieldTable<String> {

					@Override
					protected boolean getConfiguredHeaderVisible() {
						return true;
					}

					public SirnameColumn getSirnameColumn() {
						return getColumnSet().getColumnByClass(SirnameColumn.class);
					}

					public StreetColumn getStreetColumn() {
						return getColumnSet().getColumnByClass(StreetColumn.class);
					}

					public PlzColumn getPlzColumn() {
						return getColumnSet().getColumnByClass(PlzColumn.class);
					}

					public CityColumn getCityColumn() {
						return getColumnSet().getColumnByClass(CityColumn.class);
					}

					public PersonIdColumn getPersonIdColumn() {
						return getColumnSet().getColumnByClass(PersonIdColumn.class);
					}

					public NameColumn getNameColumn() {
						return getColumnSet().getColumnByClass(NameColumn.class);
					}

					@Order(0)
					public class PersonIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(1000)
					public class NameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Name");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(2000)
					public class SirnameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Sirname");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(3000)
					public class StreetColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Street");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(4000)
					public class PlzColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("PLZ");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(5000)
					public class CityColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("City");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}
				}
			}

			@Order(2000)
			public class OriginField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("OriginPlace") + ":";
				}

				@Override
				protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
					return PersonLookupCall.class;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}

				@Override
				protected void execChangedValue() {
					super.execChangedValue();
					if (getCurrentLookupRow() != null) {
						final PersonTableRowData data = (PersonTableRowData) getCurrentLookupRow().getAdditionalTableRowData();
						getOriginAddressField().setValue(data.getStreet() + " " + data.getPlz() + " " + data.getCity());
					}
				}

				public void setDefinedField(final String personId) {
					try {
						final PersonLookupCall plc = (PersonLookupCall) getConfiguredLookupCall().newInstance();
						plc.setKey(personId);
						final List<? extends ILookupRow<String>> rows = plc.getDataByKey();
						setCurrentLookupRow(rows.get(0));
						setValue(((PersonTableRowData) rows.get(0).getAdditionalTableRowData()).getName());
					} catch (InstantiationException | IllegalAccessException e) {
						e.printStackTrace();
					}
					execChangedValue();
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				protected String validateValueInternal(final String rawValue) {
					if (rawValue == null || rawValue.trim().length() == 0 ) {
						throw new VetoException(TEXTS.get("Name") + TEXTS.get("notEmpty"));
					}
					return super.execValidateValue(rawValue);
				}

				@Order(10.0)
				public class Table extends ContentAssistFieldTable<Long> {

					public SirnameColumn getSirnameColumn() {
						return getColumnSet().getColumnByClass(SirnameColumn.class);
					}

					public StreetColumn getStreetColumn() {
						return getColumnSet().getColumnByClass(StreetColumn.class);
					}

					public PlzColumn getPlzColumn() {
						return getColumnSet().getColumnByClass(PlzColumn.class);
					}

					public CityColumn getCityColumn() {
						return getColumnSet().getColumnByClass(CityColumn.class);
					}

					public PersonIdColumn getPersonIdColumn() {
						return getColumnSet().getColumnByClass(PersonIdColumn.class);
					}

					public NameColumn getNameColumn() {
						return getColumnSet().getColumnByClass(NameColumn.class);
					}

					@Order(0)
					public class PersonIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(1000)
					public class NameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Name");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(2000)
					public class SirnameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Sirname");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(3000)
					public class StreetColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Street");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(4000)
					public class PlzColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("PLZ");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(5000)
					public class CityColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("City");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}
				}
			}

			@Order(3000)
			@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
			public class OriginAddressField extends AbstractLabelField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Origin") + ":";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}

			@Order(4000)
			public class RouteDateField extends AbstractDateField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Date") + ":";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}

				@Override
				protected Date execValidateValue(final Date rawValue) {
					return super.execValidateValue(rawValue);
				}

				@Override
				protected void execInitField() {
					setValue(new Date(System.currentTimeMillis()));
				}

				@Override
				protected Date validateValueInternal(final Date rawValue) {
					if (rawValue == null) {
						throw new VetoException(TEXTS.get("Name") + TEXTS.get("notEmpty"));
					}
					return super.execValidateValue(rawValue);
				}
			}

			@Order(5000)
			public class TripField extends AbstractTableField<TripField.Table> {
				public class Table extends AbstractTable {

					@Order(1000)
					public class NewTripMenu extends AbstractMenu {
						@Override
						protected String getConfiguredText() {
							return TEXTS.get("NewTrip");
						}

						@Override
						protected Set<? extends IMenuType> getConfiguredMenuTypes() {
							return CollectionUtility.hashSet(TableMenuType.EmptySpace, TableMenuType.SingleSelection);
						}

						@Override
						protected void execAction() {
							ILookupRow<String> row = null;
							if (getTripField().getTable().getRowCount() > 0) {
								final ITableRow tableRow = getTripField().getTable().getRow(getRowCount() - 1);
								final String defaultValue = String.valueOf(tableRow.getCellValue(3)).split(",")[0];
								final PersonLookupCall plc = new PersonLookupCall();
								plc.setText(defaultValue);
								final List<? extends ILookupRow<String>> rows = plc.getDataByText();
								if (rows.size() > 0) {
									row = rows.get(0);
								}
							} else {
								row = getOriginField().getCurrentLookupRow();
							}
							final AddTripForm form = new AddTripForm(row);
							form.setRecordstatusId(GlobalDefines.RECORDSTATUS_ACTIVE);
							form.addFormListener(new NewTripFormListener(form));
							form.startNew();
						}

						private class NewTripFormListener implements FormListener {

							private final AddTripForm form;

							private NewTripFormListener(final AddTripForm form) {
								this.form = form;
							}

							@Override
							public void formChanged(final FormEvent e) {
								if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().isFormStored()) {
									createNewRow();
									setTotalDistance();
								}
							}

							private void createNewRow() {
								final PersonTableRowData origin = (PersonTableRowData) form.getOriginField().getCurrentLookupRow().getAdditionalTableRowData();
								final PersonTableRowData destination = (PersonTableRowData) form.getDestinationField().getCurrentLookupRow().getAdditionalTableRowData();
								final ITableRow lastRow = getTripField().getTable().getRow(getTripField().getTable().getRowCount() - 1);
								final int nrInRoute;
								if (lastRow != null) {
									nrInRoute = Integer.valueOf(String.valueOf(lastRow.getCell(0).getValue())) + 1;
								} else {
									nrInRoute = 1;
								}
								final String originAddress = origin.getStreet() + ", " + origin.getPlz() + " " + origin.getCity();
								final String destinationAddress = destination.getStreet() + ", " + destination.getPlz() + " " + destination.getCity();
								final Double distance = MapsUtil.getDistanceForTrip(originAddress, destinationAddress);

								final Table table = getTripField().getTable();
								final ITableRow row = createRow(
										nrInRoute,
										destination.getName(),
										destination.getSirname(),
										destinationAddress,
										distance.toString(),
										origin.getPersonId(),
										destination.getPersonId(),
										form.getRecordstatusId());
								table.addRow(row);
							}
						}
					}

					@Order(2000)
					public class DeleteTripMenu extends AbstractMenu {
						@Override
						protected String getConfiguredText() {
							return TEXTS.get("DeleteTrip");
						}

						@Override
						protected Set<? extends IMenuType> getConfiguredMenuTypes() {
							return CollectionUtility.hashSet(TableMenuType.SingleSelection);
						}

						@Override
						protected void execAction() {
							//TODO
							//							final IRouteService service = BEANS.get(IRouteService.class);
							//							final ITableRow tripRow = getTripField().getTable().getSelectedRow();
							//							getTable().deleteRow(tripRow);
							//							setTotalDistance();
						}
					}

					public ITableRow createRow(final int nrInRoute, final String destName, final String destSirname, final String destAddress,
							final String distance, final String originId, final String destinationId, final Integer recordstatusId) {
						final ITableRow row = super.createRow();
						row.setCellValue(getNumberInRouteColumn().getColumnIndex(), nrInRoute);
						row.setCellValue(getDestinationNameColumn().getColumnIndex(), destName);
						row.setCellValue(getDestinationSirnameColumn().getColumnIndex(), destSirname);
						row.setCellValue(getDestinationAddressColumn().getColumnIndex(), destAddress);
						row.setCellValue(getDistanceColumn().getColumnIndex(), distance);
						row.setCellValue(getOriginIdColumn().getColumnIndex(), originId);
						row.setCellValue(getDestinationIdColumn().getColumnIndex(), destinationId);
						row.setCellValue(getRecordstatusIdColumn().getColumnIndex(), recordstatusId);

						return row;
					}

					public DestinationNameColumn getDestinationNameColumn() {
						return getColumnSet().getColumnByClass(DestinationNameColumn.class);
					}

					public DestinationSirnameColumn getDestinationSirnameColumn() {
						return getColumnSet().getColumnByClass(DestinationSirnameColumn.class);
					}

					public DestinationAddressColumn getDestinationAddressColumn() {
						return getColumnSet().getColumnByClass(DestinationAddressColumn.class);
					}

					public DistanceColumn getDistanceColumn() {
						return getColumnSet().getColumnByClass(DistanceColumn.class);
					}

					public TripIdColumn getTripIdColumn() {
						return getColumnSet().getColumnByClass(TripIdColumn.class);
					}

					public OriginIdColumn getOriginIdColumn() {
						return getColumnSet().getColumnByClass(OriginIdColumn.class);
					}

					public DestinationIdColumn getDestinationIdColumn() {
						return getColumnSet().getColumnByClass(DestinationIdColumn.class);
					}

					public RecordstatusIdColumn getRecordstatusIdColumn() {
						return getColumnSet().getColumnByClass(RecordstatusIdColumn.class);
					}

					public NumberInRouteColumn getNumberInRouteColumn() {
						return getColumnSet().getColumnByClass(NumberInRouteColumn.class);
					}

					@Order(1000)
					public class NumberInRouteColumn extends AbstractIntegerColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("NR");
						}

						@Override
						protected int getConfiguredWidth() {
							return 50;
						}
					}

					@Order(2000)
					public class DestinationNameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Name");
						}

						@Override
						protected int getConfiguredWidth() {
							return 150;
						}
					}

					@Order(3000)
					public class DestinationSirnameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Sirname");
						}

						@Override
						protected int getConfiguredWidth() {
							return 150;
						}
					}

					@Order(4000)
					public class DestinationAddressColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Addresse");
						}

						@Override
						protected int getConfiguredWidth() {
							return 300;
						}
					}

					@Order(5000)
					public class DistanceColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("DistanceFromLastTarget");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(6000)
					public class TripIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}

						@Override
						protected boolean getConfiguredPrimaryKey() {
							return true;
						}
					}

					@Order(7000)
					public class OriginIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Origin") + TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(8000)
					public class DestinationIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Destination") + TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(9000)
					public class RecordstatusIdColumn extends AbstractIntegerColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("RecordstatusId");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}
				}

				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Trips") + ":";
				}

				@Override
				protected int getConfiguredGridH() {
					return 6;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}

			@Order(6000)
			@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
			public class TotalDistanceField extends AbstractLabelField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("TotalDistance") + ":";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}

			@Order(7000)
			public class OpenRouteButton extends AbstractButton {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("OpenRouteInGoogle");
				}

				@Override
				protected void execClickAction() {
					final String startAddress = getOriginAddressField().getValue();
					final LinkedList<String> addresses = new LinkedList<>();
					for (final ITableRow row : getTripField().getTable().getRows()) {
						addresses.add(String.valueOf(row.getCellValue(3)));
					}
					ClientSession.get().getDesktop().openUri(MapsUtil.getUriForRoute(startAddress, addresses), OpenUriAction.NEW_WINDOW);
				}
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {

		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IRouteService service = BEANS.get(IRouteService.class);
			RouteFormData formData = new RouteFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			setEnabledPermission(new UpdateRoutePermission());

			getOriginField().setValue(formData.getOrigin().getValue());
			getOriginField().execChangedValue();
			getTotalDistanceField().setValue(getTotalDistance(formData.getTrip().getRows()));
		}

		private String getTotalDistance(final TripRowData[] trips) {
			Double totalDistance = 0.0;
			for (final TripRowData tripRowData : trips) {
				totalDistance += Double.parseDouble(tripRowData.getDistance());
			}
			return totalDistance.toString().replace(".", ",");
		}

		@Override
		protected void execStore() {
			final IRouteService service = BEANS.get(IRouteService.class);
			final RouteFormData formData = new RouteFormData();
			exportFormData(formData);
			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IRouteService service = BEANS.get(IRouteService.class);
			RouteFormData formData = new RouteFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			getOriginField().setDefinedField(GlobalDefines.PRAXIS_PERSON_ID);

			setEnabledPermission(new CreateRoutePermission());
		}

		@Override
		protected void execStore() {
			final IRouteService service = BEANS.get(IRouteService.class);
			final RouteFormData formData = new RouteFormData();
			exportFormData(formData);
			service.create(formData);
		}
	}

	private void setTotalDistance() {
		Double totalDistance = 0.0;
		for (final ITableRow row : getTripField().getTable().getRows()) {
			if ((int) row.getCell(getTripField().getTable().getRecordstatusIdColumn()).getValue() != GlobalDefines.RECORDSTATUS_DELETED) {
				final String distanceString = String.valueOf(row.getCell(getTripField().getTable().getDistanceColumn()).getValue());
				totalDistance += Double.parseDouble(distanceString);
			}
		}
		getTotalDistanceField().setValue(totalDistance.toString());
	}
}
