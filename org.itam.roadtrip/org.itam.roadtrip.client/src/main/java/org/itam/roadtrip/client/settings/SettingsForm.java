package org.itam.roadtrip.client.settings;

import java.util.HashMap;

import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.itam.roadtrip.client.settings.SettingsForm.MainBox.CancelButton;
import org.itam.roadtrip.client.settings.SettingsForm.MainBox.ExportPathField;
import org.itam.roadtrip.client.settings.SettingsForm.MainBox.GoogleMapsApiKeyField;
import org.itam.roadtrip.client.settings.SettingsForm.MainBox.OkButton;
import org.itam.roadtrip.shared.settings.CreateSettingsPermission;
import org.itam.roadtrip.shared.settings.ISettingsService;
import org.itam.roadtrip.shared.settings.SettingsDefines.SettingsKeys;

public class SettingsForm extends AbstractForm {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Settings");
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public GoogleMapsApiKeyField getGoogleMapsApiKeyField() {
		return getFieldByClass(GoogleMapsApiKeyField.class);
	}

	public ExportPathField getExportPathField() {
		return getFieldByClass(ExportPathField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class GoogleMapsApiKeyField extends AbstractStringField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("GoogleMapsApiKey") + ": ";
			}

			@Override
			protected int getConfiguredMaxLength() {
				return 255;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			public boolean isMandatory() {
				return true;
			}
		}

		@Order(2000)
		public class ExportPathField extends AbstractStringField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("ExportPath") + ": ";
			}

			@Override
			protected int getConfiguredMaxLength() {
				return 255;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			public boolean isMandatory() {
				return true;
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			setEnabledPermission(new CreateSettingsPermission());
			final ISettingsService service = BEANS.get(ISettingsService.class);
			final HashMap<SettingsKeys, String> keyValuePairs = service.loadSettingsKeyValuePairs();
			getGoogleMapsApiKeyField().setValue(keyValuePairs.get(SettingsKeys.GOOGLE_MAPS_API_KEY));
			getExportPathField().setValue(keyValuePairs.get(SettingsKeys.EXPORT_PATH));
		}

		@Override
		protected void execStore() {
			final ISettingsService service = BEANS.get(ISettingsService.class);
			service.storeSettingsKeyValuePair(SettingsKeys.GOOGLE_MAPS_API_KEY, getGoogleMapsApiKeyField().getValue());
			service.storeSettingsKeyValuePair(SettingsKeys.EXPORT_PATH, getExportPathField().getValue());
		}
	}
}
