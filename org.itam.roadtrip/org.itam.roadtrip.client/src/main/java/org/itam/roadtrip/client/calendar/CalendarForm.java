package org.itam.roadtrip.client.calendar;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.CalendarMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.basic.calendar.AbstractCalendar;
import org.eclipse.scout.rt.client.ui.basic.calendar.provider.AbstractCalendarItemProvider;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.FormEvent;
import org.eclipse.scout.rt.client.ui.form.FormListener;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.IButton;
import org.eclipse.scout.rt.client.ui.form.fields.calendarfield.AbstractCalendarField;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.ContentAssistFieldTable;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.calendar.ICalendarItem;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.CalendarBox;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.CalendarBox.CalendarField;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.FilterBox;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.FilterBox.EmployeeField;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.FilterBox.ResetFilterButton;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.FilterBox.RoomField;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.LoadPropertiesBox;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.LoadPropertiesBox.LoadFromField;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.LoadPropertiesBox.LoadToField;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.LoadPropertiesBox.ReloadFromServerButton;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.LoadPropertiesBox.ResetLoadPropertiesButton;
import org.itam.roadtrip.client.calendar.CalendarForm.MainBox.OkButton;
import org.itam.roadtrip.shared.GlobalDefines.UpdateType;
import org.itam.roadtrip.shared.calendar.CalendarAppointmentRoadtrip;
import org.itam.roadtrip.shared.calendar.CalendarFormData;
import org.itam.roadtrip.shared.calendar.CalendarUtil;
import org.itam.roadtrip.shared.calendar.CreateCalendarPermission;
import org.itam.roadtrip.shared.calendar.ICalendarService;
import org.itam.roadtrip.shared.calendar.RoomLookupCall;
import org.itam.roadtrip.shared.person.EmployeeLookupCall;

@FormData(value = CalendarFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class CalendarForm extends AbstractForm {

	final Set<CalendarAppointmentRoadtrip> calendarItems = new HashSet<>();

	private void removeCalendarItemsThisAndFollowingAppointmentsInRepition(final CalendarAppointmentRoadtrip deleteApp) {
		final String repeatId = deleteApp.getAppointmentRepeatId();
		final Date dateFrom = deleteApp.getStart();

		final Collection<CalendarAppointmentRoadtrip> appsToRemove = new HashSet<>();
		for (final CalendarAppointmentRoadtrip app : calendarItems) {
			if (app.getAppointmentRepeatId().equals(repeatId) && (app.getStart().equals(dateFrom) || app.getStart().after(dateFrom))) {
				appsToRemove.add(app);
			}
		}
		calendarItems.removeAll(appsToRemove);
	}

	private void removeCalendarItemsAllAppointmentsInRepition(final CalendarAppointmentRoadtrip deleteApp) {
		final String repeatId = deleteApp.getAppointmentRepeatId();

		final Collection<CalendarAppointmentRoadtrip> appsToRemove = new HashSet<>();
		for (final CalendarAppointmentRoadtrip app : calendarItems) {
			if (app.getAppointmentRepeatId().equals(repeatId)) {
				appsToRemove.add(app);
			}
		}
		calendarItems.removeAll(appsToRemove);
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Calendar");
	}

	@Override
	protected int getConfiguredDisplayHint() {
		return IForm.DISPLAY_HINT_VIEW;
	}

	@Override
	protected boolean getConfiguredAskIfNeedSave() {
		return false;
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public FilterBox getRoomBox() {
		return getFieldByClass(FilterBox.class);
	}

	public RoomField getRoomField() {
		return getFieldByClass(RoomField.class);
	}

	public EmployeeField getEmployeeField() {
		return getFieldByClass(EmployeeField.class);
	}

	public CalendarBox getCalendarBox() {
		return getFieldByClass(CalendarBox.class);
	}

	public CalendarField getCalendarField() {
		return getFieldByClass(CalendarField.class);
	}


	public ResetFilterButton getResetFilterButton() {
		return getFieldByClass(ResetFilterButton.class);
	}

	public LoadFromField getLoadFromField() {
		return getFieldByClass(LoadFromField.class);
	}

	public LoadToField getLoadToField() {
		return getFieldByClass(LoadToField.class);
	}

	public ReloadFromServerButton getReloadFromServerButton() {
		return getFieldByClass(ReloadFromServerButton.class);
	}

	public LoadPropertiesBox getLoadPropertiesBox() {
		return getFieldByClass(LoadPropertiesBox.class);
	}

	public ResetLoadPropertiesButton getResetLoadPropertiesButton() {
		return getFieldByClass(ResetLoadPropertiesButton.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class FilterBox extends AbstractGroupBox {
			@Override
			protected boolean getConfiguredExpandable() {
				return true;
			}

			@Override
			protected boolean getConfiguredExpanded() {
				return false;
			}

			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Filter");
			}

			@Order(1000)
			public class RoomField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Room") + ":";
				}

				@Override
				protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
					return RoomLookupCall.class;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				protected void execChangedValue() {
					super.execChangedValue();
					getCalendarField().reloadCalendarItems();
				}

				@Order(10.0)
				public class Table extends ContentAssistFieldTable<Long> {

					@Override
					protected boolean getConfiguredHeaderVisible() {
						return true;
					}

					public IsInhouseColumn getIsInhouseColumn() {
						return getColumnSet().getColumnByClass(IsInhouseColumn.class);
					}

					public RecordstatusIdColumn getRecordstatusIdColumn() {
						return getColumnSet().getColumnByClass(RecordstatusIdColumn.class);
					}

					public NameColumn getNameColumn() {
						return getColumnSet().getColumnByClass(NameColumn.class);
					}

					@Order(0)
					public class RoomIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(1000)
					public class NameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Name");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(2000)
					public class IsInhouseColumn extends AbstractBooleanColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("IsInhouse");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(3000)
					public class RecordstatusIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("RecordstatusId");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}
				}
			}

			@Order(2000)
			public class EmployeeField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Employee") + ": ";
				}

				@Override
				protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
					return EmployeeLookupCall.class;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				protected void execChangedValue() {
					super.execChangedValue();
					getCalendarField().reloadCalendarItems();
				}

				@Order(10.0)
				public class Table extends ContentAssistFieldTable<Long> {

					@Override
					protected boolean getConfiguredHeaderVisible() {
						return true;
					}

					public SirnameColumn getSirnameColumn() {
						return getColumnSet().getColumnByClass(SirnameColumn.class);
					}

					public StreetColumn getStreetColumn() {
						return getColumnSet().getColumnByClass(StreetColumn.class);
					}

					public PlzColumn getPlzColumn() {
						return getColumnSet().getColumnByClass(PlzColumn.class);
					}

					public CityColumn getCityColumn() {
						return getColumnSet().getColumnByClass(CityColumn.class);
					}

					public PersonIdColumn getPersonIdColumn() {
						return getColumnSet().getColumnByClass(PersonIdColumn.class);
					}

					public NameColumn getNameColumn() {
						return getColumnSet().getColumnByClass(NameColumn.class);
					}

					@Order(0)
					public class PersonIdColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("ID");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}

						@Override
						protected boolean getConfiguredDisplayable() {
							return false;
						}
					}

					@Order(1000)
					public class NameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Name");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(2000)
					public class SirnameColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Sirname");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(3000)
					public class StreetColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("Street");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(4000)
					public class PlzColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("PLZ");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}

					@Order(5000)
					public class CityColumn extends AbstractStringColumn {
						@Override
						protected String getConfiguredHeaderText() {
							return TEXTS.get("City");
						}

						@Override
						protected int getConfiguredWidth() {
							return 100;
						}
					}
				}
			}

			@Order(3000)
			public class ResetFilterButton extends AbstractButton {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("ResetFilter");
				}

				@Override
				protected void execClickAction() {
					getRoomField().resetValue();
					getEmployeeField().resetValue();
				}

				@Override
				protected boolean getConfiguredProcessButton() {
					return false;
				}

				@Override
				protected boolean getConfiguredFillHorizontal() {
					return true;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}
		}

		@Order(1500)
		public class LoadPropertiesBox extends AbstractGroupBox {
			@Override
			protected boolean getConfiguredExpandable() {
				return true;
			}

			@Override
			protected boolean getConfiguredExpanded() {
				return false;
			}

			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("LoadProperties");
			}

			@Order(1000)
			public class LoadFromField extends AbstractDateField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("LoadFrom") + ": ";
				}

				@Override
				protected void execInitField() {
					setValue(CalendarUtil.getMondayOfCurrentWeek());
				}
			}

			@Order(2000)
			public class LoadToField extends AbstractDateField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("LoadTo") + ": ";
				}

				@Override
				protected void execInitField() {
					setValue(CalendarUtil.getSundayInSixMonths());
				}
			}

			@Order(1500)
			public class ResetLoadPropertiesButton extends AbstractButton {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("ResetButton");
				}

				@Override
				protected void execClickAction() {
					getLoadFromField().setValue(CalendarUtil.getMondayOfCurrentWeek());
					getLoadToField().setValue(CalendarUtil.getSundayInSixMonths());

					reload();
				}

				@Override
				protected boolean getConfiguredProcessButton() {
					return false;
				}

				@Override
				protected boolean getConfiguredFillHorizontal() {
					return true;
				}
			}

			@Order(2500)
			public class ReloadFromServerButton extends AbstractButton {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("ReloadFromServer");
				}

				@Override
				protected void execClickAction() {
					reload();
				}

				@Override
				protected boolean getConfiguredProcessButton() {
					return false;
				}

				@Override
				protected boolean getConfiguredFillHorizontal() {
					return true;
				}
			}
		}

		@Order(2000)
		public class CalendarBox extends AbstractGroupBox {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Calendar");
			}

			@Order(1000)
			public class CalendarField extends AbstractCalendarField<CalendarField.Calendar> {

				@Override
				protected boolean getConfiguredLabelVisible() {
					return false;
				}

				@Override
				protected boolean getConfiguredStatusVisible() {
					return false;
				}

				@Override
				protected int getConfiguredGridH() {
					return 30;
				}

				@Override
				protected int getConfiguredGridW() {
					return 0;
				}

				public class Calendar extends AbstractCalendar {


					@Order(1000)
					public class CalendarProvider extends AbstractCalendarItemProvider {

						@Override
						protected void execLoadItems(final Date minDate, final Date maxDate, final Set<ICalendarItem> result) {
							result.addAll(getShownCalendarItems());
						}

						@Order(1000)
						public class NewAppointmentMenu extends AbstractMenu {
							@Override
							protected String getConfiguredText() {
								return TEXTS.get("AddCalendarEntry");
							}

							@Override
							protected Set<? extends IMenuType> getConfiguredMenuTypes() {
								return CollectionUtility.hashSet(CalendarMenuType.EmptySpace, CalendarMenuType.CalendarComponent);
							}

							@Override
							protected void execAction() {
								final AppointmentForm form = new AppointmentForm(getCalendar().getSelectedDate());
								form.addFormListener(new NewAppointmentFormListener(form));
								form.startNew();
							}

							private class NewAppointmentFormListener implements FormListener {

								AppointmentForm form;

								public NewAppointmentFormListener(final AppointmentForm form) {
									this.form = form;
								}

								@Override
								public void formChanged(final FormEvent e) {
									if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().isFormStored()) {
										form.saveNew();
										reload();
									}
								}
							}
						}

						@Order(1500)
						public class EditAppointmentMenu extends AbstractMenu {
							@Override
							protected String getConfiguredText() {
								return TEXTS.get("EditAppointment");
							}

							@Override
							protected Set<? extends IMenuType> getConfiguredMenuTypes() {
								return CollectionUtility.hashSet(CalendarMenuType.CalendarComponent);
							}

							@Override
							protected void execAction() {
								final AppointmentForm form = new AppointmentForm((CalendarAppointmentRoadtrip) getCalendar().getSelectedComponent().getItem());
								form.addFormListener(new AppointmentFormEditListener(form));
								form.startModify();
							}

							private class AppointmentFormEditListener implements FormListener {

								AppointmentForm form;

								public AppointmentFormEditListener(final AppointmentForm form) {
									this.form = form;
								}

								@Override
								public void formChanged(final FormEvent e) {
									if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().isFormStored()) {
										final CalendarAppointmentRoadtrip oldApp = (CalendarAppointmentRoadtrip) getCalendar().getSelectedComponent().getItem();
										final CalendarAppointmentRoadtrip updatedApp = form.getUpdatedCalendarAppointmentRoadtrip();
										if (!oldApp.isRepeating() && !updatedApp.isRepeating()) {
											form.saveModifiedSingleAppointment();
											reload();
										} else {
											final UpdateAppointmentForm form = new UpdateAppointmentForm();
											form.addFormListener(new UpdateTypeFormEditListener(form, oldApp, updatedApp));
											form.start();
										}
									}
								}
							}

							private class UpdateTypeFormEditListener implements FormListener {

								private final UpdateAppointmentForm form;
								private final CalendarAppointmentRoadtrip oldApp;
								private final CalendarAppointmentRoadtrip updatedApp;

								public UpdateTypeFormEditListener(final UpdateAppointmentForm form, final CalendarAppointmentRoadtrip oldApp, final CalendarAppointmentRoadtrip updatedApp) {
									this.form = form;
									this.oldApp = oldApp;
									this.updatedApp = updatedApp;
								}

								@Override
								public void formChanged(final FormEvent e) {
									if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().getCloseSystemType() == 3) {
										final UpdateType updateType = form.getUpdateType();
										final ICalendarService service = BEANS.get(ICalendarService.class);
										service.updateAppointment(updateType, oldApp, updatedApp);

										reload();
									}
								}
							}
						}


						@Order(2000)
						public class DeleteAppointmentMenu extends AbstractMenu {
							@Override
							protected String getConfiguredText() {
								return TEXTS.get("DeleteAppointment");
							}

							@Override
							protected Set<? extends IMenuType> getConfiguredMenuTypes() {
								return CollectionUtility.hashSet(CalendarMenuType.CalendarComponent);
							}

							@Override
							protected void execAction() {
								final CalendarAppointmentRoadtrip app = (CalendarAppointmentRoadtrip) getCalendar().getSelectedComponent().getItem();
								if (app.isRepeating() && app.isRepeatTillInfinity()) {
									//TODO ASK TO DELETE THIS/ALL/FOLLOWING
								} else if (app.isRepeating() && !app.isRepeatTillInfinity() && app.getRepeatTill() != null) {
									final DeleteAppointmentConfirmationForm deleteForm = new DeleteAppointmentConfirmationForm();
									deleteForm.addFormListener(new DeleteAppointmentFormListener(deleteForm));
									deleteForm.start();
								} else {
									deleteSingleAppointment(app);
								}
							}

							private class DeleteAppointmentFormListener implements FormListener {

								DeleteAppointmentConfirmationForm form;

								public DeleteAppointmentFormListener(final DeleteAppointmentConfirmationForm form) {
									this.form = form;
								}

								@Override
								public void formChanged(final FormEvent e) {
									if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().getCloseSystemType() == IButton.SYSTEM_TYPE_OK) {
										final CalendarAppointmentRoadtrip app = (CalendarAppointmentRoadtrip) getCalendar().getSelectedComponent().getItem();
										if (form.getDeleteTypeBox().getValue().equals(form.getDeleteOnlyThisButton().getRadioValue())) {
											deleteSingleAppointment(app);
										} else if (form.getDeleteTypeBox().getValue().equals(form.getDeleteThisAndFollowingButton().getRadioValue())) {
											deleteThisAndFollowingAppointments(app);
										} else if (form.getDeleteTypeBox().getValue().equals(form.getDeleteAllButton().getRadioValue())) {
											deleteAllRepeatingAppointments(app);
										}
									}
								}
							}

							private void deleteSingleAppointment(final CalendarAppointmentRoadtrip app) {
								final ICalendarService service = BEANS.get(ICalendarService.class);
								service.deleteAppointment(app.getAppointmentId());

								calendarItems.remove(app);
								getCalendarField().reloadCalendarItems();
							}

							private void deleteAllRepeatingAppointments(final CalendarAppointmentRoadtrip app) {
								final ICalendarService service = BEANS.get(ICalendarService.class);
								service.deleteAllAppointmentsInRepetition(app.getAppointmentRepeatId());

								removeCalendarItemsAllAppointmentsInRepition(app);
								getCalendarField().reloadCalendarItems();
							}

							private void deleteThisAndFollowingAppointments(final CalendarAppointmentRoadtrip app) {
								final ICalendarService service = BEANS.get(ICalendarService.class);
								service.deleteThisAndFollowingAppointmentsInRepetition(app.getAppointmentRepeatId(), app.getStart());

								removeCalendarItemsThisAndFollowingAppointmentsInRepition(app);
								getCalendarField().reloadCalendarItems();
							}
						}
					}
				}
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final ICalendarService service = BEANS.get(ICalendarService.class);

			calendarItems.clear();
			calendarItems.addAll(service.getAllAppointments(getLoadFromField().getValue(), getLoadToField().getValue()));

			setEnabledPermission(new CreateCalendarPermission());
			getCalendarField().reloadCalendarItems();
		}

	}

	private Collection<CalendarAppointmentRoadtrip> getShownCalendarItems() {
		final Collection<CalendarAppointmentRoadtrip> shownCalendarItems = new HashSet<>();
		if (getRoomField().getCurrentLookupRow() == null && getEmployeeField().getCurrentLookupRow() == null) {
			shownCalendarItems.addAll(calendarItems);
		} else {
			if (getRoomField().getCurrentLookupRow() != null && getEmployeeField().getCurrentLookupRow() == null) {
				for (final CalendarAppointmentRoadtrip citem : calendarItems) {
					if (citem.getRoomId().equals(getRoomField().getCurrentLookupRow().getKey())) {
						shownCalendarItems.add(citem);
					}
				}
			} else if (getRoomField().getCurrentLookupRow() == null && getEmployeeField().getCurrentLookupRow() != null) {
				for (final CalendarAppointmentRoadtrip citem : calendarItems) {
					if (citem.getEmployeeId().equals(getEmployeeField().getCurrentLookupRow().getKey())) {
						shownCalendarItems.add(citem);
					}
				}
			} else if (getRoomField().getCurrentLookupRow() != null && getEmployeeField().getCurrentLookupRow() != null) {
				final Collection<CalendarAppointmentRoadtrip> tempItems = new HashSet<>();
				for (final CalendarAppointmentRoadtrip citem : calendarItems) {
					if (citem.getRoomId().equals(getRoomField().getCurrentLookupRow().getKey())) {
						tempItems.add(citem);
					}
				}
				for (final CalendarAppointmentRoadtrip citem : tempItems) {
					if (citem.getEmployeeId().equals(getEmployeeField().getCurrentLookupRow().getKey())) {
						shownCalendarItems.add(citem);
					}
				}
			}
		}
		return shownCalendarItems;
	}

	private void reload() {
		final ICalendarService service = BEANS.get(ICalendarService.class);
		calendarItems.clear();
		calendarItems.addAll(service.getAllAppointments(getLoadFromField().getValue(), getLoadToField().getValue()));

		getCalendarField().reloadCalendarItems();
	}
}
