package org.itam.roadtrip.client.person;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.radiobuttongroup.AbstractRadioButtonGroup;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.itam.roadtrip.client.person.PersonForm.MainBox.CancelButton;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.BirthdayField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.CityField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.HealthInsuranceField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.NameField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.PhoneNumberField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.PlzField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.PositionGroup;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.SirnameField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.GeneralBoxBox.StreetField;
import org.itam.roadtrip.client.person.PersonForm.MainBox.OkButton;
import org.itam.roadtrip.shared.person.CreatePersonPermission;
import org.itam.roadtrip.shared.person.IPersonService;
import org.itam.roadtrip.shared.person.PersonFormData;
import org.itam.roadtrip.shared.person.PositionCodeType;

@FormData(value = PersonFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class PersonForm extends AbstractForm {

	private String personId;

	private Integer recordstatusId;

	@FormData
	public String getPersonId() {
		return personId;
	}

	@FormData
	public void setPersonId(final String personId) {
		this.personId = personId;
	}

	@FormData
	public Integer getRecordstatusId() {
		return recordstatusId;
	}

	@FormData
	public void setRecordstatusId(final Integer recordstatusId) {
		this.recordstatusId = recordstatusId;
	}

	@Override
	public Object computeExclusiveKey() {
		return getPersonId();
	}

	@Override
	protected int getConfiguredDisplayHint() {
		return IForm.DISPLAY_HINT_VIEW;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("PersonEditNew");
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public GeneralBoxBox getGeneralBoxBox() {
		return getFieldByClass(GeneralBoxBox.class);
	}

	public BirthdayField getBirthdayField() {
		return getFieldByClass(BirthdayField.class);
	}

	public StreetField getStreetField() {
		return getFieldByClass(StreetField.class);
	}

	public CityField getCityField() {
		return getFieldByClass(CityField.class);
	}

	public PlzField getPlzField() {
		return getFieldByClass(PlzField.class);
	}

	public PositionGroup getPositionBox() {
		return getFieldByClass(PositionGroup.class);
	}

	public HealthInsuranceField getHealthInsuranceField() {
		return getFieldByClass(HealthInsuranceField.class);
	}

	public PhoneNumberField getPhoneNumberField() {
		return getFieldByClass(PhoneNumberField.class);
	}

	public NameField getNameField() {
		return getFieldByClass(NameField.class);
	}

	public SirnameField getSirnameField() {
		return getFieldByClass(SirnameField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class GeneralBoxBox extends AbstractGroupBox {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("PersonInformationen");
			}

			@Order(1000)
			public class NameField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Name") + ": ";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}

			}

			@Order(2000)
			public class SirnameField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Sirname") + ": ";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}
			}

			@Order(3000)
			public class BirthdayField extends AbstractDateField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Birthday") + ": ";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}

			@Order(4000)
			public class StreetField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Street") + ": ";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}

			}

			@Order(5000)
			public class PlzField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("PLZ");
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}
			}

			@Order(6000)
			public class CityField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("City") + ": ";
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}
			}

			@Order(6500)
			public class HealthInsuranceField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("HealthInsurance");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 100;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}

			@Order(6750)
			public class PhoneNumberField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("PhoneNumber");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 20;
				}

				@Override
				protected int getConfiguredGridW() {
					return 2;
				}
			}

			@Order(7000)
			public class PositionGroup extends AbstractRadioButtonGroup<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Position");
				}

				@Override
				protected Class<? extends ICodeType<?, String>> getConfiguredCodeType() {
					return PositionCodeType.class;
				}

				@Override
				public boolean isMandatory() {
					return true;
				}
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IPersonService service = BEANS.get(IPersonService.class);
			PersonFormData formData = new PersonFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			getForm().setSubTitle(calculateSubTitle());
		}

		@Override
		protected void execStore() {
			final IPersonService service = BEANS.get(IPersonService.class);
			final PersonFormData formData = new PersonFormData();
			exportFormData(formData);
			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IPersonService service = BEANS.get(IPersonService.class);
			PersonFormData formData = new PersonFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			setEnabledPermission(new CreatePersonPermission());
		}

		@Override
		protected void execStore() {
			final IPersonService service = BEANS.get(IPersonService.class);
			final PersonFormData formData = new PersonFormData();
			exportFormData(formData);
			service.create(formData);
		}
	}

	private String calculateSubTitle() {
		return StringUtility.join(" ", getNameField().getValue(),
				getSirnameField().getValue());
	}
}
