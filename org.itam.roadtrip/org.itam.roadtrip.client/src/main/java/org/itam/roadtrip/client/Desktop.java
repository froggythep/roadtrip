package org.itam.roadtrip.client;

import java.util.List;
import java.util.Set;

import org.eclipse.scout.rt.client.ui.action.keystroke.IKeyStroke;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.desktop.AbstractDesktop;
import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutlineViewButton;
import org.eclipse.scout.rt.client.ui.desktop.outline.IOutline;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.itam.roadtrip.client.calendar.CalendarForm;
import org.itam.roadtrip.client.calendar.CalendarOutline;
import org.itam.roadtrip.client.person.PersonForm;
import org.itam.roadtrip.client.person.PersonOutline;
import org.itam.roadtrip.client.route.RouteForm;
import org.itam.roadtrip.client.route.RouteOutline;
import org.itam.roadtrip.client.settings.SettingsForm;
import org.itam.roadtrip.shared.Icons;

/**
 * <h3>{@link Desktop}</h3>
 */
public class Desktop extends AbstractDesktop {
	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("ApplicationTitle");
	}

	@Override
	protected String getConfiguredLogoId() {
		return Icons.AppLogo;
	}

	@Override
	protected List<Class<? extends IOutline>> getConfiguredOutlines() {
		return CollectionUtility.<Class<? extends IOutline>>arrayList(
				PersonOutline.class,
				RouteOutline.class,
				CalendarOutline.class);
	}

	@Override
	protected void execDefaultView() {
		selectFirstVisibleOutline();
	}

	protected void selectFirstVisibleOutline() {
		for (final IOutline outline : getAvailableOutlines()) {
			if (outline.isEnabled() && outline.isVisible()) {
				setOutline(outline.getClass());
				return;
			}
		}
	}

	@Order(-1000)
	public class CalendarMenu extends AbstractMenu {
		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Calendar");
		}

		@Override
		protected String getConfiguredIconId() {
			return Icons.Calendar;
		}

		@Override
		protected Set<? extends IMenuType> getConfiguredMenuTypes() {
			return CollectionUtility.hashSet();
		}

		@Override
		protected void execAction() {
			final CalendarForm form = new CalendarForm();
			form.startNew();
		}
	}

	@Order(10)
	public class QuickAccessMenuMenu extends AbstractMenu {

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("QuickAccess");
		}

		@Order(1000)
		public class NewPersonMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("CreateNewPerson");
			}

			@Override
			protected void execAction() {
				new PersonForm().startNew();
			}
		}

		@Order(2000)
		public class NewRouteMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("NewRoute");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet();
			}

			@Override
			protected void execAction() {
				new RouteForm().startNew();
			}
		}
	}

	@Order(2000)
	public class SettingsMenu extends AbstractMenu {
		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Settings");
		}

		@Override
		protected Set<? extends IMenuType> getConfiguredMenuTypes() {
			return CollectionUtility.hashSet();
		}

		@Override
		protected void execAction() {
			new SettingsForm().startNew();
		}
	}



	@Order(1000)
	public class PersonOutlineViewButton extends AbstractOutlineViewButton {

		public PersonOutlineViewButton() {
			this(PersonOutline.class);
		}

		protected PersonOutlineViewButton(final Class<? extends PersonOutline> outlineClass) {
			super(Desktop.this, outlineClass);
		}

		@Override
		protected DisplayStyle getConfiguredDisplayStyle() {
			return DisplayStyle.TAB;
		}

		@Override
		protected String getConfiguredKeyStroke() {
			return IKeyStroke.F2;
		}
	}

	@Order(2000)
	public class RouteOutlineViewButton extends AbstractOutlineViewButton {

		public RouteOutlineViewButton() {
			this(RouteOutline.class);
		}

		protected RouteOutlineViewButton(final Class<? extends RouteOutline> outlineClass) {
			super(Desktop.this, outlineClass);
		}

		@Override
		protected DisplayStyle getConfiguredDisplayStyle() {
			return DisplayStyle.TAB;
		}

		@Override
		protected String getConfiguredKeyStroke() {
			return IKeyStroke.F3;
		}
	}

	@Order(3000)
	public class CalendarOutlineViewButton extends AbstractOutlineViewButton {

		public CalendarOutlineViewButton() {
			this(CalendarOutline.class);
		}

		protected CalendarOutlineViewButton(final Class<? extends CalendarOutline> outlineClass) {
			super(Desktop.this, outlineClass);
		}

		@Override
		protected DisplayStyle getConfiguredDisplayStyle() {
			return DisplayStyle.TAB;
		}

		@Override
		protected String getConfiguredKeyStroke() {
			return IKeyStroke.F3;
		}
	}
}
