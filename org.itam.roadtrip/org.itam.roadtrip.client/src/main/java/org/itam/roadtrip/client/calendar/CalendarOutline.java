package org.itam.roadtrip.client.calendar;

import java.util.List;

import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutline;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.itam.roadtrip.shared.Icons;

/**
 * <h3>{@link CalendarOutline}</h3>
 */
@Order(1000)
public class CalendarOutline extends AbstractOutline {

	@Override
	protected void execCreateChildPages(final List<IPage<?>> pageList) {
		pageList.add(new RoomTablePage());
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Room");
	}

	@Override
	protected String getConfiguredIconId() {
		return Icons.Square;
	}
}
