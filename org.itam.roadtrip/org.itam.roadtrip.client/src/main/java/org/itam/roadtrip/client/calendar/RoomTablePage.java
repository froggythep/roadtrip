package org.itam.roadtrip.client.calendar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.cell.Cell;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractIntegerColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.client.ui.form.FormEvent;
import org.eclipse.scout.rt.client.ui.form.FormListener;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBoxes;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.itam.roadtrip.client.calendar.RoomTablePage.Table;
import org.itam.roadtrip.shared.ColorsUtil;
import org.itam.roadtrip.shared.ExcelUtil;
import org.itam.roadtrip.shared.GlobalDefines;
import org.itam.roadtrip.shared.calendar.IRoomService;
import org.itam.roadtrip.shared.calendar.RoomTablePageData;

@Data(RoomTablePageData.class)
public class RoomTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Rooms");
	}

	@Override
	protected boolean getConfiguredLeaf() {
		return true;
	}

	@Override
	protected void execLoadData(final SearchFilter filter) {
		importPageData(BEANS.get(IRoomService.class).getRoomTableData(filter));
	}

	public class Table extends AbstractTable {

		@Override
		protected Class<? extends IMenu> getConfiguredDefaultMenu() {
			return EditMenu.class;
		}

		@Order(0)
		public class ExportToExcelMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("ExportToExcel");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.Header);
			}

			@Override
			protected void execAction() {
				try {
					ExcelUtil.exportToExcel(getTable().getColumnNames(), createContentMap(), GlobalDefines.FEATURE_ROOM_NAME);
					MessageBoxes.createOk().withHeader(TEXTS.get("ExportSuccessful")).show();
				} catch (final IOException e) {
					e.printStackTrace();
					MessageBoxes.createOk().withHeader(TEXTS.get("ExportFailed")).show();
				}
			}

			private Map<Integer, Collection<String>> createContentMap() {
				final Map<Integer, Collection<String>> contentMap = new HashMap<>(getTable().getRowCount());
				int rowCount = 0;
				for (final ITableRow row : getTable().getRows()) {
					final Collection<String> data = new ArrayList<>(row.getCellCount());
					for (int j = 0; j < row.getCellCount(); j++) {
						data.add(row.getCell(j).getText());
					}
					contentMap.put(Integer.valueOf(rowCount), data);
					rowCount++;
				}
				return contentMap;
			}
		}



		@Order(1000)
		public class EditMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("EditRoom");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				final RoomForm form = new RoomForm();
				form.setRoomId(getRoomIdColumn().getSelectedValue());
				form.setRecordstatusId(getRecordstatusIdColumn().getSelectedValue());
				form.addFormListener(new RoomFormListener());
				form.startModify();
			}
		}

		@Order(2000)
		public class NewMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("NewRoom");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.EmptySpace, TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				final RoomForm form = new RoomForm();
				form.setRecordstatusId(GlobalDefines.RECORDSTATUS_ACTIVE);
				form.addFormListener(new RoomFormListener());
				form.startNew();
			}
		}

		@Order(3000)
		public class DeleteRoomMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("DeleteRoom");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				final IRoomService service = BEANS.get(IRoomService.class);
				service.deleteRoom(getRoomIdColumn().getSelectedValue());
				execLoadData(getSearchFilter());
			}
		}

		private class RoomFormListener implements FormListener {

			@Override
			public void formChanged(final FormEvent e) {
				if (FormEvent.TYPE_CLOSED == e.getType() && e.getForm().isFormStored()) {
					reloadPage();
				}
			}
		}

		public IsInhouseColumn getIsInhouseColumn() {
			return getColumnSet().getColumnByClass(IsInhouseColumn.class);
		}

		public RecordstatusIdColumn getRecordstatusIdColumn() {
			return getColumnSet().getColumnByClass(RecordstatusIdColumn.class);
		}

		public ColorColumn getColorColumn() {
			return getColumnSet().getColumnByClass(ColorColumn.class);
		}

		public NameColumn getNameColumn() {
			return getColumnSet().getColumnByClass(NameColumn.class);
		}

		public RoomIdColumn getRoomIdColumn() {
			return getColumnSet().getColumnByClass(RoomIdColumn.class);
		}

		@Order(1000)
		public class RoomIdColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("ID");
			}

			@Override
			protected int getConfiguredWidth() {
				return 50;
			}

			@Override
			protected boolean getConfiguredDisplayable() {
				return false;
			}

			@Override
			protected boolean getConfiguredPrimaryKey() {
				return true;
			}
		}

		@Order(2000)
		public class NameColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Name");
			}

			@Override
			protected int getConfiguredWidth() {
				return 300;
			}
		}

		@Order(3000)
		public class IsInhouseColumn extends AbstractBooleanColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("IsInhouse");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(3500)
		public class ColorColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Color");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}

			@Override
			protected void execDecorateCell(final Cell cell, final ITableRow row) {
				final String colorHex = ColorsUtil.getColorHexStringById(String.valueOf(row.getCell(getColorColumn().getColumnIndex()).getValue()));
				if ( ! StringUtility.isNullOrEmpty(colorHex)) {
					cell.setBackgroundColor(colorHex);
				}
				cell.setText("");
			}
		}

		@Order(4000)
		public class RecordstatusIdColumn extends AbstractIntegerColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("RecordstatusId");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}

			@Override
			protected boolean getConfiguredDisplayable() {
				return false;
			}
		}


	}
}
