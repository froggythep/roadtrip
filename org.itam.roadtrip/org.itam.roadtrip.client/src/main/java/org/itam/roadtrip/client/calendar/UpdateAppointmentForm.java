package org.itam.roadtrip.client.calendar;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractRadioButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.client.ui.form.fields.radiobuttongroup.AbstractRadioButtonGroup;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.itam.roadtrip.client.calendar.UpdateAppointmentForm.MainBox.CancelButton;
import org.itam.roadtrip.client.calendar.UpdateAppointmentForm.MainBox.MessageField;
import org.itam.roadtrip.client.calendar.UpdateAppointmentForm.MainBox.OkButton;
import org.itam.roadtrip.client.calendar.UpdateAppointmentForm.MainBox.UpdateTypeBox;
import org.itam.roadtrip.client.calendar.UpdateAppointmentForm.MainBox.UpdateTypeBox.UpdateOnlyThisButton;
import org.itam.roadtrip.client.calendar.UpdateAppointmentForm.MainBox.UpdateTypeBox.UpdateThisAndFollowingButton;
import org.itam.roadtrip.shared.GlobalDefines.UpdateType;

public class UpdateAppointmentForm extends AbstractForm {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("UpdateAppointment");
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public MessageField getMessageField() {
		return getFieldByClass(MessageField.class);
	}

	public UpdateTypeBox getUpdateTypeBox() {
		return getFieldByClass(UpdateTypeBox.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	public UpdateOnlyThisButton getUpdateOnlyThisButton() {
		return getFieldByClass(UpdateOnlyThisButton.class);
	}

	public UpdateThisAndFollowingButton getUpdateThisAndFollowingButton() {
		return getFieldByClass(UpdateThisAndFollowingButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
		public class MessageField extends AbstractLabelField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("UpdateMEssage");
			}

			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}

			@Override
			protected boolean getConfiguredWrapText() {
				return true;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected int getConfiguredGridH() {
				return 2;
			}
		}

		@Order(2000)
		public class UpdateTypeBox extends AbstractRadioButtonGroup<Long> {
			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}

			@Override
			protected int getConfiguredGridH() {
				return 3;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected void execInitField() {
				getUpdateOnlyThisButton().setSelected(true);
			}

			@Order(1000)
			public class UpdateOnlyThisButton extends AbstractRadioButton<Long> {
				@Override
				protected Long getConfiguredRadioValue() {
					return 0L;
				}

				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("UpdateOnlyThis");
				}
			}

			@Order(2000)
			public class UpdateThisAndFollowingButton extends AbstractRadioButton<Long> {
				@Override
				protected Long getConfiguredRadioValue() {
					return 1L;
				}

				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("UpdateThisAndFollowing");
				}
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public UpdateType getUpdateType() {
		if (getUpdateOnlyThisButton().isSelected()) {
			return UpdateType.SINGLE;
		} else {
			return UpdateType.THIS_AND_FOLLOWING;
		}
	}
}
