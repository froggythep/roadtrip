package org.itam.roadtrip.client.route;

import java.util.Date;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.itam.roadtrip.client.route.GenerateRoutesForm.MainBox.CancelButton;
import org.itam.roadtrip.client.route.GenerateRoutesForm.MainBox.DateFromField;
import org.itam.roadtrip.client.route.GenerateRoutesForm.MainBox.DateToField;
import org.itam.roadtrip.client.route.GenerateRoutesForm.MainBox.MessageField;
import org.itam.roadtrip.client.route.GenerateRoutesForm.MainBox.OkButton;

public class GenerateRoutesForm extends AbstractForm {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("GenerateRoutes");
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public DateFromField getDateFromField() {
		return getFieldByClass(DateFromField.class);
	}

	public DateToField getDateToField() {
		return getFieldByClass(DateToField.class);
	}

	public MessageField getMessageField() {
		return getFieldByClass(MessageField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {



		@Order(0)
		@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
		public class MessageField extends AbstractLabelField {
			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}

			@Override
			protected boolean getConfiguredWrapText() {
				return true;
			}

			@Override
			protected void execInitField() {
				final String value = TEXTS.get("GenerateRoutesMessage");
				setValue(value);
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected int getConfiguredGridH() {
				return 2;
			}
		}

		@Order(1000)
		public class DateFromField extends AbstractDateField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("from") + ":";
			}

			@Override
			public boolean isMandatory() {
				return true;
			}

			@Override
			protected void execChangedValue() {
				super.execChangedValue();
				getDateToField().setValue(getValue());
			}
		}

		@Order(2000)
		public class DateToField extends AbstractDateField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("to") + ":";
			}

			@Override
			public boolean isMandatory() {
				return true;
			}

			@Override
			protected Date execValidateValue(final Date rawValue) {
				if (rawValue != null && rawValue.before(getDateFromField().getValue())) {
					throw new VetoException(TEXTS.get("ToDateBeforFromDateError"));
				}
				return super.execValidateValue(rawValue);
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}
}
