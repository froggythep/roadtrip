package org.itam.roadtrip.client.route;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.ContentAssistFieldTable;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;
import org.eclipse.scout.rt.shared.services.lookup.ILookupRow;
import org.itam.roadtrip.client.route.AddTripForm.MainBox.CancelButton;
import org.itam.roadtrip.client.route.AddTripForm.MainBox.DestinationAddressField;
import org.itam.roadtrip.client.route.AddTripForm.MainBox.DestinationField;
import org.itam.roadtrip.client.route.AddTripForm.MainBox.OkButton;
import org.itam.roadtrip.client.route.AddTripForm.MainBox.OriginAddressField;
import org.itam.roadtrip.client.route.AddTripForm.MainBox.OriginField;
import org.itam.roadtrip.shared.person.PersonLookupCall;
import org.itam.roadtrip.shared.person.PersonTableRowData;
import org.itam.roadtrip.shared.route.AddTripFormData;
import org.itam.roadtrip.shared.route.CreateAddTripPermission;
import org.itam.roadtrip.shared.route.IAddTripService;
import org.itam.roadtrip.shared.route.UpdateAddTripPermission;

@FormData(value = AddTripFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class AddTripForm extends AbstractForm {

	private String tripId;

	private Integer recordstatusId;

	@FormData
	public String getTripId() {
		return tripId;
	}

	@FormData
	public void setTripId(final String tripId) {
		this.tripId = tripId;
	}

	@FormData
	public Integer getRecordstatusId() {
		return recordstatusId;
	}

	@FormData
	public void setRecordstatusId(final Integer recordstatusId) {
		this.recordstatusId = recordstatusId;
	}

	final ILookupRow<String> originLookupRow;

	public AddTripForm() {
		originLookupRow = null;
	}

	public AddTripForm(final ILookupRow<String> originLookupRow) {
		this.originLookupRow = originLookupRow;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("AddTrip");
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public OriginField getOriginField() {
		return getFieldByClass(OriginField.class);
	}

	public OriginAddressField getOriginAddressField() {
		return getFieldByClass(OriginAddressField.class);
	}

	public DestinationField getDestinationField() {
		return getFieldByClass(DestinationField.class);
	}

	public DestinationAddressField getDestinationAddressField() {
		return getFieldByClass(DestinationAddressField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class OriginField extends AbstractSmartField<String> {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("OriginPlace") + ":";
			}

			@Override
			public boolean isMandatory() {
				return true;
			}

			@Override
			protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
				return PersonLookupCall.class;
			}

			@Override
			protected void execChangedValue() {
				super.execChangedValue();
				final PersonTableRowData data = (PersonTableRowData) getCurrentLookupRow().getAdditionalTableRowData();
				getOriginAddressField().setValue(data.getStreet() + " " + data.getPlz() + " " + data.getCity());

			}

			@Override
			protected void execInitField() {
				final String defaultValue = ((PersonTableRowData) originLookupRow.getAdditionalTableRowData()).getName();
				setValue(defaultValue);
				setCurrentLookupRow(originLookupRow);
				execChangedValue();
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected String validateValueInternal(final String rawValue) {
				if (rawValue == null || rawValue.trim().length() == 0 ) {
					throw new VetoException(TEXTS.get("Name") + TEXTS.get("notEmpty"));
				}
				return super.execValidateValue(rawValue);
			}

			@Order(10.0)
			public class Table extends ContentAssistFieldTable<Long> {

				public SirnameColumn getSirnameColumn() {
					return getColumnSet().getColumnByClass(SirnameColumn.class);
				}

				public StreetColumn getStreetColumn() {
					return getColumnSet().getColumnByClass(StreetColumn.class);
				}

				public PlzColumn getPlzColumn() {
					return getColumnSet().getColumnByClass(PlzColumn.class);
				}

				public CityColumn getCityColumn() {
					return getColumnSet().getColumnByClass(CityColumn.class);
				}

				public PersonIdColumn getPersonIdColumn() {
					return getColumnSet().getColumnByClass(PersonIdColumn.class);
				}

				public NameColumn getNameColumn() {
					return getColumnSet().getColumnByClass(NameColumn.class);
				}

				@Order(0)
				public class PersonIdColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("ID");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}

				@Order(1000)
				public class NameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Name");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(2000)
				public class SirnameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Sirname");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(3000)
				public class StreetColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Street");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(4000)
				public class PlzColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("PLZ");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(5000)
				public class CityColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("City");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}
			}
		}

		@Order(2000)
		@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
		public class OriginAddressField extends AbstractLabelField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Origin") + ":";
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}
		}

		@Order(3000)
		public class DestinationField extends AbstractSmartField<String> {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("DestinationPerson") + ":";
			}

			@Override
			public boolean isMandatory() {
				return true;
			}

			@Override
			protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
				return PersonLookupCall.class;
			}

			@Override
			public void acceptProposal(final ILookupRow<String> row) {
				final PersonTableRowData data = (PersonTableRowData) row.getAdditionalTableRowData();
				getDestinationAddressField().setValue(data.getStreet() + " " + data.getPlz() + " " + data.getCity());
				super.acceptProposal(row);
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Order(10.0)
			public class Table extends ContentAssistFieldTable<Long> {

				public SirnameColumn getSirnameColumn() {
					return getColumnSet().getColumnByClass(SirnameColumn.class);
				}

				public StreetColumn getStreetColumn() {
					return getColumnSet().getColumnByClass(StreetColumn.class);
				}

				public PlzColumn getPlzColumn() {
					return getColumnSet().getColumnByClass(PlzColumn.class);
				}

				public CityColumn getCityColumn() {
					return getColumnSet().getColumnByClass(CityColumn.class);
				}

				public PersonIdColumn getPersonIdColumn() {
					return getColumnSet().getColumnByClass(PersonIdColumn.class);
				}

				public NameColumn getNameColumn() {
					return getColumnSet().getColumnByClass(NameColumn.class);
				}



				@Order(0)
				public class PersonIdColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("ID");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}



				@Order(1000)
				public class NameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Name");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(2000)
				public class SirnameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Sirname");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(3000)
				public class StreetColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Street");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(4000)
				public class PlzColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("PLZ");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(5000)
				public class CityColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("City");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}
			}
		}



		@Order(4000)
		@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
		public class DestinationAddressField extends AbstractLabelField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("DestinationAddress") + ":";
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IAddTripService service = BEANS.get(IAddTripService.class);
			AddTripFormData formData = new AddTripFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			setEnabledPermission(new UpdateAddTripPermission());
		}

		@Override
		protected void execStore() {
			final IAddTripService service = BEANS.get(IAddTripService.class);
			final AddTripFormData formData = new AddTripFormData();
			exportFormData(formData);
			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			final IAddTripService service = BEANS.get(IAddTripService.class);
			AddTripFormData formData = new AddTripFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			setEnabledPermission(new CreateAddTripPermission());
		}

		@Override
		protected void execStore() {
			final IAddTripService service = BEANS.get(IAddTripService.class);
			final AddTripFormData formData = new AddTripFormData();
			exportFormData(formData);
			service.create(formData);
		}
	}
}
