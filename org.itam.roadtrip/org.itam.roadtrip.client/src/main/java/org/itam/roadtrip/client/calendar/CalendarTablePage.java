package org.itam.roadtrip.client.calendar;

import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.itam.roadtrip.client.calendar.CalendarTablePage.Table;
import org.itam.roadtrip.shared.calendar.CalendarTablePageData;

@Data(CalendarTablePageData.class)
public class CalendarTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Calendar");
	}

	@Override
	protected void execLoadData(final SearchFilter filter) {
	}

	public class Table extends AbstractTable {

		@Order(2000)
		public class NewMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("NewRoom");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.EmptySpace, TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				final CalendarForm form = new CalendarForm();
				form.startNew();
			}
		}

		@Order(1000)
		public class SomeColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return "TEST";
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		public SomeColumn getSomeColumn() {
			return getColumnSet().getColumnByClass(SomeColumn.class);
		}
	}
}
