package org.itam.roadtrip.client.calendar;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractTimeField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.ContentAssistFieldTable;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.CancelButton;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.CancelledField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.EmployeeField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.FromDateField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.FromTimeField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.FulldayField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.IsRepeatTillField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.IsRepeatingField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.NoticeField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.OkButton;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.PatientField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.RepeatTillInfinityField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.RepeatTypeField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.RoomField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.ToDateField;
import org.itam.roadtrip.client.calendar.AppointmentForm.MainBox.ToTimeField;
import org.itam.roadtrip.shared.RepeatTypes;
import org.itam.roadtrip.shared.calendar.AppointmentFormData;
import org.itam.roadtrip.shared.calendar.CalendarAppointmentRoadtrip;
import org.itam.roadtrip.shared.calendar.ICalendarService;
import org.itam.roadtrip.shared.calendar.RoomLookupCall;
import org.itam.roadtrip.shared.person.EmployeeLookupCall;
import org.itam.roadtrip.shared.person.PersonLookupCall;
import org.itam.roadtrip.shared.person.PersonTableRowData;

@FormData(value = AppointmentFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class AppointmentForm extends AbstractForm {

	private Date selectedDate;

	private CalendarAppointmentRoadtrip car;

	private NewHandler newHandler;

	private ModifyHandler modifyHandler;

	public AppointmentForm(final Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	public AppointmentForm(final CalendarAppointmentRoadtrip car) {
		this.car = car;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Appointment");
	}

	public void startModify() {
		modifyHandler = new ModifyHandler();
		startInternalExclusive(modifyHandler);
	}

	public void saveModifiedSingleAppointment() {
		if (modifyHandler != null) {
			modifyHandler.storeSingleAppointment();
		}
	}

	public void startNew() {
		newHandler = new NewHandler();
		startInternal(new NewHandler());
	}

	public void saveNew() {
		if (newHandler != null) {
			newHandler.store();
		}
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public FromDateField getFromDateField() {
		return getFieldByClass(FromDateField.class);
	}

	public ToDateField getToDateField() {
		return getFieldByClass(ToDateField.class);
	}

	public EmployeeField getEmployeeField() {
		return getFieldByClass(EmployeeField.class);
	}

	public PatientField getPatientField() {
		return getFieldByClass(PatientField.class);
	}

	public RoomField getRoomField() {
		return getFieldByClass(RoomField.class);
	}

	public CancelledField getIsCancelledField() {
		return getFieldByClass(CancelledField.class);
	}

	public FromTimeField getFromTimeField() {
		return getFieldByClass(FromTimeField.class);
	}

	public ToTimeField getToTimeField() {
		return getFieldByClass(ToTimeField.class);
	}

	public FulldayField getFulldayField() {
		return getFieldByClass(FulldayField.class);
	}

	public IsRepeatingField getIsRepeatingField() {
		return getFieldByClass(IsRepeatingField.class);
	}

	public RepeatTypeField getRepeatTypeField() {
		return getFieldByClass(RepeatTypeField.class);
	}

	public IsRepeatTillField getRepeatTillField() {
		return getFieldByClass(IsRepeatTillField.class);
	}

	public RepeatTillInfinityField getIsRepeatTillInfinityField() {
		return getFieldByClass(RepeatTillInfinityField.class);
	}

	public NoticeField getNoticeField() {
		return getFieldByClass(NoticeField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {



		@Order(0)
		public class EmployeeField extends AbstractSmartField<String> {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Employee") + ": ";
			}

			@Override
			protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
				return EmployeeLookupCall.class;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected void execChangedValue() {
				super.execChangedValue();
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			@Order(10.0)
			public class Table extends ContentAssistFieldTable<Long> {

				@Override
				protected boolean getConfiguredHeaderVisible() {
					return true;
				}

				public SirnameColumn getSirnameColumn() {
					return getColumnSet().getColumnByClass(SirnameColumn.class);
				}

				public StreetColumn getStreetColumn() {
					return getColumnSet().getColumnByClass(StreetColumn.class);
				}

				public PlzColumn getPlzColumn() {
					return getColumnSet().getColumnByClass(PlzColumn.class);
				}

				public CityColumn getCityColumn() {
					return getColumnSet().getColumnByClass(CityColumn.class);
				}

				public PersonIdColumn getPersonIdColumn() {
					return getColumnSet().getColumnByClass(PersonIdColumn.class);
				}

				public NameColumn getNameColumn() {
					return getColumnSet().getColumnByClass(NameColumn.class);
				}



				@Order(0)
				public class PersonIdColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("ID");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}



				@Order(1000)
				public class NameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Name");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(2000)
				public class SirnameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Sirname");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(3000)
				public class StreetColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Street");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(4000)
				public class PlzColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("PLZ");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(5000)
				public class CityColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("City");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}
			}
		}

		@Order(500)
		public class PatientField extends AbstractSmartField<String> {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Patient") + ": ";
			}

			@Override
			protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
				return PersonLookupCall.class;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected void execChangedValue() {
				super.execChangedValue();
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			@Order(10.0)
			public class Table extends ContentAssistFieldTable<Long> {

				@Override
				protected boolean getConfiguredHeaderVisible() {
					return true;
				}

				public SirnameColumn getSirnameColumn() {
					return getColumnSet().getColumnByClass(SirnameColumn.class);
				}

				public StreetColumn getStreetColumn() {
					return getColumnSet().getColumnByClass(StreetColumn.class);
				}

				public PlzColumn getPlzColumn() {
					return getColumnSet().getColumnByClass(PlzColumn.class);
				}

				public CityColumn getCityColumn() {
					return getColumnSet().getColumnByClass(CityColumn.class);
				}

				public PersonIdColumn getPersonIdColumn() {
					return getColumnSet().getColumnByClass(PersonIdColumn.class);
				}

				public NameColumn getNameColumn() {
					return getColumnSet().getColumnByClass(NameColumn.class);
				}



				@Order(0)
				public class PersonIdColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("ID");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}

				@Order(1000)
				public class NameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Name");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(2000)
				public class SirnameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Sirname");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(3000)
				public class StreetColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Street");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(4000)
				public class PlzColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("PLZ");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(5000)
				public class CityColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("City");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}
			}
		}


		@Order(750)
		public class RoomField extends AbstractSmartField<String> {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Room") + ": ";
			}

			@Override
			protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
				return RoomLookupCall.class;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			@Order(10.0)
			public class Table extends ContentAssistFieldTable<Long> {

				@Override
				protected boolean getConfiguredHeaderVisible() {
					return true;
				}

				public IsInhouseColumn getIsInhouseColumn() {
					return getColumnSet().getColumnByClass(IsInhouseColumn.class);
				}

				public RecordstatusIdColumn getRecordstatusIdColumn() {
					return getColumnSet().getColumnByClass(RecordstatusIdColumn.class);
				}

				public ColorColumn getColorColumn() {
					return getColumnSet().getColumnByClass(ColorColumn.class);
				}

				public NameColumn getNameColumn() {
					return getColumnSet().getColumnByClass(NameColumn.class);
				}

				@Order(0)
				public class RoomIdColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("ID");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}

				@Order(1000)
				public class NameColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Name");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}
				}

				@Order(2000)
				public class IsInhouseColumn extends AbstractBooleanColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("IsInhouse");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}

				@Order(3000)
				public class RecordstatusIdColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("RecordstatusId");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}

				@Order(4000)
				public class ColorColumn extends AbstractStringColumn {
					@Override
					protected String getConfiguredHeaderText() {
						return TEXTS.get("Color");
					}

					@Override
					protected int getConfiguredWidth() {
						return 100;
					}

					@Override
					protected boolean getConfiguredDisplayable() {
						return false;
					}
				}


			}
		}

		@Order(875)
		public class FulldayField extends AbstractBooleanField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Fullday");
			}

			@Override
			protected void execChangedValue() {
				getFromTimeField().setEnabled( ! getValue());
				getFromTimeField().setMandatory( ! getValue());
				getFromTimeField().setVisible( ! getValue());

				getToTimeField().setEnabled( ! getValue());
				getToTimeField().setMandatory( ! getValue());
				getToTimeField().setVisible( ! getValue());
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}
		}

		@Order(1000)
		public class FromDateField extends AbstractDateField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("from") + ": ";
			}

			@Override
			protected void execInitField() {
				setValue(selectedDate);
			}

			@Override
			protected void execChangedValue() {
				super.execChangedValue();

				getToDateField().setValue(getValue());
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}
		}

		@Order(1500)
		public class FromTimeField extends AbstractTimeField {

			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("from");
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			@Override
			protected void execChangedValue() {
				super.execChangedValue();
				getToTimeField().setValue(new Date(getValue().getTime() + 3600000));
			}
		}

		@Order(1250)
		public class ToDateField extends AbstractDateField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("to") + ": ";
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}

			@Override
			protected void execInitField() {
				setValue(selectedDate);
			}
		}

		@Order(2000)
		public class ToTimeField extends AbstractTimeField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("to");
			}

			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}

			@Override
			protected boolean getConfiguredMandatory() {
				return true;
			}
		}

		@Order(2250)
		public class IsRepeatingField extends AbstractBooleanField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("IsRepeating");
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected void execChangedValue() {
				if (getValue()) {
					getRepeatTypeField().setVisible(true);
					getRepeatTypeField().setMandatory(true);

					getIsRepeatTillInfinityField().setVisible(true);
					getIsRepeatTillInfinityField().setValue(true);
				} else {
					getRepeatTypeField().setVisible(false);
					getRepeatTypeField().setMandatory(false);
					getRepeatTypeField().resetValue();

					getIsRepeatTillInfinityField().setVisible(false);
				}
			}
		}

		@Order(2375)
		public class RepeatTypeField extends AbstractSmartField<String> {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Repetitions");
			}

			@Override
			protected Class<? extends ICodeType<?, String>> getConfiguredCodeType() {
				return RepeatTypes.class;
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}
		}

		@Order(2406)
		public class IsRepeatTillField extends AbstractDateField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("RepeatTill");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}
		}

		@Order(2437)
		public class RepeatTillInfinityField extends AbstractBooleanField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("WithoutEnd");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected void execChangedValue() {
				if (getValue()) {
					getRepeatTillField().setVisible(true);
					getRepeatTillField().setEnabled(false);
					getRepeatTillField().setMandatory(false);
				} else {
					getRepeatTillField().setVisible(true);
					getRepeatTillField().setEnabled(true);
					getRepeatTillField().setMandatory(true);
				}
			}
		}

		@Order(2500)
		public class NoticeField extends AbstractStringField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Notice") + ": ";
			}

			@Override
			protected int getConfiguredMaxLength() {
				return 4000;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected int getConfiguredGridH() {
				return 6;
			}

			@Override
			protected boolean getConfiguredMultilineText() {
				return true;
			}
		}

		@Order(3000)
		public class CancelledField extends AbstractBooleanField {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Cancelled");
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		private final ICalendarService service;

		public ModifyHandler() {
			service = BEANS.get(ICalendarService.class);
		}

		@Override
		protected void execLoad() {
			getEmployeeField().setValue(car.getEmployeeId());
			getPatientField().setValue(car.getPatientId());
			getRoomField().setValue(car.getRoomId());
			getFulldayField().setValue(car.isFullDay());
			getFromDateField().setValue(car.getStart());
			getFromTimeField().setValue(car.getStart());
			getToDateField().setValue(car.getEnd());
			getToTimeField().setValue(car.getEnd());
			getIsRepeatingField().setValue(car.isRepeating());
			getRepeatTypeField().setValue(car.getRepeatType());
			getIsRepeatTillInfinityField().setValue(car.isRepeatTillInfinity());
			getRepeatTillField().setValue(car.getRepeatTill());
			getNoticeField().setValue(car.getNotice());
			getIsCancelledField().setValue(car.isCancelled());

			getIsRepeatingField().setEnabled(false);
			getIsRepeatTillInfinityField().setEnabled(false);
			getRepeatTypeField().setEnabled(false);
		}

		protected void storeSingleAppointment() {
			service.saveAppointment(car);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		protected void store() {
			final CalendarAppointmentRoadtrip app = getNewAppointment();
			final ICalendarService service = BEANS.get(ICalendarService.class);

			service.saveAppointment(app);
		}
	}

	private CalendarAppointmentRoadtrip getNewAppointment() {

		final CalendarAppointmentRoadtrip appointment = new CalendarAppointmentRoadtrip(1L, 1L,
				getTotalDateTimeDate(getFromDateField().getValue(), getFromTimeField().getValue()),
				getTotalDateTimeDate(getToDateField().getValue(), getToTimeField().getValue()),
				getFulldayField().getValue(),
				getRoomField().getCurrentLookupRow().getKey(),
				getEmployeeField().getCurrentLookupRow().getKey(),
				getPatientField().getCurrentLookupRow().getKey(),
				getIsRepeatingField().getValue(),
				getRepeatTypeField().getValue(),
				getIsRepeatTillInfinityField().getValue(),
				getRepeatTillField().getValue(),
				getNoticeField().getValue(),
				getIsCancelledField().getValue(),
				getRoomField().getCurrentLookupRow().getText(),
				getEmployeeField().getCurrentLookupRow().getText(),
				getPatientField().getCurrentLookupRow().getText(),
				((PersonTableRowData) getPatientField().getCurrentLookupRow().getAdditionalTableRowData()).getSirname());

		return appointment;
	}

	public CalendarAppointmentRoadtrip getUpdatedCalendarAppointmentRoadtrip() {
		final CalendarAppointmentRoadtrip updatedAppointment = getNewAppointment();
		updatedAppointment.setAppointmentId(car.getAppointmentId());
		car = updatedAppointment;
		return car;
	}

	private Date getTotalDateTimeDate(final Date startDate, final Date startTime) {
		final Calendar calDate = Calendar.getInstance();
		calDate.setTime(startDate);

		if (startTime != null) {
			final Calendar calTime = Calendar.getInstance();
			calTime.setTime(startTime);

			calDate.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
			calDate.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
		}

		return calDate.getTime();
	}
}
