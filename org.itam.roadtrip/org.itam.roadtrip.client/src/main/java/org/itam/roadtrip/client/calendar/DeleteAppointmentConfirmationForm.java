package org.itam.roadtrip.client.calendar;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractRadioButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.client.ui.form.fields.radiobuttongroup.AbstractRadioButtonGroup;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.CancelButton;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.DeleteTypeBox;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.DeleteTypeBox.DeleteAllButton;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.DeleteTypeBox.DeleteOnlyThisButton;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.DeleteTypeBox.DeleteThisAndFollowingButton;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.MessageField;
import org.itam.roadtrip.client.calendar.DeleteAppointmentConfirmationForm.MainBox.OkButton;

public class DeleteAppointmentConfirmationForm extends AbstractForm {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("DeleteAppointmentConfirmation");
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public MessageField getMessageField() {
		return getFieldByClass(MessageField.class);
	}

	public DeleteTypeBox getDeleteTypeBox() {
		return getFieldByClass(DeleteTypeBox.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	public DeleteOnlyThisButton getDeleteOnlyThisButton() {
		return getFieldByClass(DeleteOnlyThisButton.class);
	}

	public DeleteThisAndFollowingButton getDeleteThisAndFollowingButton() {
		return getFieldByClass(DeleteThisAndFollowingButton.class);
	}

	public DeleteAllButton getDeleteAllButton() {
		return getFieldByClass(DeleteAllButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
		public class MessageField extends AbstractLabelField {
			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}

			@Override
			protected boolean getConfiguredWrapText() {
				return true;
			}

			@Override
			protected void execInitField() {
				final String value = TEXTS.get("DeleteAppointmentMessage");
				setValue(value);
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected int getConfiguredGridH() {
				return 2;
			}
		}

		@Order(2000)
		public class DeleteTypeBox extends AbstractRadioButtonGroup<Long> {
			@Override
			protected boolean getConfiguredLabelVisible() {
				return false;
			}

			@Override
			protected int getConfiguredGridH() {
				return 3;
			}

			@Override
			protected int getConfiguredGridW() {
				return 2;
			}

			@Override
			protected void execInitField() {
				getDeleteOnlyThisButton().setSelected(true);
			}

			@Order(1000)
			public class DeleteOnlyThisButton extends AbstractRadioButton<Long> {

				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("DeleteOnlyThis");
				}

				@Override
				protected Long getConfiguredRadioValue() {
					return 0L;
				}
			}

			@Order(2000)
			public class DeleteThisAndFollowingButton extends AbstractRadioButton<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("DeleteThisAndFollowing");
				}

				@Override
				protected Long getConfiguredRadioValue() {
					return 1L;
				}
			}

			@Order(3000)
			public class DeleteAllButton extends AbstractRadioButton<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("DeleteAll");
				}

				@Override
				protected Long getConfiguredRadioValue() {
					return 2L;
				}
			}
		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

}
